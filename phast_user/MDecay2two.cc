#include <iostream>
#include <cstring>

#include "MDecay2two.h"
#include "mass.h"
#include "heplib.h"

MDecay2two::MDecay2two(const double *p1, const double *p2,
		       double mass1, double mass2) {
  std::memcpy(gmm1,p1,3*sizeof(double));
  std::memcpy(gmm2,p2,3*sizeof(double));
  if(mass1>=0 && mass2>=0) {
    hlmass5(gmm1,gmm1,mass1); hlmass5(gmm2,gmm2,mass2);
  }
  hladdt5(gmm1,gmm2,pi_z);
}

MDecay2two::MDecay2two(const double *p,
		       const double cos_c,const double phi,
		       double mass1, double mass2) {
  for(int i=0;i<5;i++) pi_z[i] = p[i];

  if(p[4]<mass1+mass2) std::cerr << "Error<>MDecay2two: p[4]<mass1+mass2. Decay particle is lighter products!" << std::endl;
  double p_star = sqrt(
		       (p[4]*p[4]-(mass1+mass2)*(mass1+mass2))*
		       (p[4]*p[4]-(mass1+mass2)*(mass1+mass2))
		       )/(2*p[4]);
  double e1_star = sqrt(p_star*p_star+mass1*mass1);
  double e2_star = sqrt(p_star*p_star+mass2*mass2);

  double beta = sqrt(hlsclp3(pi_z,pi_z))/pi_z[3];
  double gamma = pi_z[3]/pi_z[4];

  gmm1[0] = p_star*sqrt(1-cos_c*cos_c)*cos(phi);
  gmm1[1] = p_star*sqrt(1-cos_c*cos_c)*sin(phi);
  gmm1[2] = gamma*(beta*e1_star+p_star*cos_c);
  hlmass5(gmm1,gmm1,mass1);

  gmm2[0] = -p_star*sqrt(1-cos_c*cos_c)*cos(phi);
  gmm2[1] = -p_star*sqrt(1-cos_c*cos_c)*sin(phi);
  gmm2[2] = gamma*(beta*e2_star-p_star*cos_c);
  hlmass5(gmm2,gmm2,mass2);

  double ref[3] = {-pi_z[0],
		   -pi_z[1],
		   pi_z[2]};

  hltrn3(gmm1,gmm1,ref);
  hltrn3(gmm2,gmm2,ref);
  //hlsubt5(pi_z,gmm1,gmm2);
}

void MDecay2two::MassConstaintGammaEnergy(const double *g1, 
					  const double *g2,
					  double *g1out,
					  double *g2out,
					  double mass) {

  double lg1[5],lg2[5];  hlmass5(g1,lg1,0);   hlmass5(g2,lg2,0);
  double cosT = hlsclp3(lg1,lg2)/(lg1[3]*lg2[3]);
  double mm = mass*mass/(2*(1-cosT));
  double sq_k = (lg1[3]-lg2[3])/2.0;
  double sq_d4 = sqrt( sq_k*sq_k+mm );
  double e1_new =  sq_k+sq_d4;
  double e2_new = -sq_k+sq_d4;
  
  for(int i=0;i<4;i++) g1out[i] = g1[i]*e1_new/g1[3];
  for(int i=0;i<4;i++) g2out[i] = g2[i]*e2_new/g2[3];
  g1out[4] = 0;  g2out[4] = 0;

}


void MDecay2two::MassConstaintGammaEnergy(double mass) {
  MDecay2two::MassConstaintGammaEnergy(gmm1,gmm2,gmm1,gmm2,mass);
  hladdt5(gmm1,gmm2,pi_z);
}



void MDecay2two::Print() const {
  std::cout << "pi_z = (" 
	    << pi_z[0] << " "
	    << pi_z[1] << " "
	    << pi_z[2] << " "
	    << pi_z[3] << " "
	    << pi_z[4] << ") \n"
	    << "gmm1 = ("
	    << gmm1[0] << " "
	    << gmm1[1] << " "
	    << gmm1[2] << " "
	    << gmm1[3] << " "
	    << gmm1[4] << ") \n"
	    << "gmm2 = (" 
	    << gmm2[0] << " "
	    << gmm2[1] << " "
	    << gmm2[2] << " "
	    << gmm2[3] << " "
	    << gmm2[4] << ")" << std::endl; 
}
