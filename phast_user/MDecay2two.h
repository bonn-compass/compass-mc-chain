#ifndef __MDECAY2G_H__
#define __MDECAY2G_H__

class MDecay2two {

 private:
  double pi_z[5];
  double gmm1[5];
  double gmm2[5];
  
 public:
  MDecay2two();
  MDecay2two(const double *p, const double cos_c, const double phi_c,
	     double mass1 = 0,double mass2 = 0);
  MDecay2two(const double *p1, const double *p2,
	     double mass1 = 0, double mass2 = 0);
  
  static void MassConstaintGammaEnergy(const double *g1, 
				       const double *g2,
				       double *g1out,
				       double *g2out,
				       double mass);
  void MassConstaintGammaEnergy(double mass);

  const double* fdprt() const {return pi_z;};
  const double* fgmm1() const {return gmm1;};
  const double* fgmm2() const {return gmm2;};
  
  void Print() const; 
};
#endif
