#include "MHistThreePS.h"

MHistThreePS::MHistThreePS(const char *name):
  MHistogramer(name) {
}

void MHistThreePS::BookDP() {
  if(dp_is_booked) return;
  dp_is_booked = 1;

  hdp12 = new TH1D("dp12","Invariant mass of 1-2 subsystem;M_{1,2}",100,0,3.);
  hdp23 = new TH1D("dp23","Invariant mass of 2-3 subsystem;M_{2,3}",100,0,3.);
  hdp13 = new TH1D("dp13","Invariant mass of 1-3 subsystem;M_{1,3}",100,0,3.);
  hdp2D3 = new TH2D("dp2D3","Dalitz plot;M_{1,3};M_{2,3}",100,0,3.,100,0,3.);
  hdp2D1 = new TH2D("dp2D1","Dalitz plot;M_{1,2};M_{1,3}",100,0,3.,100,0,3.);
  hdp2D2 = new TH2D("dp2D2","Dalitz plot;M_{1,2};M_{2,3}",100,0,3.,100,0,3.);
  
}

void MHistThreePS::FillDP(const MSystem & system) {
  
}

void MHistThreePS::WriteDP() {

}
