#ifndef _MHISTTWOPS_H_
#define _MHISTTWOPS_H_

#include <TH1D.h>
#include <TH2D.h>

#include "MHistogramer.h"
#include "MSystem.h"

class MHistThreePS : public MHistogramer {
private:

  int dp_is_booked;

  //Dalitz plot
  TH1D *hdp12;
  TH1D *hdp23;
  TH1D *hdp13;
  TH2D *hdp2D1;
  TH2D *hdp2D2;
  TH2D *hdp2D3;

  //momentums
  TH1D *hp1;
  TH1D *hp2;
  TH1D *hp3;

  //GJ-TY, helicity
  TH1D *hcos_gd;
  TH1D *hphi_gd;
  TH2D *hcgd_pty;
  TH2D *hmcgd;
  TH2D *hmpty;
  TH2D *htcgd;
  TH2D *htpty;  

public:
  MHistThreePS(const char *name);

  void BookDP();
  void FillDP(const MSystem & system);
  void WriteDP();

};

#endif
