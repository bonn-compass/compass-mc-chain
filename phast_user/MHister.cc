//Mikhail Mikhasenko
//27.06.13

#include <iostream>
#include <sstream>
#include <string>

#include "MHister.h"

#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TMath.h>

#define PI 3.141592
#define HASH_STEP 97

MHister::MHister(const char *name) {
  HachInit();
  file = TFile::Open(name,"RECREATE");
  if(!file) {
    std::cerr << "The file " << name 
	      << " couldnt be opened. Exit!" << std::endl;
    exit(1);
  }
}

MHister::~MHister() {
  if(file)  {
    cd_file();
    map_obj::iterator iter_i;
    for(iter_i=objects.begin();iter_i!=objects.end();iter_i++) 
      for(uint j=0;j<iter_i->second.size();j++) {
	TObject *obj = iter_i->second[j];
	//std::cout << "---" << j << " " << obj << std::endl;
	obj->Write();
      }
    for(uint k=0;k<associate.size();k++) associate[k]->Write();
    file->Close();
  }
}

void MHister::Book1D(const char* name, 
		     double nbin, double l, double u,
		     const char* title) {
  const char *title_l = title; 
  if(std::string(title) == std::string("name")) title_l = name;
  cd_file();
  TH1D *h = new TH1D(name,title_l,nbin,l,u);
  int64_t hach = Hach(name);
  objects[hach].push_back((TObject*)h);
  std::cout << "created object(TH1D) " << name << std::endl;  
}

void MHister::Book2D(const char* name, 
		     double xbin, double xl, double xu,
		     double ybin, double yl, double yu,
		     const char* title) {
  const char *title_l = title; 
  if(std::string(title) == std::string("name")) title_l = name;
  cd_file();
  TH2D *h = new TH2D(name,title_l,xbin,xl,xu,ybin,yl,yu);
  int64_t hach = Hach(name);
  objects[hach].push_back((TObject*)h);
  std::cout << "created object(TH2D) " << name << std::endl;  
}

void MHister::Book1D(const char* name) {
  std::cerr << "You try to book (" 
	    << name << ") histogram without any parameters.\n"
	    << "Sorry, thit functionality is not in.\n" 
	    << "Histogram with random ranges will be created.\n"
	    << std::endl;
  Book1D(name,200,0,50);
}

void MHister::Book2D(const char* name) {
  std::cerr << "You try to book (" 
	    << name << ") histogram without any parameters.\n"
	    << "Sorry, thit functionality is not in.\n" 
	    << "Histogram with random ranges will be created.\n"
	    << std::endl;
  Book2D(name,200,0,50,200,0,50);
}

void MHister::Fill2D(const char* name, 
		     double v1, double v2, double w, 
		     double xbin, double xl, double xu,
		     double ybin, double yl, double yu,
		     const char* title) {
  int64_t hach = Hach(name);
  map_obj::iterator iter_h = objects.find(hach);
  if(iter_h == objects.end()) {
    Book2D(name,xbin,xl,xu,ybin,yl,yu,title);
    Fill2D(name,v1,v2,w);
  } else 
    Fill2D(name,v1,v2,w);
}

void MHister::Fill1D(const char* name, double v, double w, 
		     double xbin, double xl, double xu,
		     const char* title) {
  int64_t hach = Hach(name);
  map_obj::iterator iter_h = objects.find(hach);
  if(iter_h == objects.end()) {
    Book1D(name,xbin,xl,xu,title);
    Fill1D(name,v,w);
  } else {
    Fill1D(name,v,w);
  }
}

void MHister::Fill1D(const char* name, double v, double w) {
  int64_t hach = Hach(name);
  map_obj::iterator iter_h = objects.find(hach);
  if(iter_h == objects.end()) {
    Book1D(name);
    Fill1D(name,v,w);
    return;
  } else { //found
    TH1D *h1 = (TH1D*)iter_h->second[0];
    if(!strcmp(h1->GetName(),name)) {
      h1->Fill(v,w);
      return;
    } 
    //exist - very rare - collision
    for(uint k=1;k<iter_h->second.size();k++)
      if(!strcmp(iter_h->second[k]->GetName(),name)) {
	((TH1D*)iter_h->second[k])->Fill(v,w);
	return;
      }
//    std::cout << "You are lucky guy! Two hists have the same hash: " 
//	      << "H(" << iter_h->second[0]->GetName() << ") = H(" << name << ")"
//	      << std::endl;
    Book1D(name);
    Fill1D(name,v,w);
  }
}

void MHister::Fill2D(const char* name, double v1, double v2, double w) {
  int64_t hach = Hach(name);
  map_obj::iterator iter_h = objects.find(hach);
  if(iter_h == objects.end()) {
    Book2D(name);
    Fill2D(name,v1,v2,w);
  } else { //found
    TH2D *h2 = (TH2D*)iter_h->second[0];
    if(!strcmp(h2->GetName(),name)) {
      h2->Fill(v1,v2,w);
      return;
    } 
    //exist - very rare
    for(uint k=0;k<iter_h->second.size();k++)
      if(!strcmp(iter_h->second[k]->GetName(),name)) {
	((TH2D*)iter_h->second[k])->Fill(v1,v2,w);
	return;
      }
    std::cout << "You are lucky guy! Two hists have the same hash" 
	      << std::endl;
    Book2D(name);
    Fill2D(name,v1,v2,w);
  }
}

TObject *MHister::GetObject(const char *name) {
  int64_t hach = Hach(name);
  map_obj::iterator iter_h = objects.find(hach);
  if(iter_h == objects.end()) return 0;
  else { 
    TObject *obj = iter_h->second[0];
    if(!strcmp(obj->GetName(),name)) return obj;

    //exist - very rare
    for(uint k=0;k<iter_h->second.size();k++) 
      if(!strcmp(iter_h->second[k]->GetName(),name)) 
	return iter_h->second[k];
    return 0;
  }
}

void MHister::WriteObject(TObject &t, const char *option) {
  cd_file();
  if(file != NULL) t.Write(option);
}


void MHister::CopyDir(TDirectory *source) {
   source->ls();
   TDirectory *adir = gDirectory;
   adir->cd();
   //loop on all entries of this directory
   TKey *key;
   TIter nextkey(source->GetListOfKeys());
   while ((key = (TKey*)nextkey())) {
     const char *classname = key->GetClassName();
     TClass *cl = gROOT->GetClass(classname);
     if (!cl) continue;
     if (cl->InheritsFrom(TTree::Class())) {
       TTree *T = (TTree*)source->Get(key->GetName());
       adir->cd();
       TTree *newT = T->CloneTree(-1,"fast");
       newT->Write();
     } else if (cl->InheritsFrom(TObject::Class())) {
       source->cd();
       TObject *obj = key->ReadObj();
       adir->cd();
       obj->Write();
       delete obj;
     } else continue;
   }
   adir->cd();
}

void MHister::CopyFile(const char *fname) {
  TDirectory *target = gDirectory;
  TFile *f = TFile::Open(fname);
  if (!f || f->IsZombie()) {
    printf("Cannot copy file: %s\n",fname);
    target->cd();
    return;
  }
  target->cd();
  CopyDir(f);
  delete f;
  target->cd();
}  

int64_t MHister::Hach(const char *name) {
  int i=0;
  int64_t hash=0;
  while(name[i] != '\0' && i < NAME_LENGTH_MAX) {
    int i_char = name[i]-'+'+1;
    if(i_char < 0 || i_char > HASH_STEP) 
      std::cerr << "character '" << name[i] 
		<< "' is forbidden to use in name of hist" << std::endl;
    hash += i_char*hashPow[i];
    i++;
  }
  return hash;
//  std::tr1::hash<const char*> hasher1 = *hasher;
//  //std::cout << "-----------" << hasher1("falfnamnlfn") << std::endl;
//  return hasher1(name);
}

void MHister::HachInit() {
  hashPow[0] = 1;
  for(int i=1;i<NAME_LENGTH_MAX;i++) {
    hashPow[i] = hashPow[i-1]*HASH_STEP;
    //std::cout << i << "  " << hashPow[i] << std::endl;
  }
  //hasher = new std::tr1::hash<const char*>;
}
