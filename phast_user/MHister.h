//Mikhail Mikhasenko
//26.11.13

#ifndef _MHISTER_H_
#define _MHISTER_H_

#include <iostream>
#include <string>
#include <tr1/functional>

#include "stdint.h"

#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TROOT.h>
#include <TKey.h>
#include <TSystem.h>
#include <TTree.h>

#define NAME_LENGTH_MAX 200

typedef std::map <int64_t, std::vector < TObject* > > map_obj;

class MHister {
private:

  TFile *file;
  map_obj objects;
  std::tr1::hash<const char*> *hasher;
  std::vector<TObject*> associate;

public:

  MHister(const char *name);
  ~MHister();

  void Book1D(const char* name);
  void Book2D(const char* name);

  void Book1D(const char* name, 
	      double nbin, double l, double u,
	      const char* title = "name");

  void Book2D(const char* name, 
	      double xbin, double xl, double xu,
	      double ybin, double yl, double yu,
	      const char* title = "name");

  void Fill1D(const char* name, 
	      double v, double w, 
	      double xbin, double xl, double xu,
	      const char* title = "name");

  void Fill2D(const char* name, 
	      double v1, double v2, double w, 
	      double xbin, double xl, double xu,
	      double ybin, double yl, double yu,
	      const char* title = "name");
 
  TObject *GetObject(const char *name);

  void Fill1D(const char* name, double v, double w = 1);
  void Fill2D(const char* name, double v1, double v2, double w = 1);

  void CopyDir(TDirectory *source);
  void CopyFile(const char *fname);

public:
  void cd_file() const {if(file) file->cd();};
  void WriteObject(TObject &t, const char *option = "");
  void AssociateObject(TObject &t) { associate.push_back(&t); }
public:
  int64_t hashPow[NAME_LENGTH_MAX];
  void HachInit();
  int64_t Hach(const char *name);
};

#endif
