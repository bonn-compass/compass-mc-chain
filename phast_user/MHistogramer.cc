//Mikhail Mikhasenko
//27.06.13

#include <iostream>
#include <sstream>
#include <string>

#include "MHistogramer.h"
//#include "MPlane.h"

#include <TH1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TDirectory.h>
#include <TMath.h>

#include "heplib.h"

#define PI 3.141592

MHistogramer::MHistogramer(const char *name):
  MHister(name),
  vertex_is_booked(0),
  kinematics_is_booked(0),
  recoil_advanced_is_booked(0),
  pi_zero_is_booked(0),
  pileup_is_booked(0) {
}

MHistogramer::~MHistogramer() {
  if(pileup_is_booked) WritePileUp();
  if(vertex_is_booked) WriteVertex();
  if(kinematics_is_booked) WriteKinematics();
  if(recoil_advanced_is_booked) WriteRecoilAdvanced();
  if(pi_zero_is_booked) WritePi0();
}

void MHistogramer::BookVertex() {
  cd_file();
  vertex_is_booked = 1;
  hvx           = new TH1D("vx","X of vertex;vx[cm];",300,-5,5);
  hvy           = new TH1D("vy","Y of vertex;vy[cm]",300,-5,5);
  hvz           = new TH1D("vz","Z of vertex;vz[cm]",1000,-250, 100);
  hvxy          = new TH2D("vxy","XY-destribution of vertex;vx[cm];vy[cm]",300,-5,5,300,-5,5);
}

void MHistogramer::BookKinematics() {
  cd_file();
  kinematics_is_booked = 1;
  hp_beam       = new TH1D("p_beam","Beam momentum;p_beam[GeV]",200,150,200);
  hbeam_direct  = new TH2D("beam_direct","Beam direction;Px/P;Py/P",200,-0.002,0.002, 200,-0.002,0.002);

  hmass         = new TH1D("mass","Invariant mass;mass[GeV]",300,0,3.2);
  hp_reso       = new TH1D("p_reso","Resonance momentum; p_reso[GeV]",200,100,200);
  hreso_direct  = new TH2D("reso_direct","direction of resonance;Px/P;Py/P",
			   200,-0.02,0.02, 
			   200,-0.02,0.02);

  hq_sq         = new TH1D("t_prime","t_prime",500,0,1);
  ht_min        = new TH1D("t_min","t_min",500,0,0.0002);
  
  hmt           = new TH2D("mt","t_vs_mass",100,0,3,1000,0,0.6);

  hm_recl       = new TH1D("m_recl","m_recoil;[GeV]",500,-3,20);

  hdbt          = new TH1D("dbt","Charge-particles-Beam angle;rad",300,-0,0.05);
  hdbt_sum      = new TH2D("dbt_sum","Charge-particle-Beam angle and Psum correlation;rad;GeV",
			   200,-0,0.05,
			   200,15,35);
}

void MHistogramer::BookRecoilAdvanced() {
  cd_file();
  recoil_advanced_is_booked = 1;
  m_recl_sq = new TH1D("m_recl_sq","recoil mass square;m_{recl}[GeV]",500,-3,100);
  m_down = new TH1D("m_down","Constraint down-mass;m_{recl}[GeV]",500,-2,2);
  hp_beam_measured = new TH1D("p_beam_measured","Measured P_beam;p_{beam}[GeV]",200,189,191);
  hebm_etr = new TH2D("ebm_etr","Measured E_beam and E leading track correlation;e_{beam}[GeV];e_{tr}[GeV]",200,189,191,200,150,200);
  hde_br = new TH1D("de_br","Measured P_beam - Recalculated P_beam;diff[GeV]",200,-10,10);
  //m_recl_beamRecal = new TH1D();
  //m_down_beamRecal = new TH1D();
}

void MHistogramer::BookPi0() {
  cd_file();
  pi_zero_is_booked = 1;
  hpe0 = new TH1D("hpe0","Energy of #pi^{0};e[GeV]",300,0,35);
  hpm0 = new TH1D("hpm0","Mass of #pi^{0};m_{#gamma#gamma}[GeV]",300,0,1);

  hge1 = new TH1D("hg1","Energy of highest gamma;e_{hg}[GeV]",300,-2,17);
  hge2 = new TH1D("hg2","Energy of lowest gamma;e_{lg}[GeV]",300,-2,17);
  hgx1 = new TH1D("hgx1","Px of gamma1;[GeV]",200,-2,2);
  hgx2 = new TH1D("hgx2","Px of gamma2;[GeV]",200,-2,2);

  hdga = new TH1D("hdga","Angle between gamma",300,0,0.3);

  hdecaytheta = new TH1D("hdecaytheta","#pi^{0} decay #cos(theta)",300,-1.1,1.1);
  hdecayphi   = new TH1D("hdecayphi","#pi^{0} decay phi",300,-PI*1.1,PI*1.1);
}

void MHistogramer::BookPileUp() {
  cd_file();
  pileup_is_booked = 1;
  hpileup = new TH1D("pileup","Time interval between events;ns",350,-700,700);
  hbeam_intensity = new TH1D("beam_intensity","The intensity of beam;events/sec",200,0,6e6);
}

//void MHistogramer::FillPlane(const double *v, const char *append) {
//  std::ostringstream out;
//  out << "plane_" << int(v[2]+1e-5) << append;
//  Fill2D(out.str().c_str(),v[0],v[1]);
//}

void MHistogramer::FillVertex(const double* v) const {
  FillVertex(v[0],v[1],v[2]);
}

void MHistogramer::FillVertex(double x,double y,double z) const {
  if(vertex_is_booked) {
    hvx ->Fill(x);
    hvy ->Fill(y);
    hvz ->Fill(z);
    hvxy->Fill(x,y);
  } else {
    std::cerr << "Error: vertex-hists were not booked!" << std::endl;
    exit(1);
  }
}

void MHistogramer::FillKinematics(const MSystem & system) const {

  if(kinematics_is_booked) {
    const double *b = system.fbeam();
    hp_beam    ->Fill( sqrt(b[0]*b[0]+b[1]*b[1]+b[2]*b[2]));
    hbeam_direct    ->Fill( b[0]/b[2],b[1]/b[2]);
    
    const double *reso = system.freso();
    double p_reso = sqrt(hlsclp3(reso,reso));
    hmass->Fill(reso[4]);
    hp_reso->Fill(p_reso);
    hreso_direct->Fill(reso[0]/p_reso,reso[1]/p_reso);
    
    hq_sq    ->Fill( -(system.fq_sq()-system.ftmin()) );
    ht_min    ->Fill(-system.ftmin());
    
    hmt->Fill(reso[4],
	      -(system.fq_sq()-system.ftmin()));
    
    hm_recl ->Fill(system.frecl()[4]);
    
    if(system.GetNneg() > 0) {
      double e_max = system.GetNegative(0)[3]; double i_max = 0;
      for(int i=1;i<system.GetNneg();i++) {
	double e_i = system.GetNegative(0)[3];
	if(e_i > e_max) { e_max = e_i; i_max = i; }
      }
      double angle = acos(hlsclp3(system.fbeam(),system.GetNegative(i_max)) /
			  sqrt(hlsclp3(system.fbeam(),system.fbeam()) *
			       hlsclp3(system.GetNegative(i_max),system.GetNegative(i_max))
			       )
			  );
      hdbt->Fill(angle);
      hdbt_sum->Fill(angle, p_reso);
    }
  } else {
    std::cout << "Error: kinematics-hists were not booked!" << std::endl;
    exit(1);
  }

}

void MHistogramer::FillRecoilAdvanced(const MSystem & system) {
  if(recoil_advanced_is_booked) {
    const double *recl = system.frecl();
    m_recl_sq->Fill(recl[3]*recl[3]-
		    recl[0]*recl[0]-recl[1]*recl[1]-recl[2]*recl[2]);
    
    double down_mass = ( 2*hlsclp5( system.freso(),system.fbeam() ) - 
			 system.fbeam()[4]*system.fbeam()[4] - 
			 system.freso()[4]*system.freso()[4] ) / 
      ( 2.0 *
	(system.fbeam()[3] - system.freso()[3]) );
    m_down->Fill(down_mass);
    const double *b = system.fbeam();
    hp_beam_measured->Fill( sqrt( hlsclp3(b,b) ) );

    double e_max = system.GetNegative(0)[3]; double i_max = 0;
    for(int i=1;i<system.GetNneg();i++) {
      double e_i = system.GetNegative(0)[3];
      if(e_i > e_max) { e_max = e_i; i_max = i; }
    }
    hebm_etr->Fill( b[3], system.GetNegative(i_max)[3]);
    
    MSystem rec_system(system);
    rec_system.ReCulcBeamVector();
    hde_br->Fill( sqrt(hlsclp3(b,b)) - 
		  sqrt(hlsclp3(rec_system.fbeam(),rec_system.fbeam()))
		  );
  } else {
    std::cout << "Error: advaned-recoil-hists were not booked!" << std::endl;
    exit(1);
  }

}

void MHistogramer::FillPi0(const MDecay2two & pi0) {
  if(pi_zero_is_booked) {
    hpe0->Fill(pi0.fdprt()[3]);
    hpm0->Fill(pi0.fdprt()[4]);
    
    double hge_high,hge_low;
    double hgx_high,hgx_low;  
    if(pi0.fgmm1()[3] > pi0.fgmm2()[3]) {
      hge_high = pi0.fgmm1()[3];
      hge_low =  pi0.fgmm2()[3];
      hgx_high = pi0.fgmm1()[0];
      hgx_low =  pi0.fgmm2()[0];
    } else {
      hge_high = pi0.fgmm2()[3];
      hge_low =  pi0.fgmm1()[3];
      hgx_high = pi0.fgmm2()[0];
      hgx_low =  pi0.fgmm1()[0];
    }
    hge1->Fill(hge_high);
    hge2->Fill(hge_low);
    hgx1->Fill(hgx_high);
    hgx2->Fill(hgx_low);
    
    double v1[3] = {pi0.fgmm1()[0],
		    pi0.fgmm1()[1],
		    pi0.fgmm1()[2]};
    double v2[3] = {pi0.fgmm2()[0],
		    pi0.fgmm2()[1],
		    pi0.fgmm2()[2]};
    hlnorm3(v1,v1);
    hlnorm3(v2,v2);
    double sc_pr = hlsclp3(v1,v2);
    hdga->Fill(TMath::ACos(sc_pr));
    
    double gamma_pi = pi0.fdprt()[3] / pi0.fdprt()[4];

    double g1_cm[4], g2_cm[4];
    hltrn3(pi0.fgmm1(),g1_cm,pi0.fdprt());     hltrn3(pi0.fgmm2(),g2_cm,pi0.fdprt());
    g1_cm[3] = sqrt(hlsclp3(g1_cm,g1_cm));     g2_cm[3] = sqrt(hlsclp3(g2_cm,g2_cm));
    hlbstz5(g1_cm,g1_cm,gamma_pi);             hlbstz5(g2_cm,g2_cm,gamma_pi);

    hdecaytheta->Fill( g1_cm[2]/g1_cm[3] );    hdecaytheta->Fill( g2_cm[2]/g2_cm[3] );
    hdecayphi->Fill(atan2(g1_cm[1],g1_cm[0])); hdecayphi->Fill(atan2(g2_cm[1],g2_cm[0]));
  } else {
    std::cout << "Error: Pi0-hists were not booked!" << std::endl;
    exit(1);
  }
}

void MHistogramer::FillPileUp(double intens, double t1, double t2) {
  if(pileup_is_booked) {
    hpileup->Fill(-t1);
    hpileup->Fill(t2);
    hbeam_intensity->Fill(intens);
  } else {
    std::cout << "Error: PileUp were not booked!" << std::endl;
    exit(1);
  }
}

void MHistogramer::WriteVertex() {
  cd_file();
  hvx          ->Write();
  hvy          ->Write();
  hvz          ->Write();
  hvxy         ->Write();
}

void MHistogramer::WriteKinematics() {
  cd_file();
  hp_beam      ->Write();
  hbeam_direct ->Write();
  hmass        ->Write();
  hp_reso      ->Write();
  hreso_direct ->Write();
  hq_sq        ->Write();
  ht_min       ->Write();
  hmt          ->Write();
  hm_recl      ->Write();
  hdbt         ->Write();
  hdbt_sum     ->Write();
}

void MHistogramer::WriteRecoilAdvanced() {
  cd_file();
  m_recl_sq->Write();
  m_down->Write();
  hp_beam_measured->Write();
  hebm_etr->Write();
  hde_br->Write();
}

void MHistogramer::WritePi0() {
  cd_file();
  hpe0->Write();
  hpm0->Write();
  hge1->Write();
  hge2->Write();
  hgx1->Write();
  hgx2->Write();
  hdga->Write();
  hdecaytheta->Write();
  hdecayphi  ->Write();
}
void MHistogramer::WritePileUp() {
  cd_file();
  hpileup->Write();
  hbeam_intensity->Write();
}
