//Mikhail Mikhasenko
//27.06.13

#ifndef _MHISTOGRAMER_H_
#define _MHISTOGRAMER_H_

#include <iostream>
#include <string>

#include "MHister.h"

#include "MSystem.h"
#include "MDecay2two.h"
#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TROOT.h>
#include <TKey.h>
#include <TSystem.h>
#include <TTree.h>

class MHistogramer : public MHister {
private:

  //Vertex
  int vertex_is_booked;
  TH1D *hvx;
  TH1D *hvy;
  TH1D *hvz; 
  TH2D *hvxy;

  //Kinematics
  int kinematics_is_booked;
  TH2D *hbeam_direct;
  TH1D *hp_beam;
  TH1D *hq_sq;
  TH1D *ht_min;
  TH1D *hm_recl;
  TH2D *hmt;
  TH1D *hmass;
  TH2D *hreso_direct;
  TH1D *hp_reso;
  TH1D *hdbt;
  TH2D *hdbt_sum;

  //Recoil advanced
  int recoil_advanced_is_booked;
  TH1D *m_recl_sq;
  TH1D *m_down;
  TH1D *hp_beam_measured;
  TH2D *hebm_etr;
  TH1D *hde_br;

  //Pi0
  int pi_zero_is_booked;
  TH1D *hpe0;
  TH1D *hpm0;
  TH1D *hge1;
  TH1D *hge2;
  TH1D *hgx1;
  TH1D *hgx2;
  TH1D *hdga;
  TH1D *hdecaytheta;
  TH1D *hdecayphi;

  //PileUp
  int pileup_is_booked;
  TH1D *hpileup;
  TH1D *hbeam_intensity;

public:

  MHistogramer(const char *name);
  ~MHistogramer();

  void FillPlane(const double *v, const char *append = "");

  void BookVertex();
  void FillVertex(const double* v) const;
  void FillVertex(double x,double y,double z) const;
  void WriteVertex();

  void BookKinematics();
  void FillKinematics(const MSystem & system) const;
  void WriteKinematics();

  void BookRecoilAdvanced();
  void FillRecoilAdvanced(const MSystem & system);
  void WriteRecoilAdvanced();

  void BookPi0();
  void FillPi0(const MDecay2two & pi0);
  void WritePi0();

  void BookPileUp();
  void FillPileUp(double intens, double t1, double t2);
  void WritePileUp();

};

#endif
