//Mikhail Mikhasenko
//1.03.13

#include <iostream>
#include <sstream>
#include <string>

#include "MSystem.h"
#include "mass.h"
#include "heplib.h"

using namespace std;

MSystem::MSystem() {
}

MSystem::MSystem(const double *b, 
		 int n_neg, const double *prtn_n,
		 int n_zer, const double *prtn_z,
		 int n_pos, const double *prtn_p,
		 double m_tagret) {

  memcpy(beam,b,5*sizeof(double));
 
  Nneg = n_neg;
  if(n_neg > MSYSTEM_MAX_N_PARTICLE) { 
    std::cout << "Amount of neganive particles is too many!, = " << n_neg << std::endl;
    Nneg = MSYSTEM_MAX_N_PARTICLE;
  }
  Nzer = n_zer; 
  if(n_zer > MSYSTEM_MAX_N_PARTICLE) { 
    std::cout << "Amount of gamma particles is too many!, = " << n_zer << std::endl;
    Nzer = MSYSTEM_MAX_N_PARTICLE;
  }
  Npos = n_pos; 
  if(n_pos > MSYSTEM_MAX_N_PARTICLE) { 
    std::cout << "Amount of positive particles is too many!, = " << n_neg << std::endl;
    Npos = MSYSTEM_MAX_N_PARTICLE;
  }

  for(int i=0;i<Nneg;i++) memcpy(prt_n[i],prtn_n+5*i,5*sizeof(double)); 
  for(int i=0;i<Nzer;i++) memcpy(prt_z[i],prtn_z+5*i,5*sizeof(double)); 
  for(int i=0;i<Npos;i++) memcpy(prt_p[i],prtn_p+5*i,5*sizeof(double)); 
  for(int i=0;i<5;i++) reso[i] = 0;
  ReCulcReso();

  nucl[0] = 0; nucl[1] = 0; nucl[2] = 0; 
  nucl[3] = m_tagret; nucl[4] = m_tagret;

  ReCulcDown();
  Tmin = TminCalc(beam[3],reso[4],beam[4],nucl[4]);
}

MSystem::MSystem(const double *b, double m_tagret) {
  
  memcpy(beam,b,5*sizeof(double));
  Nneg = 0; Npos = 0; Nzer = 0;
  ReCulcReso();

  nucl[0] = 0; nucl[1] = 0; nucl[2] = 0; 
  nucl[3] = m_tagret; nucl[4] = m_tagret;

  ReCulcDown();
  Tmin = TminCalc(beam[3],reso[4],beam[4],nucl[4]);
}

MSystem::MSystem( const MSystem& other ) {
  Nneg = other.GetNneg();
  Npos = other.GetNpos();
  Nzer = other.GetNzer();

  for(int i=0;i<Nneg;i++) memcpy(prt_n[i],other.GetNegative(i),5*sizeof(double)); 
  for(int i=0;i<Nzer;i++) memcpy(prt_z[i],other.GetNeutral(i),5*sizeof(double)); 
  for(int i=0;i<Npos;i++) memcpy(prt_p[i],other.GetPositive(i),5*sizeof(double)); 

  memcpy(reso,other.freso(),5*sizeof(double));

  memcpy(beam,other.fbeam(),5*sizeof(double));
  memcpy(nucl,other.fnucl(),5*sizeof(double));
  memcpy(recl,other.frecl(),5*sizeof(double));

  memcpy(trns,other.ftrns(),5*sizeof(double));

  Tmin = other.ftmin();
}

void MSystem::AddParticle(int charge, double *v5) {

  if     (charge == +1) { Npos++; memcpy(prt_p[Npos-1],v5,5*sizeof(double)); }
  else if(charge == -1) { Nneg++; memcpy(prt_n[Nneg-1],v5,5*sizeof(double)); }
  else if(charge == 0 ) { Nzer++; memcpy(prt_z[Nzer-1],v5,5*sizeof(double)); }
  else {
    std::cerr << "Error<MSystem::AddParticle>: charge is +1 or -1 or 0" << std::endl;
    return;
  }
  ReCulcReso();
  ReCulcDown();
  Tmin = TminCalc(beam[3],reso[4],beam[4],nucl[4]);
}

void MSystem::AddParticle(int charge, double *v3, double mass) {
  double v5[5]; memcpy(v5,v3,3*sizeof(double));
  hlmass5(v5,v5,mass);
  AddParticle(charge,v5);
}

void MSystem::AddParticle(int charge, double px, double py, double pz, double mass) {
  double v5[5] = {px,py,pz,0,0};
  hlmass5(v5,v5,mass);
  AddParticle(charge,v5);
}

void MSystem::ReCulcReso() {
  for(int i=0;i<5;i++) reso[i] = 0;
  for(int i=0;i<Nneg;i++) hladdt5(prt_n[i],reso,reso);
  for(int i=0;i<Nzer;i++) hladdt5(prt_z[i],reso,reso);
  for(int i=0;i<Npos;i++) hladdt5(prt_p[i],reso,reso); 
};

void MSystem::ReCulcBeamVector (double m_target,double m_recoil) {
  ReCulcBeamVector(m_target,m_recoil,
		   beam,reso,beam);
  SpecifyDownMass(m_target);
  ReCulcDown();
  Tmin = TminCalc(beam[3],reso[4],beam[4],nucl[4],recl[4]);
}

double MSystem::ReCulcBeamVector(double m_target, double m_recoil,
				 const double *beam_v,
				 const double *reso_v,
				 double *beam_out) {
  /* Calculation of beam energy if recoil-target mass constraint is applied */
  /* Square equation */
  /* calculate a_sq b_sq c_sq */
  /* x_sc = vec x * vec b / |b|, x - resonance, b - beam */

  double TARGET_MASS = m_target;
  double RECOIL_MASS = m_recoil;
  
  double p_beam_old = sqrt(hlsclp3(beam_v,beam_v));
  double x_sc = 2*hlsclp3(reso_v,beam_v)/p_beam_old;
  
  double e_sq = 2*(reso_v[3] - TARGET_MASS);
  double d_sq = reso_v[3]*reso_v[3] + hlsclp3(reso_v,reso_v) - beam_v[4]*beam_v[4] +
    (RECOIL_MASS*RECOIL_MASS - TARGET_MASS*TARGET_MASS);
  
  double a_sq = x_sc*x_sc - e_sq*e_sq;
  double b_sq = 2*reso_v[3]*x_sc*x_sc - 2*d_sq*e_sq;
  double c_sq = x_sc*x_sc*(reso_v[3]*reso_v[3]-beam_v[4]*beam_v[4]) - d_sq*d_sq;
  double descr_sq = b_sq*b_sq - 4*a_sq*c_sq;
  double solution = (-b_sq + sqrt(descr_sq)) / (2*a_sq);
  
  double e_beam_calc = solution+reso_v[3];
  double p_beam_calc = sqrt(e_beam_calc*e_beam_calc-beam_v[4]*beam_v[4]);

  if(beam_out) {
    beam_out[0] = beam_v[0]/p_beam_old*p_beam_calc;
    beam_out[1] = beam_v[1]/p_beam_old*p_beam_calc;
    beam_out[2] = beam_v[2]/p_beam_old*p_beam_calc;
    beam_out[3] = e_beam_calc;
    beam_out[4] = beam_v[4];
  }
  return p_beam_calc;
}

void MSystem::ReCulcDown() {
  hlsubt5(beam, reso, trns);
  hladdt5(nucl, trns, recl);
}

double MSystem::TminCalc(double e_beam, double m_res,
			 double m_beam, double m_target, 
			 double m_recoil) {

  double s = m_beam*m_beam + m_target*m_target + 2*e_beam*m_target;
  double first_term = 
    -s/2*(1-
	  (m_beam*m_beam+m_target*m_target+m_res*m_res+m_recoil*m_recoil)/s+
	  (m_beam*m_beam-m_target*m_target)*(m_res*m_res-m_recoil*m_recoil)/s/s
	  );
  double p0_star = sqrt((s-(m_beam+m_target)*(m_beam+m_target))*
			(s-(m_beam-m_target)*(m_beam-m_target))/s)/2;
  double p2_star = sqrt((s-(m_res+m_recoil)*(m_res+m_recoil))*
			(s-(m_res-m_recoil)*(m_res-m_recoil))/s)/2;
  double t_min = first_term+2*p0_star*p2_star;
  return t_min;

//  double p_beam = sqrt(e_beam*e_beam - m_beam*m_beam);
//  double a_t_min = p_beam*p_beam/(down_mass*down_mass)-
//    (e_beam/down_mass+1)*(e_beam/down_mass+1);
//  double b_t_min = 4*e_beam/down_mass*p_beam*p_beam - 
//    2*(2*e_beam*e_beam - m_beam*m_beam - m_res*m_res)*(e_beam/down_mass+1);
//  double c_t_min = 4*p_beam*p_beam*(e_beam*e_beam - m_res*m_res) - 
//    (2*e_beam*e_beam - m_beam*m_beam - m_res*m_res)*
//    (2*e_beam*e_beam - m_beam*m_beam - m_res*m_res);
//  double discr_t_min = b_t_min*b_t_min - 4*a_t_min*c_t_min;
//  double t_min = (-b_t_min - sqrt(discr_t_min))/(2*a_t_min);
//  if(t_min > 0)
//    std::cout << "Error: t_min more then zero!"
//	      << " t_min = " << t_min
//	      << " m = " << m_res
//	      << " e_b = " << e_beam
//	      << std::endl;
//  if(t_min < -1)      
//    std::cout << "Warning: t_min is huge" 
//	      << " t_min = " << t_min
//	      << " m = " << m_res
//	      << " e_b = " << e_beam
//	      << std::endl;
//  return t_min;
}

void MSystem::SpecifyDownMass(double down_mass) {
  hlmass5(nucl,nucl,down_mass);
  hladdt5(nucl, trns, recl);
};

void MSystem::SpecifyBeamMass(double beam_mass) {
  hlmass5(beam,beam,beam_mass);
  hlsubt5(beam, reso, trns);
  hladdt5(nucl, trns, recl);
  Tmin = TminCalc(beam[3],reso[4],beam[4],nucl[4]);
};

void MSystem::Turn(const double *ref) {

  //std::cout << Nneg << Npos << Nzer << std::endl;

  hltrn3(beam,beam,ref);  

  for(int i=0;i<Nneg;i++) hltrn3(prt_n[i],prt_n[i],ref);
  for(int i=0;i<Npos;i++) hltrn3(prt_p[i],prt_p[i],ref);
  for(int i=0;i<Nzer;i++) hltrn3(prt_z[i],prt_z[i],ref);

  hltrn3(reso,reso,ref);
  hltrn3(nucl,nucl,ref);

  ReCulcDown();
}

void MSystem::toLabSys(const double phi_lab) {
  double beam_ref[3] = {nucl[0],
			nucl[1],
			nucl[2]};

  double gamma_gd = nucl[3]/nucl[4];

  hltrn3(beam,beam,beam_ref);hlbstz5(beam,beam,gamma_gd); 
  for(int i=0;i<Npos;i++) { hltrn3(prt_p[i],prt_p[i],beam_ref);hlbstz5(prt_p[i],prt_p[i],gamma_gd);hltrn3(prt_p[i],prt_p[i],beam); }
  for(int i=0;i<Nneg;i++) { hltrn3(prt_n[i],prt_n[i],beam_ref);hlbstz5(prt_n[i],prt_n[i],gamma_gd);hltrn3(prt_n[i],prt_n[i],beam); }
  for(int i=0;i<Nzer;i++) { hltrn3(prt_z[i],prt_z[i],beam_ref);hlbstz5(prt_z[i],prt_z[i],gamma_gd);hltrn3(prt_z[i],prt_z[i],beam); }
  hltrn3(nucl,nucl,beam_ref);hlbstz5(nucl,nucl,gamma_gd);hltrn3(nucl,nucl,beam);
  hltrn3(reso,reso,beam_ref);hlbstz5(reso,reso,gamma_gd);hltrn3(reso,reso,beam);
  hltrn3(beam,beam,beam);

  hlrotz3(beam,beam,phi_lab);
  for(int i=0;i<Npos;i++) hlrotz3(prt_p[i],prt_p[i], phi_lab);
  for(int i=0;i<Nneg;i++) hlrotz3(prt_n[i],prt_n[i], phi_lab);
  for(int i=0;i<Nzer;i++) hlrotz3(prt_z[i],prt_z[i], phi_lab);
  hlrotz3(reso,reso,phi_lab);
  hlrotz3(nucl,nucl,phi_lab);

  ReCulcDown();
}


void MSystem::toGJSys() {

  double gamma_gd = reso[3]/reso[4];

  if(gamma_gd>1) {
    hltrn3(beam,beam,reso);hlbstz5(beam,beam,gamma_gd); 
    for(int i=0;i<Npos;i++) { hltrn3(prt_p[i],prt_p[i],reso);hlbstz5(prt_p[i],prt_p[i],gamma_gd);hltrn3(prt_p[i],prt_p[i],beam); }
    for(int i=0;i<Nneg;i++) { hltrn3(prt_n[i],prt_n[i],reso);hlbstz5(prt_n[i],prt_n[i],gamma_gd);hltrn3(prt_n[i],prt_n[i],beam); }
    for(int i=0;i<Nzer;i++) { hltrn3(prt_z[i],prt_z[i],reso);hlbstz5(prt_z[i],prt_z[i],gamma_gd);hltrn3(prt_z[i],prt_z[i],beam); }
    hltrn3(nucl,nucl,reso);hlbstz5(nucl,nucl,gamma_gd);hltrn3(nucl,nucl,beam);
    hltrn3(reso,reso,reso);hlbstz5(reso,reso,gamma_gd);hltrn3(reso,reso,beam);
    hltrn3(beam,beam,beam);
  }
  double phi_nucl = atan2(nucl[1],nucl[0]);
  hlrotz3(beam,beam, phi_nucl);
  for(int i=0;i<Npos;i++) hlrotz3(prt_p[i],prt_p[i], phi_nucl);
  for(int i=0;i<Nneg;i++) hlrotz3(prt_n[i],prt_n[i], phi_nucl);
  for(int i=0;i<Nzer;i++) hlrotz3(prt_z[i],prt_z[i], phi_nucl);
  hlrotz3(nucl,nucl, phi_nucl);
  hlrotz3(reso,reso, phi_nucl);
 
  ReCulcDown();
}


void MSystem::printInfo() const {
  ostringstream out;
  std::cout << "reso = (" 
	    << reso[0] << ", "
	    << reso[1] << ", "
	    << reso[2] << ", "
	    << reso[3] << ", "
	    << reso[4] << ") \n";
  std::cout << "beam = ("
	    << beam[0] << ", "
	    << beam[1] << ", "
	    << beam[2] << ", "
	    << beam[3] << ", "
	    << beam[4] << ") \n";
  std::cout << "trns = (" 
	    << trns[0] << ", "
	    << trns[1] << ", "
	    << trns[2] << ", "
	    << trns[3] << ", "
	    << trns[4] << ") \n";
  std::cout << "nucl = (" 
	    << nucl[0] << ", "
	    << nucl[1] << ", "
	    << nucl[2] << ", "
	    << nucl[3] << ", "
	    << nucl[4] << ") \n";
  std::cout << "recl = (" 
	    << recl[0] << ", "
	    << recl[1] << ", "
	    << recl[2] << ", "
	    << recl[3] << ", "
	    << recl[4] << ")" << std::endl;
}
 
void MSystem::Print() const {
  std::cout << "reso = (" 
	    << reso[0] << ", "
	    << reso[1] << ", "
	    << reso[2] << ", "
	    << reso[3] << ", "
	    << reso[4] << ") \n";
  std::cout << "beam = ("
	    << beam[0] << ", "
	    << beam[1] << ", "
	    << beam[2] << ", "
	    << beam[3] << ", "
	    << beam[4] << ") \n";
  std::cout << "trns = (" 
	    << trns[0] << ", "
	    << trns[1] << ", "
	    << trns[2] << ", "
	    << trns[3] << ", "
	    << trns[4] << ") \n";
  std::cout << "nucl = (" 
	    << nucl[0] << ", "
	    << nucl[1] << ", "
	    << nucl[2] << ", "
	    << nucl[3] << ", "
	    << nucl[4] << ") \n";
  std::cout << "recl = (" 
	    << recl[0] << ", "
	    << recl[1] << ", "
	    << recl[2] << ", "
	    << recl[3] << ", "
	    << recl[4] << ")" << std::endl;
}

