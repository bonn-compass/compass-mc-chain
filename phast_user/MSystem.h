//Mikhail Mikhasenko
//1.03.13

#ifndef _MSYSTEM_H_
#define _MSYSTEM_H_

#include "mass.h"
#include <string>
#include <cmath>
#include <memory.h>

#define MSYSTEM_MAX_N_PARTICLE 50

class MSystem {
protected:
  double beam[5];
  double nucl[5];
  double recl[5];

  double reso[5];
  double trns[5];

  double Tmin;

  int Nneg;
  int Npos;
  int Nzer;
  double prt_n[MSYSTEM_MAX_N_PARTICLE][5];
  double prt_p[MSYSTEM_MAX_N_PARTICLE][5];
  double prt_z[MSYSTEM_MAX_N_PARTICLE][5];

  void ReCulcDown();

public:
  MSystem();

  MSystem(const double *b,
	  int n_neg, const double *prtn_n,
	  int n_zer = 0, const double *prtn_z = 0,
	  int n_pos = 0, const double *prtn_p = 0,
	  double m_tagret = PROT_MASS);
  MSystem(const double *b,
	  double m_tagret = PROT_MASS);
  MSystem( const MSystem& other );
  
  void ReCulcReso();
  virtual void ReCulcBeamVector(double m_target = PROT_MASS,
				double m_recoil = PROT_MASS);
  static double ReCulcBeamVector (double down_mass, double recoil_mass,
				  const double *beam_v,
				  const double *reso_v,
				  double *beam_out = 0);
  static double TminCalc(double e_beam, double m_res,
			 double m_beam = PI_MASS, 
			 double m_target = PROT_MASS, 
			 double m_recoil = PROT_MASS);
  void Turn(const double *ref);
  
  int GetNneg() const {return Nneg;};
  int GetNzer() const {return Nzer;};
  int GetNpos() const {return Npos;};

  const double *GetPositive(int i) const {return (i<Npos) ? prt_p[i] : 0;}; 
  const double *GetNegative(int i) const {return (i<Nneg) ? prt_n[i] : 0;}; 
  const double *GetNeutral (int i) const {return (i<Nzer) ? prt_z[i] : 0;}; 

  const double* fbeam() const {return beam;};
  const double* fnucl() const {return nucl;};
  const double* frecl() const {return recl;};
  const double* freso() const {return reso;};
  const double* ftrns() const {return trns;};
  double fq_sq() const {return trns[4]*fabs(trns[4]);};
  double ftmin() const {return Tmin;};

  void SpecifyDownMass(double down_mass);
  void SpecifyBeamMass(double beam_mass);
  void AddParticle(int charge, double *vector5);
  void AddParticle(int charge, double *vector3, double mass);
  void AddParticle(int charge, double px, double py, double pz, double mass);

  virtual void toLabSys(const double phi_lab);
  virtual void toGJSys();

  void printInfo() const;
  void Print() const;
  
};
#endif
