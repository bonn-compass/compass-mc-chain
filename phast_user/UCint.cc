// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME UCint

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "UserInfo.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_UserInfo(void *p = 0);
   static void *newArray_UserInfo(Long_t size, void *p);
   static void delete_UserInfo(void *p);
   static void deleteArray_UserInfo(void *p);
   static void destruct_UserInfo(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::UserInfo*)
   {
      ::UserInfo *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::UserInfo >(0);
      static ::ROOT::TGenericClassInfo 
         instance("UserInfo", ::UserInfo::Class_Version(), "UserInfo.h", 18,
                  typeid(::UserInfo), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::UserInfo::Dictionary, isa_proxy, 4,
                  sizeof(::UserInfo) );
      instance.SetNew(&new_UserInfo);
      instance.SetNewArray(&newArray_UserInfo);
      instance.SetDelete(&delete_UserInfo);
      instance.SetDeleteArray(&deleteArray_UserInfo);
      instance.SetDestructor(&destruct_UserInfo);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::UserInfo*)
   {
      return GenerateInitInstanceLocal((::UserInfo*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::UserInfo*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr UserInfo::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *UserInfo::Class_Name()
{
   return "UserInfo";
}

//______________________________________________________________________________
const char *UserInfo::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::UserInfo*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int UserInfo::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::UserInfo*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *UserInfo::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::UserInfo*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *UserInfo::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::UserInfo*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void UserInfo::Streamer(TBuffer &R__b)
{
   // Stream an object of class UserInfo.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(UserInfo::Class(),this);
   } else {
      R__b.WriteClassBuffer(UserInfo::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_UserInfo(void *p) {
      return  p ? new(p) ::UserInfo : new ::UserInfo;
   }
   static void *newArray_UserInfo(Long_t nElements, void *p) {
      return p ? new(p) ::UserInfo[nElements] : new ::UserInfo[nElements];
   }
   // Wrapper around operator delete
   static void delete_UserInfo(void *p) {
      delete ((::UserInfo*)p);
   }
   static void deleteArray_UserInfo(void *p) {
      delete [] ((::UserInfo*)p);
   }
   static void destruct_UserInfo(void *p) {
      typedef ::UserInfo current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::UserInfo

namespace {
  void TriggerDictionaryInitialization_UCint_Impl() {
    static const char* headers[] = {
"UserInfo.h",
0
    };
    static const char* includePaths[] = {
"/localhome/mikhasenko/Tools/phast.7.148/lib/",
"/localhome/mikhasenko/Tools/root-git/localbuild/include",
"/localhome/mikhasenko/Tools/phast.7.148/user/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "UCint dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$UserInfo.h")))  UserInfo;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "UCint dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "UserInfo.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"UserInfo", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("UCint",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_UCint_Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_UCint_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_UCint() {
  TriggerDictionaryInitialization_UCint_Impl();
}
