#include <iostream>
#include <cmath>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "G3part.h"

//
// It's an example 
// of user's event selection 
// routine and analysis 
// (K0, K*(892) study)
//
// Output of this example are some
// histograms and simple Ntuple ("K0")
// which you may find in output histogram file
// (hist.root by default)
// ROOT script root/Kstar.C is an example of
// how to get some plots out of this Ntuple.
// 
// For your personal stuff
// just copy this file
// to UserEventN.cc
// (where N is not yet used number)
// rename function to 'UserEventN'
// and pay attention to Ntuple
// and histogram names: they has to be unique. 
// 
// If you want current event to be saved to output tree 
// (microDST) call at least once "e.TagToSave()" function
//

// example of prototype of FORTRAN function 
// (from "fortran" directory or from CERNLIB)
extern "C" float prob_(float&, int&);

void UserEvent0(PaEvent& e)
{
  // Example of calling CERNLIB PROB function
  // float chi=10; int ndf=10;  
  // cout<<"PROB(10,10) = "<<prob_(chi, ndf)<<endl;

  // constants
  const double M_pi = G3partMass[8];
  const double M_K0 = G3partMass[16];
  const double M_KS = G3partMass[61];

  static TH1D* h1[50];  
  // static TH2D* h2[50];  
  static TTree* tree(NULL);

  //
  // Variables to be stored into analysis Ntuple
  //
  // (if you want to extend the list:
  // - add variable here,
  // - add in Ntuple definition below 
  // and do not forget to fill the variable in the code)
  //
  static int   Run;     // run number
  static int   Evt;     //  event number
  static float Zprim;   // Z coordinate of primary vertex (10000 if not found)
  static int   Nprim;   // Number of tracks in primary vertex (-1 in fot found)
  static float Ch2prim; // Chi2 of primary vertex
  static float K0mass;  // pi+pi- invariant mass
  static float K0z;     // Z coordinate of K0 candidate vertex
  static float Ch2V0;   // Chi2 if K0 vertex
  static float K0mom;   // K0 candidate |P|
  static float K0PIcos; // K0's pi cos(theta)
  static float KSmass;  // "K0 pi" invariant mass
  static float KSmas1;  // "K0 pi" invariant mass if PDG K0 mas is taken
  static float PImom;   // PI momentum in "K0 pi" combination
  static float PIcos;   // cos(theta) of PI in "K0 pi"
 

  
  //---------------

  static bool first(true);
  if(first){ // histograms and Ntupes booking block
    Phast::Ref().HistFileDir("UserEvent0");
   
    // 1-d 
    h1[2]  = new TH1D("h02","V0 candidate Z (cm)",       100, -250, 250);
    h1[3]  = new TH1D("h03","V0 candidate Z (cm). Zoom", 200,  -100,  0);
    
    h1[10] = new TH1D("h10","M(pi+pi-) - M(K0) [Mev]. #DelatZ > 50",             100, -100, 100);
    h1[11] = new TH1D("h11","M(pi+pi-) - M(K0) [Mev]. #DelatZ > 50 (per event)", 100, -100, 100);
    
    // 2-d
    // h2[1]  = new TH2D("2d_h01", "Y vertex VS X vertex", 50,-5, 5, 50, -5, 5);

    //
    // Ntuple definition 
    //
    tree = new TTree("USR0","User Ntuple example"); // name (has to be unique) and title of the Ntuple
    
    tree->Branch("Run",    &Run,    "Run/I");
    tree->Branch("Evt",    &Evt,    "Evt/I");
    tree->Branch("Zprim",  &Zprim,  "Zprim/F");
    tree->Branch("Nprim",  &Nprim,  "Nprim/I");
    tree->Branch("Ch2prim",&Ch2prim,"Chi2prim/F");
    tree->Branch("K0mass", &K0mass, "K0mass/F");
    tree->Branch("K0z",    &K0z,    "K0z/F");
    tree->Branch("Ch2V0",  &Ch2V0,  "Ch2V0/F");
    tree->Branch("K0mom",  &K0mom,  "K0mom/F");
    tree->Branch("K0PIcos",&K0PIcos,"K0PIcos/F");
    tree->Branch("KSmass", &KSmass, "KSmass/F");
    tree->Branch("KSmas1", &KSmas1, "KSmas1/F");
    tree->Branch("PImom",  &PImom,  "PImom/F");
    tree->Branch("PIcos",  &PIcos,  "PIcos/F");
    
    first=false;
  } // end of histogram booking
  
  Run        =  e.RunNum();
  Evt        =  e.UniqueEvNum();
  int Bvtx   =  e.iBestPrimaryVertex();

  // store info. about primary vertex (if found)
  Zprim   = 100000;
  Ch2prim = -1;
  Nprim   = -1;
  for(int iv = 0; iv < e.NVertex(); iv++){ // loop over reconstructed vertices
    const PaVertex& v = e.vVertex(iv);
    if(! v.IsPrimary()) continue; // not primary. Skip.
    if(Bvtx !=-1 && Bvtx != iv) continue; // "best primary exists" but not current one
    Zprim   = v.Pos(2);
    Ch2prim = v.Chi2();
    Nprim = v.NOutParticles(); // number of tracks in vertex 
  } // end of loop over vertices


  //
  // K0 search
  //
  
  bool k0_candidate(false);
 
  for(int iv = 0; iv < e.NVertex(); iv++){ // loop over reconstructed vertices
  
    k0_candidate = false;
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) continue;              // Primary. Skip.
    if(v.NOutParticles() != 2)     continue; // look on V0s only
    K0z    = v.Pos(2);
    Ch2V0  = v.Chi2();
    int ip1 = v.iOutParticle(0);
    int ip2 = v.iOutParticle(1);
    PaParticle p1 = e.vParticle(ip1);
    PaParticle p2 = e.vParticle(ip2);
    if(p1.NFitPar() == 0 || p2.NFitPar() == 0) continue; // skip if no fited parameters in vertex
    if(p1.Q() == p2.Q())                       continue; // skip equal charge pairs    
    if(p1.Q() < p2.Q()) swap(p1,p2);   // now first particle is positive
    h1[2]->Fill(K0z);  h1[3]->Fill(K0z); 
 
    //
    // calculate pi+pi- invariant mass
    //
    TLorentzVector lv1 = p1.ParInVtx(iv).LzVec(M_pi); // assign pi mass to particle #1
    TLorentzVector lv2 = p2.ParInVtx(iv).LzVec(M_pi); // assign pi mass to particle #2
    TLorentzVector lv = lv1+lv2; // Lorentz vector of pi+pi- system
    K0mass = lv.M();
    K0mom = (lv.Vect()).Mag();

    // Theta angle of pi in K0 rest frame
    TVector3 b = -lv.BoostVector();          // boost vector of p1+p2 system
    TLorentzVector lv1p(lv1); lv1p.Boost(b); // transform p1 4vector to p1+p2 system
    TLorentzVector lv2p(lv2); lv2p.Boost(b); // transform p2 4vector to p1+p2 system
    K0PIcos = cos(lv.Angle(lv1p.Vect()));
    
    if(K0z - Zprim >  50) {             // decay pass cut
      h1[10]->Fill(1000*(lv.M()-M_K0));
      h1[11]->Fill(1000*(lv.M()-M_K0)); // 2-d copy (to be normalized in UserJobEnd)
    }

    // mass cuts for K0 candidate selection
    if(fabs(K0mass-M_K0) < 0.1) k0_candidate=true;

    //
    // Search K*(892) -> K0 pi
    //
    bool ks_candidate;

    for(int ip = 0; ip < e.NParticle(); ip++) { // loop over all particels in the event
      if(ip == ip1) continue;
      if(ip == ip2) continue;

    
      const PaParticle& p = e.vParticle(ip);           // pion candidate
 
      if(p.NVertex() == 0) continue;                   // pion not assosiated to vertex
      for(int ivpi = 0; ivpi < p.NVertex(); ivpi++) {  // loop over vertices pion is assosiated
    
	ks_candidate=false;
	KSmass = -1; 
	KSmas1 = -1; 
	PImom  = -1000;
	PIcos  =-10;
	
	int jv = p.iVertex(ivpi);                     // vertex id (in event's vVertex)
	const PaVertex& vpi = e.vVertex(jv);
 	if(!vpi.IsPrimary()) continue;                // Non primary. Skip.
	if(Bvtx != -1 && Bvtx != jv) continue;        // "best primary exists" but not this one
	if(e.vVertex(jv).Pos(2) > K0z) continue;      // pi vertex is more downstream then K0
	PImom = p.ParInVtx(jv).qP();  
	
	// "K0 pi" invariant mass
	TLorentzVector lpi = p.ParInVtx(jv).LzVec(M_pi);
	TLorentzVector lks = lv+lpi;
	KSmass = lks.M();
	
	// "K0 pi" invariant mass, if to asume PDG K0 mass
	TLorentzVector lv1(lv);
	lv1.SetVectM(lv.Vect(),M_K0);
	KSmas1 = (lv1+lpi).M();
	
	// PI cos(theta) in (K0 pi) cms
	TVector3 b = -lks.BoostVector();
	TLorentzVector lpip(lpi); lpip.Boost(b);
	PIcos = cos(lks.Angle(lpip.Vect()));
	
	// cuts for K*(892) candidate selection
	if(fabs(KSmass-M_KS) < 0.3) ks_candidate = true;
	

	// save Ntuple if there is K0 and K* candidates
	// (one entry per "vertex pi" combination)
	
	if(k0_candidate && ks_candidate) {
	  tree->Fill(); // fill Ntuple with K* candidate
	  Phast::Ref().h_file = tree->GetCurrentFile(); // "refresh" pointer to histogram file
	}
	
      } // end of loop over vertices where pion candidate had been fitted
    } // and of loop over all particles in the event
  } // end of loop over vertices

  //
  // Event selection 
  //
  // if output stream is specified, current event will be saves
  // if TagToSave() function had been called at least once.

  // For example: here we will save events 
  // with at least one K0 candidate 
  if(k0_candidate) e.TagToSave();
}














