#include <iostream>
#include <cmath>
#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "PaAlgo.h"
#include "G3part.h"

//
// It's an example 
// of user's function
// with mu-scattering related 
// kinematical variables calculations
// and event selection.
//
// Output of this example are some
// histograms and simple Ntuple
// which you may find in output histogram file
// (hist.root by default)
// 
//
// If you want current event to be saved to output tree 
// (microDST) call at least once "e.TagToSave()" function.
//
// If you use this example like a starting point 
// for your analysies (i.e. you had copied this UserEvent1.cc
// to some UseEventN.cc, N = 2,3...20) do not forget to change:
// - function name below
// - all histogram IDs from usr1_... to usrN_...
// - Ntuple name from USR1 to USRN
//
//

// 08-03-2004, Andrea Ferrero <aferrero@to.infn.it>
//   Added example code for MW1 muon reconstruction. Scattered muons candidates are identified by 
// MW1 for calorimetric trigger events, and DIS variables are calculated. The J/Psi invariant mass for mu+/mu- identified in 
// MW1 is also calculated.

void UserEvent1(PaEvent& e)
{

  const double M_mu = G3partMass[5];  // muon   mass
  const double M_pi = G3partMass[8];  // pion   mass
  const double M_Pr = G3partMass[14]; // proton mass

  int method = 1;



  static TH1D*     h1[50];  
  static TTree* tree(NULL);
  
  //
  // Variables to be stored into analysis Ntuple
  //
  // (if you want to extend the list:
  // - add variable here,
  // - add in Ntuple definition below 
  // and do not forget to fill the variable in the code)
  //
  static int   Run;     // Run number
  static float Xprim;   // X coordinate of primary vertex
  static float Yprim;   // Y coordinate of primary vertex
  static float Zprim;   // Z coordinate of primary vertex
  static int   Nout;    // Number of tracks in primary vertex
  static float Ch2prim; // Chi2 of primary vertex
  static float Y;       // Y
  static float W;       // W2
  static float Xf;      // Xf
  static float Q2;      // Q2
  static float Xbj;     // Xbjorken
  static float Mmiss;   // Missing mass
  static float Df;      // Depolarization factor

  W=Xf=Q2=Xbj=-10000.; // some defaults 

  //---------------
  static bool first(true);
  if(first){ // histograms and Ntupes booking block
    Phast::Ref().HistFileDir("UserEvent1");

    // 1-d histopgrams 
    h1[1]  = new TH1D("h01","Primary vertex Z position (cm)",       100, -250, 250);
    h1[2]  = new TH1D("h02","N outgoing tracks in vertex",   20,    0,  20);
    h1[3]  = new TH1D("h03","W",                            100,    0,  20);
    h1[4]  = new TH1D("h04","Xf of 'leading' hardron",      100,   -1,   2);
    h1[5]  = new TH1D("h05","Missing energy",               100,   -20, 20);
    h1[6]  = new TH1D("h06","Y",                            110,   -0.1, 1);

    h1[21]  = new TH1D("h21","J/#Psi",150,2,5);


    // variable bin histograms
    Float_t xbins[101];
    for(int i=0; i<101; i++ ) xbins[i] = 1e-4 * pow(10,0.05*i); 
    h1[10] = new TH1D("h10","Q^{2}",  100, xbins);
    for(int i=0; i<101; i++ ) xbins[i] = 1e-5 * pow(10,0.05*i); 
    h1[11] = new TH1D("h11","Xbj",  100, xbins);

    // Kinematic variables histos
    for(int i=0; i<101; i++ ) xbins[i] = 1e-3 * pow(10,0.05*i); 
    h1[12] = new TH1D("h12","Q^{2}",  100, xbins);
    for(int i=0; i<101; i++ ) xbins[i] = 1e-5 * pow(10,0.05*i); 
    h1[13] = new TH1D("h13","Xbj",  100, xbins);
    h1[14] = new TH1D("h14","Y", 110, -0.1, 1);
    // Ntuple definition 

    //
    tree = new TTree("USR1","User Ntuple example"); // name (has to be unique) and title of the Ntuple
    
    tree->Branch("Run",    &Run,    "Run/I");
    tree->Branch("Xprim",  &Xprim,  "Xprim/F");
    tree->Branch("Yprim",  &Yprim,  "Yprim/F");
    tree->Branch("Zprim",  &Zprim,  "Zprim/F");
    tree->Branch("Nout",   &Nout,   "Nout/I");
    tree->Branch("Ch2prim",&Ch2prim,"Chi2prim/F");
    tree->Branch("Y",      &W,      "Y/F");
    tree->Branch("W",      &W,      "W/F");
    tree->Branch("Xf",     &Xf,     "Xf/F");
    tree->Branch("Q2",     &Q2,     "Q2/F");
    tree->Branch("Xbj",    &Xbj,    "Xbj/F");
    tree->Branch("Mmiss",  &Mmiss,  "Mmiss/F");
    tree->Branch("Df",     &Df,     "Df/F");

    first=false;
  } // end of histogram booking
  //---------------

  Run = e.RunNum();

  if(method == 1) { // method # 1 (starting from vertex loop)

    for(int iv = 0; iv < e.NVertex(); iv++){  // loop over reconstructed vertices
      const PaVertex& v = e.vVertex(iv);   // "v" now is "synonym" for "vertex # iv" 

      // select vertices with beam and mu' 
      if(! v.IsPrimary()) continue;        // skip not primary vertex.
      int imu0 = v.InParticle();           // get beam      mu index
      int imu1 = v.iMuPrim();              // get scattered mu index
      if(imu0 == -1)    continue;          // skip if there is no beam (same as "if not primary")      
      if(imu1 == -1)    continue;          // skip if there is no mu'

      // get mu, mu' Lorentz vectors at primary vertex 
      const PaParticle& Mu0   = e.vParticle(imu0);    // it's beam muon 
      const PaParticle& Mu1   = e.vParticle(imu1);    // it's scattered muon
      const PaTPar& ParamMu0  = Mu0.ParInVtx(iv);     // fitted mu  parameters in the primary vertex
      const PaTPar& ParamMu1  = Mu1.ParInVtx(iv);     // fitted mu' parameters in the primary vertex
      TLorentzVector LzVecMu0 = ParamMu0.LzVec(M_mu); // calculate beam      mu Lorentz vector
      TLorentzVector LzVecMu1 = ParamMu1.LzVec(M_mu); // calculate scattered mu Lorentz vector
    
      // calculate some kinematic variables
      Y   = (LzVecMu0.E()-LzVecMu1.E())/LzVecMu0.E();
      Q2  =      PaAlgo::Q2 (LzVecMu0, LzVecMu1);
      Xbj =      PaAlgo::xbj(LzVecMu0, LzVecMu1);
      W   = sqrt(PaAlgo::W2 (LzVecMu0, LzVecMu1));
      double D = 0; double dD = 0;
#ifndef NO_FORTRAN
      PaAlgo::GetDepolarizationFactor(double(Q2), double(Xbj), double(Y), D, dD, true);
#else
      double R  = 0.1;  // meaningless. just an example 
      double dR = 0.01; // meaningless. just an example 
      PaAlgo::GetDepolarizationFactor(double(Q2), double(Xbj), double(Y), R, dR, D, dD, true);
#endif
      if(false) cout<<" Depolarization factor = "<<D<<" +- "<<dD<<endl;
      Df = D; // save to ntuple

      // have a look on other particles in the vertex
      PaTPar MaxMomParam; double maxP = 0; int ih=-1;
      TLorentzVector LzVecOut(0,0,0,0); // for sum 4-vectors of all created particles  
      for(int j = 0; j < v.NOutParticles(); j++) { // loop over outgoing paticles of the vertex
	int ip = v.iOutParticle(j);     // index of outgoing particle
	const PaParticle& p = e.vParticle(ip);
	if(p.IsMuPrim()) continue;      // skip mu'
	const PaTPar& param   = p.ParInVtx(iv);
	LzVecOut += param.LzVec(M_pi);  // add 4-vector to sum
	if(param.Mom() > maxP) { // search max. mom. particle
	  maxP = param.Mom();
	  ih=ip;
	  MaxMomParam = param;
	}
      } // end of loop over outgoing particles


      TLorentzVector q =  LzVecMu0 -  LzVecMu1; // virtual photon's 4-vector 
      TLorentzVector p(0,0,0,M_Pr);             // proton's 4-vector (in rest) 
      double Mx2    =  (LzVecMu0 + p - (LzVecMu1+LzVecOut)).M2(); 
      Mmiss  = (Mx2 - M_Pr*M_Pr)/(2*M_Pr);
      
      if(ih != -1) { // "leading" hardron had found
	TLorentzVector LzVecHardron = MaxMomParam.LzVec(M_pi);  
	Xf = PaAlgo::Xf(LzVecMu0, LzVecMu1, LzVecHardron);
	h1[4]->Fill(Xf); // fill Xf of hardron
      }

      h1[1] ->Fill(v.Pos(2));          // fill vertex Z position
      h1[2] ->Fill(v.NOutParticles()); // histogram number of outgoing particles in the vertex
      h1[3] ->Fill(W);                 // fill W histogarm
      h1[5]->Fill(Mmiss);
      h1[6]->Fill(Y);
      h1[10]->Fill(Q2);                // fill Q2 histogarm
      h1[11]->Fill(Xbj);               // fill Xbj histogarm
      
      // fill N-tuple's variables.
      Xprim   = v.Pos(0);
      Yprim   = v.Pos(1);
      Zprim   = v.Pos(2);
      if(isnan(Zprim)) cout<<"Zprim is nan (1)!"<<endl;
      Ch2prim = v.Chi2();
      Nout    = v.NOutParticles();

      tree->Fill(); // store Ntuple entry.
      Phast::Ref().h_file = tree->GetCurrentFile(); // "refresh" pointer to histogram file

    } // end of loop over vertices


  } else { // Method # 2 (starting from particles loop)
 

    for(int ip0 = 0; ip0 < e.NParticle(); ip0++){  // loop over reconstructed particles
      const PaParticle& p0 = e.vParticle(ip0);
      if(abs(p0.Q()) != 1) continue;
      if(! p0.IsBeam())     continue; // skip if it's not the beam

      for(int i=0; i < p0.NVertex(); i++) { // loop over vertices this beam associated with 
	int iv = p0.iVertex(i);             // get vertex index
	const PaVertex& v = e.vVertex(iv);  

	for(int j = 0; j < v.NOutParticles(); j++) { // loop over outgoing paticles of the vertex
	  int ip = v.iOutParticle(j);  // index of outgoing particle
	  const PaParticle& p = e.vParticle(ip);
	  if(! p.IsMuPrim()) continue; // skip if it's not mu'

	  const PaTPar& ParMu0  = p0.ParInVtx(iv);      // fitted mu  parameters in the primary vertex
	  const PaTPar& ParMu   = p .ParInVtx(iv);      // fitted mu' parameters in the primary vertex
	  TLorentzVector LzVecMu0 = ParMu0.LzVec(M_mu); // calculate beam      mu Lorentz vector
	  TLorentzVector LzVecMu  = ParMu .LzVec(M_mu); // calculate scattered mu Lorentz vector

	  // calculate some kinematic variables
	  Q2  =      PaAlgo::Q2 (LzVecMu0, LzVecMu);
	  Xbj =      PaAlgo::xbj(LzVecMu0, LzVecMu);
	  W   = sqrt(PaAlgo::W2 (LzVecMu0, LzVecMu));
	  
	  // fill histograms
	  h1[1] ->Fill(v.Pos(2));          // fill vertex Z position
	  h1[2] ->Fill(v.NOutParticles()); // histogram number of outgoing particles in the vertex
	  h1[3] ->Fill(W);                 // fill W histogarm
	  h1[10]->Fill(Q2);                // fill Q2 histogarm
	  h1[11]->Fill(Xbj);               // fill Xbj histogarm
	  
	  // fill N-tuple's variables.
	  Xprim   = v.Pos(0);
	  Yprim   = v.Pos(1);
	  Zprim   = v.Pos(2);
	  if(isnan(Zprim)) cout<<"Zprim is nan (2)!"<<endl;
	  Ch2prim = v.Chi2();
	  Nout    = v.NOutParticles();

	  tree->Fill(); // store Ntuple entry.
	  Phast::Ref().h_file = tree->GetCurrentFile(); // "refresh" pointer to histogram file

	} // end of loop over outgoing particles
      } // end of loop over vertices
    } // end of loop over reconstructed particles
  
} // end of method chooser

  /*  
  //  MW1 reconstruction example (by Andrea Ferrero <aferrero@to.infn.it> )
  int iprim = e.iBestPrimaryVertex();
  if(iprim >= 0) {
    const PaVertex& v = e.vVertex(iprim);
    int imu0 = v.InParticle();              // get beam      mu index
    int imu1 = PaAlgo::GetMW1ScatMuon(e);   // get scattered mu index
    if(imu0 > -1 && imu1 > -1) {         // skip if there is no beam or sattered muon.
      // get mu, mu' Lorentz vectors at primary vertex 
      const PaParticle& Mu0   = e.vParticle(imu0);    // it's beam muon 
      const PaParticle& Mu1   = e.vParticle(imu1);    // it's scattered muon
      const PaTPar& ParamMu0  = Mu0.ParInVtx(iprim);     // fitted mu  parameters in the primary vertex
      const PaTPar& ParamMu1  = Mu1.ParInVtx(iprim);     // fitted mu' parameters in the primary vertex
      TLorentzVector LzVecMu0 = ParamMu0.LzVec(M_mu); // calculate beam      mu Lorentz vector
      TLorentzVector LzVecMu1 = ParamMu1.LzVec(M_mu); // calculate scattered mu Lorentz vector
      
      // calculate some kinematic variables
      Y   = (LzVecMu0.E()-LzVecMu1.E())/LzVecMu0.E();
      Q2  = PaAlgo::Q2 (LzVecMu0, LzVecMu1);
      Xbj = PaAlgo::xbj(LzVecMu0, LzVecMu1);
      
      h1[12]->Fill(Q2);                // fill Q2 histogarm
      h1[13]->Fill(Xbj);               // fill Xbj histogarm
      h1[14]->Fill(Y);                 // fill Y histogarm
    }
  }    
  //  end of MW1 reconstruction example (by Andrea Ferrero <aferrero@to.infn.it> )
  */
    

  // It's an example of how to access run-dependent information 

  const PaSetup& s = PaSetup::Ref();              // get PaSetup object
  vector<Float_t>  vecTargPolar = s.vTargPolar(); // get target polarizations
  

  //
  // Event selection 
  //
  // if output stream is specified, current event will be saves
  // if TagToSave() function had been called at least once.

  // For example: here we will save events 
  // with Q2 > something  
  if(Q2 > 0.01) e.TagToSave();
}














