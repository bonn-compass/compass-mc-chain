#include <iostream>
#include <sstream>
#include <cmath>
#include <vector>
#include <bitset>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#include "mass.h"
#include "heplib.h"
#include "MHister.h"

/*
  The program looks to calorimeters.
*/


//bool myfunction (int i,int j) { return (i<j); }

//parameters of cuts, which are applied:
#define N_PV 1
#define NOUT_PART 3
#define ECAL1_TH 0.25
#define ECAL2_TH 1
#define NGAMMA 2
#define QSUM -1
#define TRACK_MOMENTUM_CUT -180

using namespace std;
TLorentzVector prepare_gamma_LV(double x,double y,double z,double E, double v[]);

void UserEvent110(PaEvent& e) {

  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHister h( ( sh_file_name+".mine" ).c_str() );

  static bool first(true);
  if(first){ 
    first=false;
  } 

  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/
  int trig = e.TrigMask();
  std::bitset<12> mask_from_event(trig);
  std::bitset<12> mask; mask[2]=1;
  int ui_mask = mask.to_ulong();
  bool is_kept = trig & ui_mask;
  for(int i=0;i<12;i++) 
    if(mask_from_event[i]) 
      h.Fill1D("trigger",i,1,
	       12,-0.5,11.5,"Trigger;bit");

  /*------------------------------------primary verteces---------------------------------------*/
  vector<int> pv;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { pv.push_back(iv); }
  }
  h.Fill1D( "Npv", pv.size(), 1, 20, -0.5, 19.5, "Amount of PV;Npv" );
  //if( pv.size() < N_PV ) return;
  
  /*--------------------------------------Outgoing particles-----------------------------------*/
  vector<int> op;
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& vi = e.vVertex(pv[i]);
    for(int j=0;j<vi.NOutParticles();j++) {
      double pMom = e.vParticle( vi.iOutParticle(j) ).ParInVtx(pv[i]).qP();
      h.Fill1D("OUTp",pMom,1,
	       250, -250, 250, "Momentum of track;p[GeV]" );
      //if(pMom < TRACK_MOMENTUM_CUT) continue;
      op.push_back( vi.iOutParticle(j) );
    }
  }

  h.Fill1D ("OUTpv", op.size(), 1, 10, -0.5, 9.5, "Amount of OUT-particles from PVs;Np" );
  //if( op.size() != NOUT_PART ) return;

  int sum_of_charges=0;
  for(uint i=0;i<op.size();i++) sum_of_charges += e.vParticle(op[i]).Q();
  h.Fill1D ("qSum", sum_of_charges, 1, 7, -3.5, 3.5, "sum of charges;sum Q" );
  //if( sum_of_charges != QSUM ) return; 
  
  /*-----------------------------------------Calorimetry---------------------------------------*/
  std::vector<int> gammas1,gammas2;
  
  const int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    if(part.Q()==0) {
      if(part.NFitPar()!=0) std::cout<<"Error: part.NFitPar()!=0" << std::endl;
      
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	const PaCaloClus &cl = e.vCaloClus(iClast);
	//std:cout << "Info: " << cl.CalorimName() << " is cal" << cl.iCalorim() << std::endl;
	if( cl.iCalorim() == 0 ) gammas1.push_back(iClast);
	if( cl.iCalorim() == 1 ) gammas2.push_back(iClast);
      }
    }
  }
  
  h.Fill1D("ECAL1m",gammas1.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL1;E[GeV];Nsh");
  h.Fill1D("ECAL2m",gammas2.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL2;E[GeV];Nsh");

  h.Fill1D("ECAL1p2m",gammas1.size()+gammas2.size(), 1, 
	   20,-0.5, 19.5, "Amount of shower ECAL1+ECAL2;E[GeV];Nsh");

  std::vector<int> position_of_good;
  for(uint i=0;i<gammas1.size();i++) 
    if(e.vCaloClus(gammas1[i]).E()>ECAL1_TH) position_of_good.push_back(gammas1[i]);
  for(uint i=0;i<gammas2.size();i++) 
    if(e.vCaloClus(gammas2[i]).E()>ECAL2_TH) position_of_good.push_back(gammas2[i]);

  h.Fill1D("Ngamma_more_th",position_of_good.size(),1,
	   20,-0.5,19.5);

  int NClus=e.NCaloClus();
  std::vector<int> p_from_cl;
  h.Fill1D("NClus",NClus,1,20,-0.5,19.5,"Numbers of clusters; N_{cl}");
  for(int i=0;i<NClus;i++) {
    const PaCaloClus &cl=e.vCaloClus(i);
    h.Fill1D("iCalorim",cl.iCalorim(),1,5,-0.5,4.5);
    int pQ0=0, pQm=0, pQp=0; 
    for(int j=0;j<cl.NParticles();j++) {
      p_from_cl.push_back(cl.iParticle(j));
      if(e.vParticle(cl.iParticle(j)).Q()== 0) pQ0++;
      if(e.vParticle(cl.iParticle(j)).Q()==-1) pQm++;
      if(e.vParticle(cl.iParticle(j)).Q()==+1) pQp++;
    }
    std::ostringstream name[3][4];
    for(int i=0;i<4;i++) {
      name[0][i]<<"partPerClusterIn"<<i<<"WithPlusVsMinus";
      name[1][i]<<"partPerClusterIn"<<i<<"WithZeroVsMinus";
      name[2][i]<<"partPerClusterIn"<<i<<"WithZeroVsPlus";
    }
    h.Fill2D(name[0][cl.iCalorim()].str().c_str(),pQp,pQm,1, 5,-0.5,4.5, 5,-0.5,4.5);
    h.Fill2D(name[1][cl.iCalorim()].str().c_str(),pQ0,pQm,1, 5,-0.5,4.5, 5,-0.5,4.5);
    h.Fill2D(name[2][cl.iCalorim()].str().c_str(),pQ0,pQp,1, 5,-0.5,4.5, 5,-0.5,4.5);

    if(cl.iCalorim() != 0) {
      h.Fill1D("ClusSizeIn0",cl.Size(),1,20,-0.5,19.5,"Size of cluster; N_{cell}");    
      h.Fill2D("ClusSize_vs_EIn0",cl.Size(),cl.E(),1,
	       20,-0.5,19.5,
	       200,0,100,"Size of cluster vs Energy in ECAL1; N_{cell};E(GeV)");
      h.Fill1D("ClusPartIn0",cl.NParticles(),1,20,-0.5,19.5,"Particles in cluster; N_{part}");
    }
    if(cl.iCalorim() != 1) {
      h.Fill1D("ClusSizeIn1",cl.Size(),1,20,-0.5,19.5,"Size of cluster; N_{cell}");    
      h.Fill2D("ClusSize_vs_EIn1",cl.Size(),cl.E(),1,
	       20,-0.5,19.5,
	       200,0,100,"Size of cluster vs Energy; N_{cell};E(GeV)");
      h.Fill1D("ClusPartIn1",cl.NParticles(),1,20,-0.5,19.5,"Particles in cluster; N_{part}");
    }

  }
  
//  std::cout << "------------------ New event ------------------" << std::endl;
//  for(int i=0;i<NClus;i++) {
//    const PaCaloClus &cl=e.vCaloClus(i);
//    std::cout << "E="<< cl.E() << ", Cells:";
//    const vector<int> &content = cl.vCellNumber();
//    for(int j=0;j<content.size();j++)  std::cout << content[j  << ", ";
//    std::cout << std::endl;
//  }  



  //if(position_of_good.size() != NGAMMA) return;

//  /*---------------------------------------Invariant mass--------------------------------------*/   
//
//  double v[] = {e.vVertex(pv[0]).X(),
//		e.vVertex(pv[0]).Y(),
//		e.vVertex(pv[0]).Z()};
//
//  TLorentzVector g1_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[0]).X(),
//					  e.vCaloClus(position_of_good[0]).Y(),
//					  e.vCaloClus(position_of_good[0]).Z(),
//					  e.vCaloClus(position_of_good[0]).E(),v);
//  TLorentzVector g2_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[1]).X(),
//					  e.vCaloClus(position_of_good[1]).Y(),
//					  e.vCaloClus(position_of_good[1]).Z(),
//					  e.vCaloClus(position_of_good[1]).E(),v);
//  
//  TLorentzVector gg_lv=g1_lv+g2_lv;
//  h.Fill1D("gg_mass",gg_lv.M(),1,
//	   200,0,0.7);
//  if( fabs(gg_lv.M()-PI0_MASS) > PI0_CUT ) return;
//
//  if(e.vParticle(op[1]).Q()==+1) swap(op[1],op[0]);
//  if(e.vParticle(op[2]).Q()==+1) swap(op[2],op[0]);
//  
//  const TLorentzVector p1_lv = e.vParticle(op[0]).ParInVtx(pv[0]).LzVec(PI_MASS);
//  const TLorentzVector p2_lv = e.vParticle(op[1]).ParInVtx(pv[0]).LzVec(PI_MASS);
//  const TLorentzVector p3_lv = e.vParticle(op[2]).ParInVtx(pv[0]).LzVec(PI_MASS);
//  TLorentzVector three_pi_lv1 = p1_lv+p2_lv+gg_lv;
//  TLorentzVector three_pi_lv2 = p1_lv+p3_lv+gg_lv;
//  h.Fill1D("pipipi_mass",three_pi_lv1.M(),1, 200,0,1);
//  h.Fill1D("pipipi_mass",three_pi_lv2.M()           );
//  if( fabs(three_pi_lv1.M()-ETA_MASS)>ETA_CUT && 
//      fabs(three_pi_lv2.M()-ETA_MASS)>ETA_CUT ) return;
//
//  TLorentzVector sum_lv = p1_lv+p2_lv+p3_lv+gg_lv;
//  h.Fill1D("m4pi",three_pi_lv1.M(),1, 200,0,3);
//  
//  //TLorentzVector gg_lv=g1_lv+g2_lv;
//
//  /*-----------------------------------------End of cuts---------------------------------------*/  

  e.TagToSave();
}
