#include <iostream>
#include "PaEvent.h"

// Misc tests and checks

void UserEvent13(PaEvent& e)   {


  //int Run        =  e.RunNum();
  //int Evt        =  e.UniqueEvNum();
  
  for(int i = 0; i < e.NParticle(); i++){ // loop over particle
    const PaParticle& p = e.vParticle(i);
    if(p.iTrack() < 0) continue; // no track associated, skip this particle
    const PaTrack& t = e.vTrack(p.iTrack());
    if(!t.HasMom()){   // if track has no momentum 
      if(p.Q() == 1) { // should never happen
	e.Print(1);    // ... but if it happened, print this event 
      }
    }
  }// end of loop over particles
  return;

  static int n=0;
  double Z = 2250.;
  cout<<endl<<"--------------------------------------------------------------"<<endl;
  for(int i = 0; i < int(e.vTrack().size()); i++){ // loop over tracks
    const PaTrack& t = e.vTrack()[i];
    // start from the first point
    const PaTPar par = t.vTPar(0);
    PaTPar pout0, pout1;
    cout<<"Event "<< n <<" Track "<<i<<"  Zmin: "<<t.Zmin()<<"  Zmax: "<<t.Zmax()<<endl;
    par.Extrapolate(Z, pout0);
    pout0.Print();
    // starts from closest to Z point
    t.Extrapolate(Z, pout1);
    pout1.Print();
  }
 n++;
}
