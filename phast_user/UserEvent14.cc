#include <iostream>
#include <cmath>
#include <stdio.h>
#include <algorithm> 
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "PaDigit.h"

// Predicate for "remove_if"
bool keep_CEDAR_RPD( const PaDigit& d) { 
  if(d.DecodeMapName().find("CE") == 0) return false;
  if(d.DecodeMapName().find("RP") == 0) return false;
  return true;
}

void UserEvent14(PaEvent& e)   {
  
  static TH1D* h1[50];
  static TH2D* h2[50];

  static bool first(true);
  if(first){ // histograms booking block
    Phast::Ref().HistFileDir("Hadron Filtering");

    h1[1]   = new TH1D("h01","Rejection counters",     40, 0, 20);

    h1[9]   = new TH1D("h09","NOutParticles", 50, 0, 50);
    h1[10]  = new TH1D("h10","Qsum in PV",    30, -15, 15);
    h1[11]  = new TH1D("h11","N tracks in the event",  200, 0, 200);
    h1[12]  = new TH1D("h12","PV |Psum|",              880, 0, 220);
    h1[13]  = new TH1D("h13","PV |Psum| for Q = -1",   880, 0, 220);

    h2[10]  = new TH2D("2h10","Missing P: Py VS Px [Gev]", 200, -0.5, 0.5, 200, -0.5, 0.5); 

    first = false;
  }

  h1[1] -> Fill(0.);

  // cut #1 At least 1 primary vertex has to be reconstructed
  int iv = e.iBestPrimaryVertex();
  if(iv == -1)                      return;
  h1[1] -> Fill(1.);
  

  // cut #2 Primary vertex  - within target (with some tolerances)
  const PaVertex& v = e.vVertex(iv);
  const float& PVx = v.X();
  const float& PVy = v.Y();
  const float& PVz = v.Z();

  if( (-90.0 > PVz || PVz > -10.0)) return; 
  if( (PVx*PVx + PVy*PVy) > 4.0 )   return;  
  h1[1] -> Fill(2.);

  // cut #3 Total charge of tracks in the primary vertex
  int Qsum = 0;
  h1[9]->Fill(v.NOutParticles()+0.5);
  TVector3 momsum;
  for(int i = 0; i < v.NOutParticles(); i++){ // loop over outgoing particles
    const int& ip = v.iOutParticle(i);
    assert(ip >= 0);
    const PaParticle& p = e.vParticle(ip);
    assert (p.iTrack() >=0);
    if(p.Q() == -777) continue; // unknown charge. Skip.
    Qsum += p.Q();              // sum up charges
    const PaTPar& par = p.ParInVtx(iv); // parameters of the particle in the PV
    momsum += par.Mom3();
  } 
  h1[10]->Fill(Qsum+0.5);
  if(abs(Qsum) > 10)                return;
  h1[1] -> Fill(3.);

  // cut #4 Ntrack
  h1[11]->Fill(e.NTrack()+0.5);
  if(e.NTrack() >= 30)              return;
  h1[1] -> Fill(4.);

  // misc histograms
  int ib = v.InParticle();
  assert(ib >=0);
  const PaParticle& b = e.vParticle(ib); // beam
  const PaTPar& bpar  = b.ParInVtx(iv);
  TVector3 bmom   = bpar.Mom3();
  TVector3 mismom = bmom-momsum;

  h2[10]->Fill(mismom.X(), mismom.Y());
  h1[12]->Fill(momsum.Mag());
  if(Qsum == -1) h1[13]->Fill(momsum.Mag());

  // delete all PaDigits exept of CEDAR and RPD
  for(int i = int(e.vHit().size()-1); i >= 0; i--){ // loop ovet PaHits
    PaHit & h = e.vHit()[i];
    if(h.iDet() == -777) {   // special PaHit (DAQ digit container)
      vector<PaDigit>& vd = h.vDigits();
      vector<PaDigit>::iterator newend = remove_if(vd.begin(), vd.end(), keep_CEDAR_RPD);
      vd.erase(newend, vd.end());
      break;
    }
  }
  
  e.TagToSave(); // tag this event for saving
}
