#include <iostream>
#include <cmath>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaMagInfo.h"
#include "PaEvent.h"
#include "PaMetaDB.h"
#include "PaAlgo.h"
#include "PaUtils.h"

// Examples of MetaDB use
// etc...

void UserEvent2(PaEvent& e)
{

  const PaSetup& setup = PaSetup::Ref();
  int run = e.RunNum();

  cout<<endl;
  cout<<" Year = "<<e.Year()<<endl;
  cout<<" n-1 in production   (from PaSetup of mDST )           : "<< setup.NminusOne()<<endl;
  cout<<" n-1 most up-to-date (from mySQL 'snapshot')           : "<< PaMetaDB::Ref().NminusOne(run)<<endl;


  float x,y,z,bx,by,bz; x=y=z=0;
  setup.MagField(x,y,z,bx,by,bz);
  cout<< " As was used in production:   Solenoid current = "<<setup.SolenoidCurrent()<<"  Bz = "<<bz<<" [T]"<<endl;
  vector<float> pol = setup.TargetPolarizations();
  if(pol.size() >=2){
    cout<< "                          :   Polarizations = "<<pol[0]<<"  "<<pol[1]<<endl;
  }
  cout<<endl;


  PaMetaDB::Ref().PrintPolarDBentry(run);

  cout<<endl;
  if(PaMetaDB::Ref().TargetSpinZproj(run, pol)){
    if(pol.size() == 2) cout<<"D up spin = "<<pol[0]<<"  D down spin = "<<pol[1]<<endl;
    if(pol.size() == 3) cout<<"D up spin = "<<pol[0]<<"  D centr spin = "<<pol[1]<<"  D down spin = "<<pol[2]<<endl;
  }

  // cout<<endl<<"Beam polarization for P = 190 GeV, year 2003 : "<<100.*PaAlgo::GetBeamPol(190., 2003)<<" %"<<endl;
  
  if(e.ChunkNumber() != -1){ 
    cout<<endl<<"This event belongs to period: "<<e.PeriodName()<<" chunk # "<<e.ChunkNumber()<<endl;
    cout<<"Original raw data file " <<e.RawDataFile()<<endl; 
  }

  cout<<endl<<"Some 'hystory' of current mDST: "<<endl;
  for(int i=0; i < 3 ; i++) {
    cout<<"* "<<setup.vTag()[i]<<endl;
  }

  cout<<endl;
  cout<<"  ---  Mag. field map samples ---   "<<endl;
  PaMagInfo* m = PaSetup::Ref().PtrMagField()->getMagInfo();
  assert (m != NULL);
  float z1 = m[0].zcm/10.; // solenoid  
  float z2 = m[1].zcm/10.; // SM1
  float z3 = m[2].zcm/10.; // SM2
  float xxx[] = {   0,   0,    0};
  float yyy[] = {   0,   0,    0};
  float zzz[] = {  z1,  z2,   z3};
  float xx,yy,zz,bbx,bby,bbz;
  for(int k=0; k <  PaSetup::Ref().PtrMagField()->getNumOfMags(); k++){
    xx=xxx[k]; yy=yyy[k]; zz=zzz[k];
    setup.MagField(xx,yy,zz,bbx,bby,bbz);
    printf("x = %7.2f  y =  %7.2f   z =  %7.2f [cm] : Bx = %11.8f   By = %11.8f   Bz = %11.8f [T]\n", xx,yy,zz,bbx,bby,bbz);
  }


  Phast::Ref().next_file = true; // skip to next input file

  //static bool first = true;
  //if(first) e.TagToSave();
  //first = false;
}
