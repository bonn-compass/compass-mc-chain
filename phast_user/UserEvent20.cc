#include <iostream>
#include <cmath>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "G3part.h"
#include "MHister.h"

// Basic values
// Nneg, Npos, Nneu

using namespace std;

void UserEvent20(PaEvent& e) {
  
  static MHister h("/tmp/test_hister.root");

  int NVertex = e.NVertex(); if(NVertex!=1) return;
  int Bvtx   =  e.iBestPrimaryVertex(); if(Bvtx == -1) return;

  const PaVertex& pv = e.vVertex(Bvtx);
  int iBeam = pv.InParticle(); if (iBeam == -1) return;

  int nTrackPV = pv.NOutParticles();   //if (nTrackPV != 3) return;

  double Npos=0,Nneg=0;
  for (int i = 0; i < nTrackPV; i++) {
    int iPart = pv.iOutParticle(i);
    int Q = e.vParticle(iPart).Q();
    if(Q ==+1) Npos++;
    if(Q ==-1) Nneg++;
  }
  h.Fill2D("nPosNeg",Npos,Nneg,1,
	   10,-0.5,9.5,
	   10,-0.5,9.5);

  cout << "-----------------" << endl;
  int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    cout << i << ", Q = " << part.Q() 
	 << ", NCal = " << part.NCalorim()
	 << ", NVert = " << part.NVertex()
	 << endl;
    if(part.Q()==0) {
      if(part.NCalorim()==0) {cerr<< "Error!!"<<endl; return;}
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	cout << "\t" << e.vCaloClus(iClast).E() << endl;
      }
    }
  }

//  hnPosNeu->Fill(Npos,Nneu);
//  hnNegNeu->Fill(Nneg,Nneu);

  //filling 
  h.Fill1D("nvert",e.NVertex(),1,10,-0.5,9.5);
  h.Fill1D("npart",e.NParticle(),1,20,-0.5,19.5);

  //processing
  const PaParticle& beamPart(e.vParticle(iBeam));
  const PaTPar& beamPar(beamPart.ParInVtx(Bvtx));
  
  h.Fill1D("pbeam",beamPar.Mom(),1,100,180,205);
  
  TVector3 sum(0,0,0);
  for (int i = 0; i < nTrackPV; i++) {
    int iPart = pv.iOutParticle(i);
    const PaParticle& part(e.vParticle(iPart));
    const PaTPar& par(part.ParInVtx(Bvtx));
    sum += par.Mom3();
  }
  h.Fill1D("preso",sum.Mag(),1,100,180,205);
  
  //cout << sum.Mag() << endl;
  
//  double sum_energy = 0;
//  //cout << "-----------------------------" << endl;
//  for(int i=0;i<e.NParticle();i++) {
//    //sum_energy += e.vParticle(i).Mom3 
//    //cout << e.vParticle(i).NVertex() << endl;
//  }

}
