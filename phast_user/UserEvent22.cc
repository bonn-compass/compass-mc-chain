#include <iostream>
#include <cmath>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "G3part.h"
#include "MHister.h"
#include "mass.h"

//two tracks
//Q1+Q2 = 0

using namespace std;

void UserEvent22(PaEvent& e) {
  
  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHister h( ( sh_file_name+".mine" ).c_str() );

  int NVertex = e.NVertex(); if(NVertex!=1) return;
  int Bvtx   =  e.iBestPrimaryVertex(); if(Bvtx == -1) return;

  const PaVertex& pv = e.vVertex(Bvtx);
  h.Fill2D("vxy",pv.X(),pv.Y(),1,50,-3,3,50,-3,3);
  h.Fill1D("vz",pv.Z(),1,50,-80,-20);
  int iBeam = pv.InParticle(); if (iBeam == -1) return;

  int nTrackPV = pv.NOutParticles();   if (nTrackPV != 2) return;
  
  const PaParticle & part1 = e.vParticle(pv.iOutParticle(0));
  const PaParticle & part2 = e.vParticle(pv.iOutParticle(1));
  
  if(part1.Q() + part2.Q() != 0) return;
  
  int Nneu = 0;
  int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    if(part.Q()==0) {
      if(part.NCalorim()==0) {cerr<< "Error!!"<<endl; return;}
      h.Fill1D("nclusters",part.NCalorim(),1,
	       5,-0.5,4.5);
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	h.Fill1D("clust_e",e.vCaloClus(iClast).E(),1,
		 200,0,40);
	h.Fill2D("clust_size_vs_energy",e.vCaloClus(iClast).Size(),e.vCaloClus(iClast).E(),1,
		 20,0,40,
		 50,0,20);
	h.Fill1D("clust_nPart",e.vCaloClus(iClast).NParticles (),1,
		 200,0,100);
	//if(e.vCaloClus(iClast).E()>1) return;
      }
      Nneu++;
    }
  }
  //if(Nneu != 0) return;
  //cout << "Hurray" << endl;
  
  TLorentzVector p1 = part1.ParInVtx(Bvtx).LzVec(K_MASS);
  TLorentzVector p2 = part2.ParInVtx(Bvtx).LzVec(K_MASS);
  TLorentzVector p12 = p1+p2;
  h.Fill1D("dipion_mass",p12.M(),1,
	   100,0,3.0);
  h.Fill1D("nneu",Nneu,1,
	   21,-0.5,20.5);

  //processing
  const PaParticle& beamPart(e.vParticle(iBeam));
  const PaTPar& beamPar(beamPart.ParInVtx(Bvtx));
  
  h.Fill1D("pbeam",beamPar.Mom(),1,100,188,192);
  
  TVector3 sum(0,0,0);
  for (int i = 0; i < nTrackPV; i++) {
    int iPart = pv.iOutParticle(i);
    const PaParticle& part(e.vParticle(iPart));
    const PaTPar& par(part.ParInVtx(Bvtx));
    sum += par.Mom3();
  }
  h.Fill1D("preso",sum.Mag(),1,100,180,205);
  
  //cout << sum.Mag() << endl;
  
//  double sum_energy = 0;
//  //cout << "-----------------------------" << endl;
//  for(int i=0;i<e.NParticle();i++) {
//    //sum_energy += e.vParticle(i).Mom3 
//    //cout << e.vParticle(i).NVertex() << endl;
//  }
  //e.TagToSave();
}
