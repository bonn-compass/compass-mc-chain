//
// Peter.Fauland@cern.ch
//
// Example of how to search for D* via D0->K0pi+pi-
// using MC data
//
// version 4.2

#include <iostream>
#include <cmath>
#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TLorentzVector.h"
#include "TLorentzRotation.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "PaParticle.h"
#include "PaAlgo.h"
#include "G3part.h"
#include "PaMCvertex.h"
#include "PaMCtrack.h"


void UserEvent3(PaEvent& e)         // shortcut e = PaEvent
{
  
  static TTree* tree(NULL);
  
  int Run =0;
  int Pid =0;
  double Energy = 0.0;
  double Momentum = 0.0;
  
  static bool first(true);
  if(first){                         // histograms and Ntupes booking block
    
    //
    // Ntuple definition 
    //

    tree = new TTree("tree","tree"); // name (has to be unique) and title of the Ntuple    
    
    tree->Branch("Run",           &Run,           "Run/I");
    tree->Branch("Energy",        &Energy,        "Energy/D");
    tree->Branch("Momentum",      &Momentum,      "Momentum/D");
    tree->Branch("Pid",           &Pid,           "Pid/I");
    first=false;
  } // end of histogram booking
  



// ******************************************************************************
//
//  Vadim:   dstarkpipiDST.2002.04   files are for D*+ only !!
//          adstarkpipiDST.2002.04   files     for D*-
//
//  to be found on /castor/cern.ch/user/c/comgeant/data/mc/dstar/muplus160/2002/mDST
//
//  D^*+ --> D^0 + pi^+ --> ( K^- + pi^+ ) + pi^+
//
//  D^*- --> D^0bar + pi^- --> ( K^+ + pi^- ) + pi^-
//
// 

  for(int mc_t = 0; mc_t < e.NMCtrack(); mc_t++)      // looping over number of MC tracks
    {
      const PaMCtrack& t = e.vMCtrk(mc_t);            // assign MC track number
      if( t.Pid()!= 9 )                               // search for pi+ in case of Pid = 8
      	continue;

      int i_orig = t.iVertexOfOrigin();               // Index of original Vertex 
      if(i_orig == -1) continue;

      const PaMCvertex& v = e.vMCvtx(i_orig);         // MC vertex number "i_orig" 

      int i_mothertrack = v.iTrackOfOrigin();         // Index of mother particle (incomming trajectory) 
                                                      // in PaEvent::vecMCtrk
      if (i_mothertrack == -1) continue;

      const PaMCtrack& mt = e.vMCtrk(i_mothertrack);  // MC track number "i_mothertrack"

      if (mt.Pid() !=56) continue;                    // We do have a pi+ coming from D0 (Pid=37) decay

 
          // Pid 35 = D+, 36 = D-, 37 = D0, 56 = D*-, 55 = D*+, 7 = pi0, 8 = pi+, 9 = pi-, 11 = K+, 12 = K- 
  
          Energy = t.E();
          TVector3 momdrei = t.Mom3();
          Momentum = momdrei.Mag();
          Pid = t.Pid();                              // listing of seen particles
          tree->Fill();
       
    }

}

