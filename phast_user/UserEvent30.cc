#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#include "mass.h"

void calculate_and_fill_under(std::vector<double> &gammas, TH2D &hist);
void calculate_and_fill_upper(std::vector<double> &gammas, TH2D &hist);
void calculate_and_fill      (std::vector<double> &gammas, TH2D &hist);

TLorentzVector prepare_gamma_LV(double x,double y,double z,double E, double v[]);

using namespace std;
void UserEvent30(PaEvent& e) {

  static TH1D* hNpv;
  static TH1D* hZpv;
  static TH2D* hXYpv;
  static TH1D* hOUTpv;
  static TH2D *hECALunder;
  static TH2D* hECAL1under;
  static TH2D* hECAL2under;

  static TH2D *hECALupper;
  static TH2D* hECAL1upper;
  static TH2D* hECAL2upper;

  static TH2D* hECAL;
  static TH2D* hECAL1;
  static TH2D* hECAL2;

  static TH1D* hECALmult;
  static TH1D* hECAL1mult;
  static TH1D* hECAL2mult;

  static TH1D* hGGmass;
  static TH1D* hExcl;
  static TH1D* hInvMass;

  static TH1D* hCharge;

  static bool first(true);
  if(first){ // histograms and Ntupes booking block

    hNpv      = new TH1D("Npv", "Amount of PV;Npv",  20, -0.5, 19.5);
    hZpv      = new TH1D("Zpv", "z of PV;Z[cm]",      200, -250,  120);
    hXYpv     = new TH2D("XYpv","xy of PV;X[cm];Y[cm]",    50,   -5,   +5,
   	      	                                           50,   -5,   +5);
    hOUTpv    = new TH1D("OUTpv",  "Amount of OUT-particles from PV;Np", 10, -0.5, 9.5);
    hECALunder  = new TH2D("ECALunder","Amount of shower under Eth vs Eth;Eth[GeV];Nsh",10, 0.5, 10.5,
			                                                          30,-0.5, 29.5);
    hECAL1under  = new TH2D("ECAL1under","Amount of shower under Eth vs Eth;Eth[GeV];Nsh",10, 0.5, 10.5,
			                                                            30,-0.5, 29.5);
    hECAL2under  = new TH2D("ECAL2under","Amount of shower under Eth vs Eth;Eth[GeV];Nsh",10, 0.5, 10.5,
			                                                            30,-0.5, 29.5);

    hECALupper  = new TH2D("ECALupper","Amount of shower upper Eth vs Eth;Eth[GeV];Nsh",10, 0.5, 10.5,
			                                                          20,-0.5, 19.5);
    hECAL1upper  = new TH2D("ECAL1upper","Amount of shower upper Eth vs Eth;Eth[GeV];Nsh",10, 0.5, 10.5,
	  		                                                            20,-0.5, 19.5);
    hECAL2upper  = new TH2D("ECAL2upper","Amount of shower upper Eth vs Eth;Eth[GeV];Nsh",10, 0.5, 10.5,
	  		                                                            20,-0.5, 19.5);


    hECAL  = new TH2D ("ECAL","Amount of shower vs E;E[GeV];Nsh", 25, 0.5, 100.5, 10,-0.5, 9.5);
    hECAL1  = new TH2D("ECAL1","Amount of shower vs E;E[GeV];Nsh",25, 0.5, 100.5, 10,-0.5, 9.5);
    hECAL2  = new TH2D("ECAL2","Amount of shower vs E;E[GeV];Nsh",25, 0.5, 100.5, 10,-0.5, 9.5);

    hECALmult  = new TH1D("ECALm","Amount of shower;E[GeV];Nsh",20,-0.5, 19.5);
    hECAL1mult  = new TH1D("ECAL1m","Amount of shower;E[GeV];Nsh",20,-0.5, 19.5);
    hECAL2mult  = new TH1D("ECAL2m","Amount of shower;E[GeV];Nsh",20,-0.5, 19.5);

    hGGmass = new TH1D("GGmass","Mass of 2#gamma;M_{2#gamma}",200,0,1);

    hExcl = new TH1D("Excl","Sum of momentum;E_{sum}[GeV]",200,100,220);
    hInvMass = new TH1D("InvMass","Inv Mass of system;M_{sum}[GeV]",100,0,3);
    
    hCharge = new TH1D("Charge","Sum of charge;sign",5,-2.5,2.5);

    first=false;
  } // end of histogram booking

  
  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/

  /*------------------------------------primary verteces---------------------------------------*/
  int Npv=0;
  int iPV=0;
  PaVertex const *v0;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { Npv++; iPV = iv; }
  }
  hNpv->Fill( Npv );
  if(Npv!=1) return;
  v0 = &e.vVertex(iPV);

  double v[] = {v0->X(),v0->Y(),v0->Z()};
  hZpv->Fill( v[2] );
  if( v[2] > -29 || v[2] < -66 ) return;

  hXYpv->Fill( v[0],v[1] );

  /*--------------------------------------Outgoing particles-----------------------------------*/
  hOUTpv->Fill( v0->NOutParticles() );
  if(v0->NOutParticles() != 2) return;
  TLorentzVector p1 = e.vParticle(v0->iOutParticle(0)).ParInVtx(iPV).LzVec(PI_MASS);
  TLorentzVector p2 = e.vParticle(v0->iOutParticle(1)).ParInVtx(iPV).LzVec(PI_MASS);
  int Q1 = e.vTrack(e.vParticle(v0->iOutParticle(0)).iTrack()).Q();
  int Q2 = e.vTrack(e.vParticle(v0->iOutParticle(1)).iTrack()).Q();
  hCharge->Fill(Q1+Q2);
  /*-----------------------------------------Calorimetry---------------------------------------*/
  std::vector<double> gammas;
  std::vector<double> gammas1,gammas2;
  std::vector<int> position1,position2;
  std::vector<double> position;
  
  const int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    if(part.Q()==0) {
      if( part.NCalorim() != 1) cout << "Strange: part.NCalorim()!=1" << endl;
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	const PaCaloClus &cl = e.vCaloClus(iClast);
	gammas.push_back(cl.E());
	position.push_back(iClast);
	if( cl.CalorimName().find("EC01P1") != std::string::npos ) { 
	  gammas1.push_back(cl.E());
	  position1.push_back(iClast);
	}
	if( cl.CalorimName().find("EC02P1") != std::string::npos ) {
	  gammas2.push_back(cl.E());
	  position2.push_back(iClast);
	}
      }
    }
  }
  //cout << gammas.size() << "="<<gammas1.size()<<"+"<<gammas2.size()<< endl;
  
  hECALmult ->Fill(gammas .size());
  hECAL1mult->Fill(gammas1.size());
  hECAL2mult->Fill(gammas2.size());

  calculate_and_fill_under(gammas, *hECALunder);
  calculate_and_fill_under(gammas1,*hECAL1under);
  calculate_and_fill_under(gammas2,*hECAL2under);

  calculate_and_fill_upper(gammas, *hECALupper);
  calculate_and_fill_upper(gammas1,*hECAL1upper);
  calculate_and_fill_upper(gammas2,*hECAL2upper);

  calculate_and_fill(gammas ,*hECAL );
  calculate_and_fill(gammas1,*hECAL1);
  calculate_and_fill(gammas2,*hECAL2);

//  vector<TLorentzVector*> gamma_LV;
//  for(uint i=0;i<gamma.size();i++)
//  TLorentzVector g1 = 
//    prepare_gamma_LV(e.vCaloClus(position[0]).X(),
//		     e.vCaloClus(position_of_good[0]).Y(),
//		     e.vCaloClus(position_of_good[0]).Z(),
//		     e.vCaloClus(position_of_good[0]).E(),v);

  std::vector<double> position_of_good;
  const double Ecut1 = 2;
  const double Ecut2 = 4;
  for(uint i=0;i<gammas1.size();i++) 
    if(gammas1[i]>Ecut1) position_of_good.push_back(position1[i]);
  for(uint i=0;i<gammas2.size();i++) 
    if(gammas2[i]>Ecut2) position_of_good.push_back(position2[i]);

  if(position_of_good.size()!=2) return;


  TLorentzVector g1 = 
    prepare_gamma_LV(e.vCaloClus(position_of_good[0]).X(),
		     e.vCaloClus(position_of_good[0]).Y(),
		     e.vCaloClus(position_of_good[0]).Z(),
		     e.vCaloClus(position_of_good[0]).E(),v);
  //g1.Print();

  TLorentzVector g2 = 
    prepare_gamma_LV(e.vCaloClus(position_of_good[1]).X(),
		     e.vCaloClus(position_of_good[1]).Y(),
		     e.vCaloClus(position_of_good[1]).Z(),
		     e.vCaloClus(position_of_good[1]).E(),v);
  //g2.Print();

  TLorentzVector gg = g1+g2;
  hGGmass->Fill(gg.M());
  if(fabs(gg.M()-PI0_MASS)>0.015) return;


  TLorentzVector sum = (p1+p2)+gg;

  hExcl   ->Fill(sum.P());
  hInvMass->Fill(sum.M());

  //e.TagToSave();
}

  
void calculate_and_fill_under(std::vector<double> &array, TH2D &hist) {
  const int NbinsX = hist.GetNbinsX();
  const double BinWidth = hist.GetXaxis()->GetBinWidth(1);
  for(int i=0;i<NbinsX;i++) {
    double Ecut = hist.GetXaxis()->GetBinUpEdge(i+1);
    int amount = 0;
    for(uint j=0;j<array.size();j++) if(array[j]<Ecut) amount++;
    hist.Fill(Ecut-BinWidth/2.0,amount);
  }
}

void calculate_and_fill_upper(std::vector<double> &array, TH2D &hist) {
  const int NbinsX = hist.GetNbinsX();
  const double BinWidth = hist.GetXaxis()->GetBinWidth(1);
  //cout << "calculate_and_fill_upper " << array.size() << endl;
  for(int i=0;i<NbinsX;i++) {
    double Ecut = hist.GetXaxis()->GetBinUpEdge(i+1);
    int amount = 0;
    for(uint j=0;j<array.size();j++) if(array[j]>Ecut) amount++;
    hist.Fill(Ecut-BinWidth/2.0,amount);
  }
}

void calculate_and_fill(std::vector<double> &array, TH2D &hist) {
  const int NbinsX = hist.GetNbinsX();
  const double BinWidth = hist.GetXaxis()->GetBinWidth(1);
  for(int i=0;i<NbinsX;i++) {
    double Ecut1 = hist.GetXaxis()->GetBinLowEdge(i+1);
    double Ecut2 = hist.GetXaxis()->GetBinUpEdge(i+1);
    int amount = 0;
    for(uint j=0;j<array.size();j++) if(array[j]>Ecut1 && array[j]<Ecut2) amount++;
    hist.Fill(Ecut2-BinWidth/2.0,amount);
  }
}

