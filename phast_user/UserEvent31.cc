#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#include "mass.h"
#include "heplib.h"
#include "MHister.h"
#include "MHistogramer.h"
#include "MSystem.h"
#include "MDecay2two.h"

#define N_PV 1

#define VZ_UP   -29
#define VZ_DOWN -66

#define V_R 1.55

#define NOUT_PART 2

#define ECAL1_TH .8
#define ECAL2_TH  8
#define NGAMMA 2

#define PI_Z_RANGE 0.035

#define EXCL_UP    195
#define EXCL_DOWN  185

#define CHARGE_SUM  0

using namespace std;

void prepare_gamma_V(double x,double y,double z,double E,
		      double v[],
		      double *gamma_vector);

void calculate_and_fill_under(std::vector<double> &gammas, TH2D &hist);
void calculate_and_fill_upper(std::vector<double> &gammas, TH2D &hist);

void plot_mass_if_two (std::vector<TLorentzVector> &gamma_LV,
		       std::vector<int> origin,
		       TH2D &hist, TH2D &hist1, TH2D &hist2);
void plot_mass_if_two (std::vector<TLorentzVector> &gamma_LV,
		       TH2D &hist);

TLorentzVector prepare_gamma_LV(double x,double y,double z,double E,double v[]);

void UserEvent31(PaEvent& e) {

  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHistogramer h( ( sh_file_name+".mine" ).c_str() );

  static bool first(true);
  if(first){ // histograms and Ntupes booking block

    h.BookVertex();
    h.BookKinematics();
    h.BookPi0();    
    h.BookRecoilAdvanced();

    h.Book2D("ECALunder" ,40, 0, 4, 30,-0.5, 29.5, "Amount of shower under Eth vs Eth;Eth[GeV];Nsh");
    h.Book2D("ECAL1under",40, 0, 4, 30,-0.5, 29.5, "Amount of shower under Eth vs Eth;Eth[GeV];Nsh");
    h.Book2D("ECAL2under",40, 0, 4, 30,-0.5, 29.5, "Amount of shower under Eth vs Eth;Eth[GeV];Nsh");

    h.Book2D("ECALupper" ,40, 0, 4, 30,-0.5, 29.5, "Amount of shower upper Eth vs Eth;Eth[GeV];Nsh");
    h.Book2D("ECAL1upper",40, 0, 4, 30,-0.5, 29.5, "Amount of shower upper Eth vs Eth;Eth[GeV];Nsh");
    h.Book2D("ECAL2upper",40, 0, 4, 30,-0.5, 29.5, "Amount of shower upper Eth vs Eth;Eth[GeV];Nsh");

    h.Book2D("ECALmass" ,20, 0, 6, 50, 0, 0.5,   "Mgg, if Ng=2 upper Eth vs Eth;Eth[GeV];M_{gg}");
    h.Book2D("ECAL1mass",20, 0, 6, 50, 0, 0.5,   "Mgg, if Ng=2 upper Eth vs Eth;Eth[GeV];M_{gg}");
    h.Book2D("ECAL2mass",20, 0, 6, 50, 0, 0.5,   "Mgg, if Ng=2 upper Eth vs Eth;Eth[GeV];M_{gg}");

    h.Book2D("ECALmass_s" ,20, 0, 8, 50, 0, 0.5, "Mgg, if Ng=2 upper Eth vs Eth;Eth[GeV];M_{gg}");
    h.Book2D("ECAL1mass_s",20, 0, 4, 50, 0, 0.5, "Mgg, if Ng=2 upper Eth vs Eth;Eth[GeV];M_{gg}");
    h.Book2D("ECAL2mass_s",20, 0, 15, 50, 0, 0.5,"Mgg, if Ng=2 upper Eth vs Eth;Eth[GeV];M_{gg}");


    first=false;
  } // end of histogram booking

  
  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/

//  h.Fill1D("TrigMask",e.TrigMask(),1, 256,-.5,255.5);
//  cout << "e.TrigMask() = " << e.TrigMask() << endl;
//  cout << "e.SubTriggerMask() = " << e.SubTriggerMask() << endl;

//  cout << "-----------------level0----------------------" << endl;
//  e.Print(0);
//  cout << "-----------------level1----------------------" << endl;
//  e.Print(1);
//  cout << "-----------------level2----------------------" << endl;
//  e.Print(2);

  /*------------------------------------primary verteces---------------------------------------*/
  int Npv=0;
  int iPV=0;
  PaVertex const *v0;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { Npv++; iPV = iv; }
  }
  h.Fill1D( "Npv", Npv, 1, 20, -0.5, 19.5, "Amount of PV;Npv" );
  if(Npv!=N_PV) return;
  v0 = &e.vVertex(iPV);

  double v[] = {v0->X(),v0->Y(),v0->Z()};
  h.Fill1D( "Zpv", v[2], 1, 200, -250, 120, "z of PV;Z[cm]" );
  if( v[2] > VZ_UP || v[2] < VZ_DOWN ) return;

  h.Fill2D("XYpv", v[0],v[1], 1, 
	   50,   -5,   +5,
	   50,   -5,   +5, "xy of PV;X[cm];Y[cm]");
  if( v[0]*v[0]+v[1]*v[1] > V_R*V_R ) return;

  double mean_time = e.vTrack( e.vParticle( v0->InParticle() ).iTrack() ).MeanTime();
  h.Fill1D("beam_time",mean_time,1,
	   300,-15,15,"Mean time of beam particle");
  if(fabs(mean_time) > 2.5) return;
  TLorentzVector lvbeam = e.vParticle(v0->InParticle()).ParInVtx(iPV).LzVec(PI_MASS);
  double beam[] = {lvbeam.X(),lvbeam.Y(),lvbeam.Z(),lvbeam.E(),lvbeam.M()};

  /*--------------------------------------Outgoing particles-----------------------------------*/
  h.Fill1D ("OUTpv", v0->NOutParticles(), 1, 10, -0.5, 9.5, "Amount of OUT-particles from PV;Np" );
  if(v0->NOutParticles() != NOUT_PART) return;
  TLorentzVector p1 = e.vParticle(v0->iOutParticle(0)).ParInVtx(iPV).LzVec(PI_MASS);
  TLorentzVector p2 = e.vParticle(v0->iOutParticle(1)).ParInVtx(iPV).LzVec(PI_MASS);
  int Q1 = e.vTrack(e.vParticle(v0->iOutParticle(0)).iTrack()).Q();
  int Q2 = e.vTrack(e.vParticle(v0->iOutParticle(1)).iTrack()).Q();
  h.Fill1D("Charge",Q1+Q2,1, 5,-2.5,2.5, "Sum of charge;sign" );
  if(Q1+Q2 != CHARGE_SUM) return;

  double prt1[] = {p1.X(),p1.Y(),p1.Z(),p1.E(),p1.M()};
  double prt2[] = {p2.X(),p2.Y(),p2.Z(),p2.E(),p2.M()};
  /*-----------------------------------------Calorimetry---------------------------------------*/
  std::vector<double> gammas;
  std::vector<double> gammas1,gammas2;
  std::vector<int> position1,position2;
  std::vector<double> position;
  std::vector<int> origin;
  
  const int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    if(part.Q()==0) {
      if( part.NCalorim() != 1) cout << "Strange: part.NCalorim()!=1" << endl;
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	const PaCaloClus &cl = e.vCaloClus(iClast);
	gammas.push_back(cl.E());
	position.push_back(iClast);
	if( cl.CalorimName().find("EC01P1") != std::string::npos ) { 
	  gammas1.push_back(cl.E());
	  position1.push_back(iClast);
	  origin.push_back(1);
	}
	if( cl.CalorimName().find("EC02P1") != std::string::npos ) {
	  gammas2.push_back(cl.E());
	  position2.push_back(iClast);
	  origin.push_back(2);
	}
      }
    }
  }
  
  h.Fill1D("ECALm", gammas .size(), 1, 20,-0.5, 19.5, "Amount of shower;E[GeV];Nsh");
  h.Fill1D("ECAL1m",gammas1.size(), 1, 20,-0.5, 19.5, "Amount of shower;E[GeV];Nsh");
  h.Fill1D("ECAL2m",gammas2.size(), 1, 20,-0.5, 19.5, "Amount of shower;E[GeV];Nsh");

  calculate_and_fill_under(gammas, *( (TH2D*)h.GetObject("ECALunder") ));
  calculate_and_fill_under(gammas1,*( (TH2D*)h.GetObject("ECAL1under") ));
  calculate_and_fill_under(gammas2,*( (TH2D*)h.GetObject("ECAL2under") ));

  calculate_and_fill_upper(gammas, *( (TH2D*)h.GetObject("ECALupper") ));
  calculate_and_fill_upper(gammas1,*( (TH2D*)h.GetObject("ECAL1upper") ));
  calculate_and_fill_upper(gammas2,*( (TH2D*)h.GetObject("ECAL2upper") ));

  /*-------------------------------------------------------------------------------*/
  //plot_mass_if_two
  vector<TLorentzVector> gamma_LV;
  for(uint i=0;i<position.size();i++)
    gamma_LV.push_back(
		       prepare_gamma_LV(e.vCaloClus(position[i]).X(),
					e.vCaloClus(position[i]).Y(),
					e.vCaloClus(position[i]).Z(),
					e.vCaloClus(position[i]).E(),v)
		       );
//  plot_mass_if_two(gamma_LV, origin,
//		   *( (TH2D*)h.GetObject("ECALmass") ),
//		   *( (TH2D*)h.GetObject("ECAL1mass") ),
//		   *( (TH2D*)h.GetObject("ECAL2mass") ));
  //all  
  plot_mass_if_two(gamma_LV, *( (TH2D*)h.GetObject("ECALmass_s") ));
  //ecal1
  gamma_LV.clear();
  for(uint i=0;i<position1.size();i++)
    gamma_LV.push_back(
		       prepare_gamma_LV(e.vCaloClus(position1[i]).X(),
					e.vCaloClus(position1[i]).Y(),
					e.vCaloClus(position1[i]).Z(),
					e.vCaloClus(position1[i]).E(),v)
		       );
  plot_mass_if_two(gamma_LV, *( (TH2D*)h.GetObject("ECAL1mass_s") ));
  //ecal2
  gamma_LV.clear();
  for(uint i=0;i<position2.size();i++)
    gamma_LV.push_back(
		       prepare_gamma_LV(e.vCaloClus(position2[i]).X(),
					e.vCaloClus(position2[i]).Y(),
					e.vCaloClus(position2[i]).Z(),
					e.vCaloClus(position2[i]).E(),v)
		       );
  plot_mass_if_two(gamma_LV, *( (TH2D*)h.GetObject("ECAL2mass_s") ));
  /*-------------------------------------------------------------------------------*/

  std::vector<double> position_of_good;

  for(uint i=0;i<gammas1.size();i++) 
    if(gammas1[i]>ECAL1_TH) position_of_good.push_back(position1[i]);
  for(uint i=0;i<gammas2.size();i++) 
    if(gammas2[i]>ECAL2_TH) position_of_good.push_back(position2[i]);

  if(position_of_good.size()!=NGAMMA) return;

  //gamma 1
  double g1[5];
  prepare_gamma_V(e.vCaloClus(position_of_good[0]).X(),
		   e.vCaloClus(position_of_good[0]).Y(),
		   e.vCaloClus(position_of_good[0]).Z(),
		   e.vCaloClus(position_of_good[0]).E(),
		   v,
		   g1);
  hlmass5(g1,g1,0);

  //gamma 2
  double g2[5];
  prepare_gamma_V(e.vCaloClus(position_of_good[1]).X(),
		   e.vCaloClus(position_of_good[1]).Y(),
		   e.vCaloClus(position_of_good[1]).Z(),
		   e.vCaloClus(position_of_good[1]).E(),
		   v,
		   g2);
  hlmass5(g2,g2,0);

  //pi0
  MDecay2two pi0(g1,g2);

  h.Fill1D("GGmass", pi0.fdprt()[4], 1 ,200,0,1, "Mass of 2#gamma;M_{2#gamma}" );
  if(fabs(pi0.fdprt()[4]-PI0_MASS)>PI_Z_RANGE) return;

  MSystem sys(beam,
	      1,prt1,
	      1,pi0.fdprt(),
	      1,prt2,
	      PROT_MASS);
  MSystem osys(sys); 
  sys.ReCulcBeamVector(PROT_MASS,PROT_MASS);

  h.Fill1D("InvMass",sys.freso()[4],1, 100,0,3, "Inv Mass of system;M_{sum}[GeV]");
  h.Fill1D("Excl", sys.freso()[3] ,1, 200,100,220, "Sum of momentum;E_{sum}[GeV]");
  if( sys.freso()[3]>EXCL_UP || sys.freso()[3]<EXCL_DOWN) return;  

  h.FillVertex(v);
  h.FillPi0(pi0);
  h.FillKinematics(sys);
  h.FillRecoilAdvanced(osys);

  //e.TagToSave();
}


void prepare_gamma_V(double x,double y,double z,double E,
		      double v[],
		      double *gamma_vector) {
  
  double dist = sqrt((x-v[0])*(x-v[0])+(y-v[1])*(y-v[1])+(z-v[2])*(z-v[2]));
  gamma_vector[0] = (x-v[0])/dist*E;
  gamma_vector[1] = (y-v[1])/dist*E;
  gamma_vector[2] = (z-v[2])/dist*E;
  gamma_vector[3] = E;
  gamma_vector[4] = 0;
}


void plot_mass_if_two (std::vector<TLorentzVector> &gamma_LV,
		       std::vector<int> origin,
		       TH2D &hist, TH2D &hist1, TH2D &hist2) {
  if(hist.GetNbinsX() != hist1.GetNbinsX() ||
     hist2.GetNbinsX() != hist1.GetNbinsX()) return; // fool protection
  
  const int NbinsX = hist.GetNbinsX();
  const double BinWidth = hist.GetXaxis()->GetBinWidth(1);
  for(int i=0;i<NbinsX;i++) {
    double Ecut = hist.GetXaxis()->GetBinUpEdge(i+1);
    vector<int> where;
    for(uint j=0;j<gamma_LV.size();j++) if(gamma_LV[j].E()>Ecut) where.push_back(j);
    //hist.Fill(Ecut-BinWidth/2.0,amount);
    if(where.size()==2) {
      double invMass = ( gamma_LV[ where[0] ]+gamma_LV[ where[1] ] ).M();
      if(     origin[ where[0] ] == 1 && origin[ where[1] ] == 1) hist1.Fill(Ecut-BinWidth/2.0,invMass);
      else if(origin[ where[0] ] == 2 && origin[ where[1] ] == 2) hist2.Fill(Ecut-BinWidth/2.0,invMass);
      else hist.Fill(Ecut-BinWidth/2.0,invMass);
    }
  }  

}

void plot_mass_if_two (std::vector<TLorentzVector> &gamma_LV, TH2D &hist) {
  const int NbinsX = hist.GetNbinsX();
  const double BinWidth = hist.GetXaxis()->GetBinWidth(1);
  for(int i=0;i<NbinsX;i++) {
    double Ecut = hist.GetXaxis()->GetBinUpEdge(i+1);
    vector<int> where;
    for(uint j=0;j<gamma_LV.size();j++) if(gamma_LV[j].E()>Ecut) where.push_back(j);
    //hist.Fill(Ecut-BinWidth/2.0,amount);
    if(where.size()==2) {
      double invMass = ( gamma_LV[ where[0] ]+gamma_LV[ where[1] ] ).M();
      hist.Fill(Ecut-BinWidth/2.0,invMass);
    }
  }  
}
