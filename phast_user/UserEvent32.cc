#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#include "mass.h"
#include "heplib.h"
#include "MHister.h"

/*
  The program makes preselection for two-charge, two-gamma final state.
*/

//parameters of cuts, which are applied:
#define N_PV 1
#define NOUT_PART 2
#define ECAL1_TH 10
#define ECAL2_TH 10
#define NGAMMA 2

using namespace std;

void UserEvent32(PaEvent& e) {

  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHister h( ( sh_file_name+".mine" ).c_str() );

  static bool first(true);
  if(first){ 
    first=false;
  } 

  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/

  /*------------------------------------primary verteces---------------------------------------*/
  vector<int> pv;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { pv.push_back(iv); }
  }
  h.Fill1D( "Npv", pv.size(), 1, 20, -0.5, 19.5, "Amount of PV;Npv" );
  if( pv.size() < N_PV ) return;
  
  /*--------------------------------------Outgoing particles-----------------------------------*/
  vector<int> op;
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& vi = e.vVertex(pv[i]);
    for(int j=0;j<vi.NOutParticles();j++) op.push_back( vi.iOutParticle(j) );
  }

  h.Fill1D ("OUTpv", op.size(), 1, 10, -0.5, 9.5, "Amount of OUT-particles from PVs;Np" );
  if( op.size() != NOUT_PART ) return;
    
  /*-----------------------------------------Calorimetry---------------------------------------*/
  std::vector<double> gammas1,gammas2;
  std::vector<int> position1,position2;
  
  const int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    if(part.Q()==0) {
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	const PaCaloClus &cl = e.vCaloClus(iClast);
	if( cl.CalorimName().find("EC01P1") != std::string::npos ) { 
	  gammas1.push_back(cl.E());
	  position1.push_back(iClast);
	}
	if( cl.CalorimName().find("EC02P1") != std::string::npos ) {
	  gammas2.push_back(cl.E());
	  position2.push_back(iClast);
	}
      }
    }
  }
  
  h.Fill1D("ECAL1m",gammas1.size(), 1, 20,-0.5, 19.5, "Amount of shower;E[GeV];Nsh");
  h.Fill1D("ECAL2m",gammas2.size(), 1, 20,-0.5, 19.5, "Amount of shower;E[GeV];Nsh");

  if(gammas1.size()+gammas2.size()<NGAMMA) return;

  std::vector<double> position_of_good;
  for(uint i=0;i<gammas1.size();i++) 
    if(gammas1[i]>ECAL1_TH) position_of_good.push_back(position1[i]);
  for(uint i=0;i<gammas2.size();i++) 
    if(gammas2[i]>ECAL2_TH) position_of_good.push_back(position2[i]);

  h.Fill1D("Ngamma_more_th",gammas1.size()+gammas2.size(),1,
	   20,-0.5,19.5);
  if(position_of_good.size()>NGAMMA) return;

  h.Fill1D("Ngamma_cut",gammas1.size()+gammas2.size(),1,
	   20,-0.5,19.5);
  
  /*-----------------------------------------End of cuts---------------------------------------*/  

  e.TagToSave();
}
