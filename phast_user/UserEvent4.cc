#include <iostream>
#include <cmath>
#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "PaDigit.h"

// misc examples how to access DAQ digits and CsDidigits (if they were stored)

void UserEvent4(PaEvent& e)   {
  
  cout<<"======>  Run # "<<e.RunNum()<<"  Spill # "<<e.SpillNum()<<"  Event-in-Spill # "<<e.EvInSpill()<<endl<<endl;
  const vector<PaDigit>& vdd = e.RawDigits();
  for(int i=0; i < int(vdd.size()); i++){ // loop over DAQ digits
    const PaDigit& d = vdd[i];
    string detnam = d.DecodeMapName();
    cout<<detnam<<" DAQ digit contains "<< d.NDigInfo()<<" words: ";
    for(int j=0; j < d.NDigInfo(); j++){
      printf("%4u  ", int(d.DigInfo(j)));
    }
    cout<<endl;
  }// end of loop over DAQ digits

  return;

  static TH1D* h[20];
  static bool first(true);
  if(first){  
    Phast::Ref().HistFileDir("Digits test");
    h[1] = new TH1D("h1","A1",1024,0,1023);
    h[2] = new TH1D("h2","A2",1024,0,1023);
    h[3] = new TH1D("h3","A3",1024,0,1023);

    h[4] = new TH1D("h4","ECAL digits total size [bytes] / event", 100,0,10000);
    h[5] = new TH1D("h5","ECAL digits energy", 1000,0,10);

    first=false;
  } // end of histogram booking
  
  const vector<PaDigit>& vdaqdig = e.RawDigits();
  for(int i=0; i < int(vdaqdig.size()); i++){ // loop over DAQ digits
    const PaDigit& d = vdaqdig[i];
    string detnam = d.DecodeMapName();
    if(detnam.find("RA") == 0){  // select only RICH APV digits
      float a1 = d.DigInfo(3);
      float a2 = d.DigInfo(4);
      float a3 = d.DigInfo(5);
      h[1]->Fill(a1);
      h[2]->Fill(a2);
      h[3]->Fill(a3);
    }
  }// end of loop over DAQ digits



  double n_ecal_dig = 0;
  const vector<PaDigit>& vdig = e.Digits();
  for(int i=0; i < int(vdig.size()); i++){ // loop over digits
    const PaDigit& d = vdig[i];
    string detnam = d.DecodeMapName();
    if(detnam.find("EC") == 0) { // ECAL digits
      n_ecal_dig = n_ecal_dig + 4*(1 + d.vDigInfo().size() + d.vAux().size()); 
      if(!d.vDigInfo().empty()) {
	h[5]->Fill(d.vDigInfo()[0]);
      }
    } // end of ECAL digits "if"
  }// end of loop over digits
  h[4]->Fill(n_ecal_dig);

}
