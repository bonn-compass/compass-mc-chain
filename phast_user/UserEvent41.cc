#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TTree.h"

#include "mass.h"
#include "heplib.h"
#include "MHister.h"
#include "MHistogramer.h"

#include "MSystem.h"

#include "RPD_Helper.h"

#define _USE_MATH_DEFINES
#include <cmath>

/*
  The program select three-charges.
  - Exactly 1 primary
  - Three tracks, sum Qi=-1
  - RPD
  - Exclusivity
  Than tree with piKK is filled.
*/

//parameters of cuts, which are applied:
#define N_PV 1
#define NOUT_PART 3
#define ECAL1_TH 10
#define ECAL2_TH 10
#define NGAMMA 2


using namespace std;
void UserEvent41(PaEvent& e) {

  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHistogramer h( ( sh_file_name+".mine" ).c_str() );

  static TTree *tree;
  static TVector3 *kplus;
  static TVector3 *kmins;
  static TVector3 *pi;

  static bool first(true);
  if(first){ 
    first=false;
    h.BookVertex();
    h.BookKinematics();
    h.BookRecoilAdvanced();
    
    Phast::Ref().h_file->cd();
    tree = new TTree("tree","interesting events");    

    tree->Branch("kplus","TVector3",&kplus);
    tree->Branch("kmins","TVector3",&kmins);
    tree->Branch("pi",   "TVector3",&pi);
  } 

  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/

  /*------------------------------------primary verteces---------------------------------------*/
  vector<int> pv;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { pv.push_back(iv); }
  }
  h.Fill1D( "Npv", pv.size(), 1, 20, -0.5, 19.5, "Amount of PV;Npv" );
  if( pv.size() != N_PV ) return;
  const int iPV = pv[0];
  
  /*--------------------------------------Outgoing particles-----------------------------------*/
  vector<int> op;
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& vi = e.vVertex(pv[i]);
    for(int j=0;j<vi.NOutParticles();j++) op.push_back( vi.iOutParticle(j) );
  }
  h.Fill1D ("OUTpv", op.size(), 1, 10, -0.5, 9.5, "Amount of OUT-particles from PVs;Np" );
  if( op.size() != NOUT_PART ) return;

  int Qsum=0;
  for(uint i=0;i<op.size();i++) Qsum += e.vParticle( op[i] ).Q();
  h.Fill1D ("Qsum", Qsum, 1, 7, -3.5, 3.5, "Sum of charges;Q_{sum}" );
  if(Qsum!=-1) return;

  for(uint i=1;i<op.size();i++) 
    if( e.vParticle( op[i] ).Q() == +1) 
      std::swap(op[0],op[i]);
  
  TLorentzVector p1 = e.vParticle(op[0]).ParInVtx( iPV ).LzVec(PI_MASS);
  TLorentzVector p2 = e.vParticle(op[1]).ParInVtx( iPV ).LzVec(PI_MASS);
  TLorentzVector p3 = e.vParticle(op[2]).ParInVtx( iPV ).LzVec(PI_MASS);
  double prt1[] = {p1.X(),p1.Y(),p1.Z(),p1.E(),p1.M()};
  double prt2[] = {p2.X(),p2.Y(),p2.Z(),p2.E(),p2.M()};
  double prt3[] = {p3.X(),p3.Y(),p3.Z(),p3.E(),p3.M()};

  TLorentzVector b0 = e.vParticle(e.vVertex( iPV ).InParticle()).ParInVtx( iPV ).LzVec(PI_MASS);
  double beam[] = {b0.X(),b0.Y(),b0.Z(),b0.E(),b0.M()};

  // cout << "\n===============" << endl;
  MSystem sys(beam, PROT_MASS); // sys.Print(); cout << "\n---------------" << endl;
  sys.AddParticle(+1,prt1);     // sys.Print(); cout << "\n---------------" << endl;
  sys.AddParticle(-1,prt2);     // sys.Print(); cout << "\n---------------" << endl;
  sys.AddParticle(-1,prt3);     // sys.Print(); cout << "\n---------------" << endl;

  MSystem osys(sys); 
  sys.ReCulcBeamVector(PROT_MASS,PROT_MASS);

  double v[] = {e.vVertex(iPV).X(), e.vVertex(iPV).Y(), e.vVertex(iPV).Z()};
//  double Esum=0;  
//  for(uint i=0;i<op.size();i++) Esum += e.vParticle(op[i]).ParInVtx( iPV ).Mom();
  h.Fill1D ("Esum_before_RPD", sys.freso()[3], 1, 500, 0, 210, "Sum of E_{i};" );
  /*-----------------------------------------End of cuts---------------------------------------*/  
  
  static RPD *myproton;
  myproton = &RPD::Instance();
  myproton->Search(e.vVertex(iPV));
  //multiplicity
  vector <TLorentzVector> protons = myproton->vTracks();
  h.Fill1D("RPDm",protons.size(),1, 10,-0.5,9.5,"multiplicity in RPD");
  if(protons.size() != 1) return;

  //is there best  
  int iBestProtonTrack = myproton->iBestProtonTrack();
  h.Fill1D("IsBest",iBestProtonTrack+1, 1,
	   2,-0.5,1.5,"Is There 'Best Proton Track'");

  if(myproton->iBestProtonTrack() == -1) return;

  double phiRPD = protons[0].Phi();
  double phi0 = atan2(sys.frecl()[1],sys.frecl()[0]);
  h.Fill1D("phiRPD",phiRPD,1, 100, -M_PI, M_PI);
  h.Fill1D("phi0",  phi0,   1, 100, -M_PI, M_PI);
  h.Fill1D("dPhi",  phi0-phiRPD, 1, 100, -2.0*M_PI, 2.0*M_PI);

  if( fabs(phiRPD-phi0) > 0.17) return;
  
  h.Fill1D ("Esum_after_RPD", sys.freso()[3], 1, 500, 0, 210, "Sum of E_{i};" );
  if(sys.freso()[3]>196 || sys.freso()[3]<187) return;

  h.FillVertex(v);
  h.FillKinematics(sys);
  h.FillRecoilAdvanced(osys);

  /* Mein experiment */
  double pipi1[5]; hladdt5(prt1,prt2,pipi1);
  double pipi2[5]; hladdt5(prt1,prt3,pipi2);
  
  hlmass5(prt1,prt1,K_MASS);
  hlmass5(prt2,prt2,K_MASS);
  hlmass5(prt3,prt3,K_MASS);
  double KK1[5]; hladdt5(prt1,prt2,KK1);
  double KK2[5]; hladdt5(prt1,prt3,KK2);
  if(pipi1[4]<0.3) {
    h.Fill1D("massKK",KK1[4],1,100,0.8,1.2,"Mass of KK subsystem");
    hlmass5(prt3,prt3,PI_MASS);
    double m3[5]; hladdt5(KK1,prt3,m3);
    if( fabs(KK1[4]-1.024) < 0.015 ) h.Fill1D("Cmes",m3[4],1,10,1.2,1.8);
    h.Fill2D("massPHIPI_vs_massKK",m3[4],KK1[4],1,
	     10 ,1.2,1.8,
	     20,0.98,1.08);
  }
  if(pipi2[4]<0.3) {
    h.Fill1D("massKK",KK2[4],1,100,0.8,1.2,"Mass of KK subsystem");
    hlmass5(prt2,prt2,PI_MASS);
    double m3[5]; hladdt5(KK2,prt2,m3);
    if( fabs(KK2[4]-1.024) < 0.015 ) h.Fill1D("Cmes",m3[4],1,10,1.2,1.8);
    h.Fill2D("massPHIPI_vs_massKK",m3[4],KK2[4],1,
	     10 ,1.2,1.8,
	     20,0.98,1.08);
  }

  if(pipi1[4]<0.5 || pipi2[4]<0.5) {
    *kplus = TVector3(prt1);
    *kmins = TVector3(prt2);
    *pi    = TVector3(prt3);
    tree->Fill();
  }
  
  //e.TagToSave();
}




//int ChannelToPMT(const std::string& RpdPlanes, const int chId, 
//		 int &a, int &up, int &adc) const {
//  std::stringstream nameIs;
//  thisPMT = -1;
//  
//  if (RpdPlanes == "RP01TA__") {
//    if      (chId >=  0 && chId < 12) { a=1; up = 1; adc = 0; return chId; } 
//    else if (chId >= 32 && chId < 44) { a=1; up = 0; adc = 0; return chId - 32; } 
//    else { cerr << "Error<ChannelToPMT>:  chId !in [0,12)&[32,44)" << endl; exit(1); }
//
//  } else if (RpdPlanes == "RP01TBl_") {
//    if      (chId >=  0 && chId < 12) { a = 0; up = 1; adc = 0;  return chId; } 
//    else if (chId >= 16 && chId < 28) { a = 0; up = 1; adc = 0;  return 12 + chId - 16; } 
//    else if (chId >= 32 && chId < 44) { a = 0; up = 0; adc = 0;  return chId - 32; } 
//    else if (chId >= 48 && chId < 60) { a = 0; up = 0; adc = 0;  return 12 + chId - 48; } 
//    else { cerr << "Error<ChannelToPMT>:  chId !in [0,12)&[16,28)&[32,44)&[48,60)" << endl; exit(1); }
//
//  } else if (RpdPlanes == "RP01Ql__") {
//    if      (chId >=  0 && chId < 12) { a = 1; up = 1; adc = 1;  return chId; }
//    else if (chId >= 12 && chId < 24) { a = 1; up = 0; adc = 1;  return chId - 12; } 
//    else if (chId >= 24 && chId < 48) { a = 0; up = 1; adc = 1;  return chId - 24; }
//    else if (chId >= 48 && chId < 72) { a = 0; up = 0; adc = 1;  return chId - 24 - 24; }
//    else { cerr << "Error<ChannelToPMT>:  chId !in [0,72)" << endl; exit(1); }
//  
//  } else if (RpdPlanes == "RP01Qh__") {
//    if      (chId >=  0 && chId < 12) { a = 1; up = 1; adc = 1;  return chId; }
//    else if (chId >= 12 && chId < 24) { a = 1; up = 0; adc = 1;  return chId - 12; } 
//    else if (chId >= 24 && chId < 48) { a = 0; up = 1; adc = 1;  return chId - 24; }
//    else if (chId >= 48 && chId < 72) { a = 0; up = 0; adc = 1;  return chId - 24 - 24; }
//    else { cerr << "Error<ChannelToPMT>:  chId !in [0,72)" << endl; exit(1); }
//
//  } else {
//    thisPMT = -1;
//    nameIs << "Unkn";
//  }
//  
//  return nameIs.str();
//}
