#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#include "mass.h"
#include "heplib.h"
#include "MHister.h"

/*
  The program makes preselection for two-charge, two-gamma final state.
*/

//parameters of cuts, which are applied:
#define N_PV 1
#define NOUT_PART 3
#define ECAL1_TH 10
#define ECAL2_TH 10
#define NGAMMA 2

using namespace std;
void UserEvent42(PaEvent& e) {

  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHister h( ( sh_file_name+".mine" ).c_str() );

  static bool first(true);
  if(first){ 
    first=false;
  } 

  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/

  /*------------------------------------primary verteces---------------------------------------*/
  vector<int> pv;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { pv.push_back(iv); }
  }
  h.Fill1D( "Npv", pv.size(), 1, 20, -0.5, 19.5, "Amount of PV;Npv" );
  if( pv.size() != N_PV ) return;
  
  /*--------------------------------------Outgoing particles-----------------------------------*/
  vector<int> op;
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& vi = e.vVertex(pv[i]);
    for(int j=0;j<vi.NOutParticles();j++) op.push_back( vi.iOutParticle(j) );
  }
  h.Fill1D ("OUTpv", op.size(), 1, 10, -0.5, 9.5, "Amount of OUT-particles from PVs;Np" );
  if( op.size() != NOUT_PART ) return;

  int Qsum=0;
  for(uint i=0;i<op.size();i++) Qsum += e.vParticle( op[i] ).Q();
  h.Fill1D ("Qsum", Qsum, 1, 7, -3.5, 3.5, "Sum of charges;Q_{sum}" );
  if(Qsum!=-1) return;

  double Esum=0;  
  for(uint i=0;i<op.size();i++) Esum += e.vParticle(op[i]).ParInVtx(pv[0]).Mom();
  h.Fill1D ("Esum", Esum, 1, 500, 0, 210, "Sum of |moments|;|p_{1}|+|p_{2}|+|p_{3}|" );
  
  /*-----------------------------------------End of cuts---------------------------------------*/  

  e.TagToSave();
}
