#include <iostream>
#include <cmath>
#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TBranch.h"
#include "Phast.h"
#include "UserInfo.h"

//
// Example how to save objects of user-defined class "UserInfo"
// into events' tree of output mDST
//
// To avoid 
//"Warning in <TClass::TClass>: no dictionary for class UserInfo is available"
// PHAST which will read produced mDST must have UserInfo.h and UserInfo.cc in ./user/
// identical to what was used at writing.
//


void UserEvent5(PaEvent& e)   {
  
  static UserInfo inf;
  static bool first = true;
  
  Phast& ph = Phast::Ref();

  // create new branch for UserInfo class in PaEvent tree
  if(first) { // create new branch for UserInfo class
    static UserInfo* ptr_userinfo = &inf;
    const char* usrclass_name = ptr_userinfo->GetName();
    TTree* tr = ph.out_event_tree;
    if(tr == NULL) return;   // no output was requested (-o option is missing)
    TBranch* b_user = tr->Branch(usrclass_name, usrclass_name, &ptr_userinfo, 32000,1);
    assert(b_user != NULL);
    first=false;
  }
  
  inf.SetEv(e.EvInSpill()); // store event-in-spill number (for example)
  cout<<"UserEvent5: save info "<<e.EvInSpill()<<endl;

  e.TagToSave();            // tag this event to be saved.
}

