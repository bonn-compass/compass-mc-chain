#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"
#include "PaSetup.h"
#include "PaDetect.h"
//#include "PaDetect.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"
#include "TCanvas.h"
#include "TLine.h"

TLine *detector_line_zy(const PaDetect & d);

using namespace std;
void UserEvent50(PaEvent& e) {
  
  static int Nev = 0;

  static bool first(true);
  if(first){ // histograms and Ntupes booking block


    first=false;
  }
  
  Nev++;

  if(Nev == 30) {
    TCanvas c1 ("c1","plot",0,0,5500,600);
    c1.Range(-400,-300,5100,300);
    
    const PaSetup &setup = PaSetup::Ref();
    int Ndet = setup.NDetectors();
    cout << Ndet << endl;
    for(int i=0;i<Ndet;i++) {
      const PaDetect &det = setup.Detector(i);
      cout << det.Name() << ": (" << det.X() << "," << det.Y() << "," << det.Z() << ")" << endl;
      detector_line_zy(det)->Draw();
    }
    int Ncal = setup.NCalorimeter();
    for(int i=0;i<Ndet;i++) {
      const PaCalorimeter &cal = setup.Calorimeter(i);
      cout << cal.Name() << ": (" << cal.Position().X() << "," << cal.Position().Y() << "," << cal.Position().Z() << ")" << endl;
      TLine *l =  new TLine(cal.Position().Z(), cal.Position().Y()-100,
			    cal.Position().Z(), cal.Position().Y()+100);
      l->SetLineColor(kRed);
      l->SetLineWidth(.1);
      l->Draw("same");
    }

    c1.SaveAs("/tmp/scheme.pdf");

    Phast::Ref().next_file = true;
  }

}


TLine *detector_line_zy(const PaDetect & d) {
  TLine *l = new TLine(d.Z(),d.Y()-d.YSiz()/2.0,
		       d.Z(),d.Y()+d.YSiz()/2.0);
  l->SetLineWidth(0.001);
  return l;
}
