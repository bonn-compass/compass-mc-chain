#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#include "mass.h"
#include "heplib.h"
#include "MHister.h"

/*
  The program makes preselection for one-charge, two-gamma final state.
*/

//parameters of cuts, which are applied:
#define N_PV 1
#define NOUT_PART 1
#define ECAL1_TH 0.0
#define ECAL2_TH   8
#define NGAMMA 2

using namespace std;

TLorentzVector prepare_gamma_LV(double x,double y,double z,double E, double v[]);
void prepare_gamma_V(double x,double y,double z,double E,
		      double v[],
		      double *gamma_vector);
void plot_mass_if_two (std::vector<TLorentzVector> &gamma_LV,
		       TH2D &hist);

void UserEvent52(PaEvent& e) {

  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHister h( ( sh_file_name+".mine" ).c_str() );

  static bool first(true);
  if(first){ 
    h.Book2D("ECALmass_s" ,20, 0, 8, 50, 0, 0.5, "Mgg, if Ng=2 upper Eth vs Eth;Eth[GeV];M_{gg}");
    h.Book2D("ECAL1mass_s",20, 0, 4, 50, 0, 0.5, "Mgg, if Ng=2 upper Eth vs Eth;Eth[GeV];M_{gg}");
    h.Book2D("ECAL2mass_s",20, 0, 15, 50, 0, 0.5,"Mgg, if Ng=2 upper Eth vs Eth;Eth[GeV];M_{gg}");
    first=false;
  } 

  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/

  /*------------------------------------primary verteces---------------------------------------*/
  vector<int> pv;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { pv.push_back(iv); }
  }
  h.Fill1D( "Npv", pv.size(), 1, 20, -0.5, 19.5, "Amount of PV;Npv" );
  if( pv.size() != N_PV ) return;
  double v[] = {e.vVertex(pv[0]).X(),
		e.vVertex(pv[0]).Y(),
		e.vVertex(pv[0]).Z()};
  
  /*--------------------------------------Outgoing particles-----------------------------------*/
  vector<int> op;
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& vi = e.vVertex(pv[i]);
    for(int j=0;j<vi.NOutParticles();j++) op.push_back( vi.iOutParticle(j) );
  }
  h.Fill1D ("OUTpv", op.size(), 1, 10, -0.5, 9.5, "Amount of OUT-particles from PVs;Np" );
  if( op.size() != 1 ) return;

  int Qsum = e.vParticle( op[0] ).Q();
  h.Fill1D ("Qsum", Qsum, 1, 3, -1.5, 1.5, "Sum of charges;Q_{sum}" );
  if(Qsum!=-1) return;

  /*-----------------------------------------Calorimetry---------------------------------------*/
  std::vector< pair<int,int> > gammas;
  
  const int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    if(part.Q()==0) {
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	const PaCaloClus &cl = e.vCaloClus(iClast);
	if( cl.CalorimName().find("EC01P1") != std::string::npos ) gammas.push_back( std::make_pair(iClast,1) );
	if( cl.CalorimName().find("EC02P1") != std::string::npos ) gammas.push_back( std::make_pair(iClast,2) );
      }
    }
  }

  /*-------------------------------------------------------------------------------*/
  vector<TLorentzVector> gamma_LV;
  for(uint i=0;i<gammas.size();i++)
    gamma_LV.push_back(
		       prepare_gamma_LV(e.vCaloClus(gammas[i].first).X(),
					e.vCaloClus(gammas[i].first).Y(),
					e.vCaloClus(gammas[i].first).Z(),
					e.vCaloClus(gammas[i].first).E(),v)
		       );
  //all  
  plot_mass_if_two(gamma_LV, *( (TH2D*)h.GetObject("ECALmass_s") ));
  //ecal1
  gamma_LV.clear();
  for(uint i=0;i<gammas.size();i++)
    if(gammas[i].second == 1)
      gamma_LV.push_back(
			 prepare_gamma_LV(e.vCaloClus(gammas[i].first).X(),
					  e.vCaloClus(gammas[i].first).Y(),
					  e.vCaloClus(gammas[i].first).Z(),
					  e.vCaloClus(gammas[i].first).E(),v)
			 );
  plot_mass_if_two(gamma_LV, *( (TH2D*)h.GetObject("ECAL1mass_s") ));
  //ecal2
  gamma_LV.clear();
  for(uint i=0;i<gammas.size();i++)
    if(gammas[i].second == 2)
      gamma_LV.push_back(
			 prepare_gamma_LV(e.vCaloClus(gammas[i].first).X(),
					  e.vCaloClus(gammas[i].first).Y(),
					  e.vCaloClus(gammas[i].first).Z(),
					  e.vCaloClus(gammas[i].first).E(),v)
			 );
  plot_mass_if_two(gamma_LV, *( (TH2D*)h.GetObject("ECAL2mass_s") ));
  /*-------------------------------------------------------------------------------*/

  h.Fill1D("nGamma",gammas.size(),1, 20, -0.5, 19.5);
  if(gammas.size() < 2) return;
  
  vector<int> hg;
  for(uint i=0;i<gammas.size();i++) {
    if(gammas[i].second == 1 && e.vCaloClus(gammas[i].first).E()>ECAL1_TH) hg.push_back(i);
    if(gammas[i].second == 2 && e.vCaloClus(gammas[i].first).E()>ECAL2_TH) hg.push_back(i);
  }  

  h.Fill1D("nHighGamma",hg.size(),1, 10, -0.5,9.5);
  if(hg.size() != NGAMMA) return;

  /*-----------------------------------------End of cuts---------------------------------------*/  
  double Esum=0; 
  Esum += e.vParticle(op[0]).ParInVtx(pv[0]).Mom();
  Esum += e.vCaloClus(gammas[hg[0]].first).E();
  Esum += e.vCaloClus(gammas[hg[1]].first).E();
  h.Fill1D ("Esum", Esum, 1, 500, 0, 250, "Sum of |Energies|;|p_{-}+E_{g1}+E_{g2}|" );

  double g1[5];
  prepare_gamma_V(e.vCaloClus(gammas[hg[0]].first).X(),
		  e.vCaloClus(gammas[hg[0]].first).Y(),
		  e.vCaloClus(gammas[hg[0]].first).Z(),
		  e.vCaloClus(gammas[hg[0]].first).E(),
		  v,
		  g1);
  hlmass5(g1,g1,0);

  double g2[5];
  prepare_gamma_V(e.vCaloClus(gammas[hg[1]].first).X(),
		  e.vCaloClus(gammas[hg[1]].first).Y(),
		  e.vCaloClus(gammas[hg[1]].first).Z(),
		  e.vCaloClus(gammas[hg[1]].first).E(),
		  v,
		  g2);
  hlmass5(g2,g2,0);
  double gg[5];
  hladdt5(g1,g2,gg);

  h.Fill1D ("GGMass", gg[4], 1, 200, 0, 0.5, "Mass of the two gammas;M_{#gamma#gamma}" );

  e.TagToSave();
}
