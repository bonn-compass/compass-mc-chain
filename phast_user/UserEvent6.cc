#include <iostream>
#include <cmath>
#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "TBranch.h"
#include "Phast.h"
#include "UserInfo.h"

//
// Example how to read user-defined objects of class "UserInfo"
// saved in events' tree of mDST (see UserEvent5.cc)
//


void UserEvent6(PaEvent& e)   {
  
  static UserInfo inf;
  static bool first = true;

  Phast& ph = Phast::Ref();
  

  if(first){
    static UserInfo* ptr_userinfo = &inf;
    const char* usrclass_name = ptr_userinfo->GetName();
    TBranch*  user_branch = ph.in_event_tree->GetBranch(usrclass_name); 
    if (user_branch == NULL) return;
    user_branch->SetAddress(&ptr_userinfo);
    user_branch->GetEntry(ph.iev_en);
    first = false;
  }

  cout<<e.EvInSpill()<<"  "<<inf.Ev()<<endl;
}

