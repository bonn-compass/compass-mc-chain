//
// Example on how to store ROOT objects into a tree.
//
// This example selects 3pi events and stores the track parameters
// into a tree.  The accompanying scripts rootscripts/example_3pi.C
// and rootscripts/example_3pi_driver.C illustrate how to access the
// contents of such a tree on the phast level.
//
// author: Tobias.Schlueter@physik.uni-muenchen.de

#include "TTree.h"
#include "TClonesArray.h"
#include "PaEvent.h"

namespace {
  int run;
  int spill;
  int evInSpill;
  int trigMask;
  TVector3* vPV;
  TVector3* pBeam;
  TClonesArray *tracks;

  TTree *tree;

  void
  init()
  {
    TTree* t = tree = new TTree("tree3pi", "3 pi example tree");
    t->Branch("run", &run, "run/I");
    t->Branch("spill", &spill, "spill/I");
    t->Branch("evInSpill", &evInSpill, "evInSpill/I");
    t->Branch("trigMask", &trigMask, "trigMask/I");

    // For reasons unbeknowenst to me ROOT needs a pointer to a
    // pointer, which means that we have to use pointers for the
    // objects that we store in a tree.
    vPV = new TVector3();
    t->Branch("vPV", "TVector3", &vPV);
    pBeam = new TVector3();
    t->Branch("pBeam", "TVector3", &pBeam);

    // We store the parameters of the outgoing tracks in a
    // TClonesArray to illustrate its usage.
    tracks = new TClonesArray("PaTPar");
    t->Branch("tracks", &tracks, 256000, 1);
  }
}

void
UserEvent60(PaEvent& e)
{
  static bool first = true;
  if (first)
    {
      first = false;
      init();
    }
  
  run = e.RunNum();
  spill = e.SpillNum();
  evInSpill = e.EvInSpill();
  trigMask = e.TrigMask();

  int iPV = e.iBestPrimaryVertex();
  if (iPV == -1)
    return;
  const PaVertex& pv = e.vVertex(iPV);
  vPV->SetXYZ(pv.X(), pv.Y(), pv.Z());

  int nTrackPV = pv.NOutParticles();
  if (nTrackPV != 3)
    return;

  // Check charge conservation
  int q = 0;
  for (int i = 0; i < nTrackPV; i++)
    {
      int iPart = pv.iOutParticle(i);
      const PaParticle& part(e.vParticle(iPart));
      // const PaTPar& par(part.ParInVtx(iPV));
      q += part.Q();
    }

  if (q != -1)
    return;


  //
  // Get the beam direction.
  int iBeam = pv.InParticle();
  if (iBeam == -1)
    return;
  const PaParticle& beamPart(e.vParticle(iBeam));
  const PaTPar& beamPar(beamPart.ParInVtx(iPV));
  *vPV = beamPar.Mom3();

  // I clear the array here, it may happen that I add a return between
  // the filling of the array below and the tracks->Clear() below, in
  // a later revision of this code so I do this superfluous call to
  // make sure that I'm not leaking memory, not now and not in the
  // future.
  tracks->Clear();
  // SetOwner guarantees that we determine when to clear out the
  // contents of the array.
  tracks->SetOwner();

  //
  // Record the parameters of the tracks leaving the primary vertex.
  for (int i = 0; i < nTrackPV; i++)
    {
      int iPart = pv.iOutParticle(i);
      const PaParticle& part(e.vParticle(iPart));
      const PaTPar& par(part.ParInVtx(iPV));

      // This is the magic syntax for allocating & filling the i'th
      // entry of the TClonesArray.
      // It does the following:
      // it allocates the i'th element of the array *tracks (the
      // new(...)) and then fills it with a copy of par (the
      // PaTPar(par) part).
      new((*tracks)[i]) PaTPar(par);
    }

  // Everything's setup, let's fill the tree.
  tree->Fill();

  // Clear the array, so we don't leak memory.
  tracks->Clear();
}
