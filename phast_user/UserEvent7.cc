#ifdef CORAL_IN_USERFUNC

// CORAL includes
#include "CsInit.h"
#include "CsGeom.h"
#include "CsEvent.h"
#include "CsTrack.h"
#include "CsBeam.h"
#include "CsVertex.h"
#include "CsParticle.h"
#ifdef USE_Oracle
#include "CsOraStore.h"
#endif
#include "DaqDataDecoding/OnlineFilter.h"
#include "Reco/CalorimeterParticle.h"

// PHAST includes
#include "PaEvent.h"

//
// Test of special mode in which CORAL library could be called from inside user functions.
//
// WARNING: this feature is needed for very specific purposes.
// NOT for general use.
// To compile PHAST with this feature:
//   $make clean
//   $cd coral
//   $make CORAL_IN_USERFUNC=1
// resulting executable: pcol.exe
//  
// To go to normal Phast mode one has to "make clean; make"
//

void UserEvent7(PaEvent& e)
{
  // ... senseless actions. Just to test an access to CORAL classes.
  CsEvent* ev = CsEvent::Instance();
  ev->getNextEvent();

}

#endif
