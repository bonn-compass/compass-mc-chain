#include <bitset>
#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#include "mass.h"
#include "heplib.h"
#include "MHister.h"

#include "MHistogramer.h"
#include "MSystem.h"

#define _USE_MATH_DEFINES
#include <cmath>

/*
  The program makes preselection for three-charge, two-gamma final state.
  GG-mass: eta interval + pi0 interval.
*/

//parameters of cuts, which are applied:
#define N_PV 1
#define NOUT_PART 3
#define QSUM -1
#define ECAL1_TH 5
#define ECAL2_TH 3
#define ZVERTEX_L -80
#define ZVERTEX_R -20
#define NGAMMA 2
//gg inv mass
#define GG_L1 0.10
#define GG_R1 0.18
#define GG_L2 0.48
#define GG_R2 0.62
//3pi inv mass
#define PI3_L1 0.48
#define PI3_R1 0.62
#define PI3_L2 0.65
#define PI3_R2 0.91
//exclusivity
#define EXCLUS_L 160

using namespace std;

TLorentzVector prepare_gamma_LV(double x,double y,double z,double E, double v[]);

void UserEvent73(PaEvent& e) {

  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHistogramer h( ( sh_file_name+".mine" ).c_str() );

  static bool first(true);
  if(first){ 

    h.BookVertex();
    h.BookKinematics();
    h.BookRecoilAdvanced();

    first=false;
  } 

  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/
  int trig = e.TrigMask();
  std::bitset<12> mask_from_event(trig);
  for(int i=0;i<12;i++) 
    if(mask_from_event[i]) 
      h.Fill1D("trigger",i,1,
	       12,-0.5,11.5,"Trigger;bit");

  /*------------------------------------primary verteces---------------------------------------*/
  vector<int> pv;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { pv.push_back(iv); }
  }
  h.Fill1D( "Npv", pv.size(), 1, 20, -0.5, 19.5, "Amount of PV;Npv" );
  if( pv.size() != N_PV ) return;
  h.Fill1D( "vz", e.vVertex(pv[0]).Z(), 1, 1500,-500,1000 );
  if( e.vVertex(pv[0]).Z()>ZVERTEX_R || e.vVertex(pv[0]).Z()<ZVERTEX_L ) return;

  /*--------------------------------------Outgoing particles-----------------------------------*/
  vector<int> op;
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& vi = e.vVertex(pv[i]);
    for(int j=0;j<vi.NOutParticles();j++) {
      double pMom = e.vParticle( vi.iOutParticle(j) ).ParInVtx(pv[i]).qP();
      h.Fill1D("OUTp",pMom,1,
	       250, -250, 250, "Momentum of track;p[GeV]" );
      op.push_back( vi.iOutParticle(j) );
    }
  }

  h.Fill1D ("OUTpv", op.size(), 1, 10, -0.5, 9.5, "Amount of OUT-particles from PVs;Np" );
  if( op.size() != NOUT_PART ) return;

  int sum_of_charges=0;
  for(uint i=0;i<op.size();i++) sum_of_charges += e.vParticle(op[i]).Q();
  h.Fill1D ("qSum", sum_of_charges, 1, 7, -3.5, 3.5, "sum of charges;sum Q" );
  if( sum_of_charges != QSUM ) return; 
  
  /*-----------------------------------------Calorimetry---------------------------------------*/
  std::vector<int> gammas1,gammas2;
  
  const int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    if(part.Q()==0) {
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	const PaCaloClus &cl = e.vCaloClus(iClast);
	if( cl.iCalorim() == 0 ) gammas1.push_back(iClast);
	if( cl.iCalorim() == 1 ) gammas2.push_back(iClast);
      }
    }
  }
  
  h.Fill1D("ECAL1m",gammas1.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL1;E[GeV];Nsh");
  h.Fill1D("ECAL2m",gammas2.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL2;E[GeV];Nsh");

  h.Fill1D("ECAL1p2m",gammas1.size()+gammas2.size(), 1, 
	   20,-0.5, 19.5, "Amount of shower ECAL1+ECAL2;E[GeV];Nsh");
  if(gammas1.size()+gammas2.size()<NGAMMA) return;

  std::vector<int> position_of_good;
  for(uint i=0;i<gammas1.size();i++) 
    if(e.vCaloClus(gammas1[i]).E()>ECAL1_TH) position_of_good.push_back(gammas1[i]);
  for(uint i=0;i<gammas2.size();i++) 
    if(e.vCaloClus(gammas2[i]).E()>ECAL2_TH) position_of_good.push_back(gammas2[i]);

  h.Fill1D("Ngamma_more_th",gammas1.size()+gammas2.size(),1,
	   20,-0.5,19.5);
  if(position_of_good.size() != NGAMMA) return;

  /*---------------------------------------Invariant mass--------------------------------------*/   

  double v[] = {e.vVertex(pv[0]).X(),
		e.vVertex(pv[0]).Y(),
		e.vVertex(pv[0]).Z()};

  TLorentzVector g1_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[0]).X(),
					  e.vCaloClus(position_of_good[0]).Y(),
					  e.vCaloClus(position_of_good[0]).Z(),
					  e.vCaloClus(position_of_good[0]).E(),v);
  TLorentzVector g2_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[1]).X(),
					  e.vCaloClus(position_of_good[1]).Y(),
					  e.vCaloClus(position_of_good[1]).Z(),
					  e.vCaloClus(position_of_good[1]).E(),v);
  
  TLorentzVector gg_lv=g1_lv+g2_lv;
  h.Fill1D("gg_mass",gg_lv.M(),1,
	   200,0,0.7);

  if(e.vParticle(op[1]).Q()==+1) swap(op[1],op[0]);
  if(e.vParticle(op[2]).Q()==+1) swap(op[2],op[0]);  

  const TLorentzVector p1_lv = e.vParticle(op[0]).ParInVtx(pv[0]).LzVec(PI_MASS);
  const TLorentzVector p2_lv = e.vParticle(op[1]).ParInVtx(pv[0]).LzVec(PI_MASS);
  const TLorentzVector p3_lv = e.vParticle(op[2]).ParInVtx(pv[0]).LzVec(PI_MASS);

  bool gg_cut = false;
  if( gg_lv.M()>GG_L1 && gg_lv.M()<GG_R1 ) {
    TLorentzVector three_pi_lv1 = p1_lv+p2_lv+gg_lv;
    TLorentzVector three_pi_lv2 = p1_lv+p3_lv+gg_lv;
    h.Fill1D("pipipi_mass",three_pi_lv1.M(),1, 200,0,1);
    h.Fill1D("pipipi_mass",three_pi_lv2.M()           );
    gg_cut = true;
  }
  if( gg_lv.M()>GG_L2 && gg_lv.M()<GG_R2 ) {
    TLorentzVector three_pi_lv1 = p1_lv+p2_lv+gg_lv;
    TLorentzVector three_pi_lv2 = p1_lv+p3_lv+gg_lv;
    h.Fill1D("pipieta_mass",three_pi_lv1.M(),1, 200,0,1.5);
    h.Fill1D("pipieta_mass",three_pi_lv2.M()             );
    gg_cut = true;
  }
  if(!gg_cut) return;

  TLorentzVector sum_lv = p1_lv+p2_lv+p3_lv+gg_lv;
  h.Fill1D("m4pi",sum_lv.M(),1, 200,0,3);
  
  TLorentzVector b0 = e.vParticle(e.vVertex(pv[0]).InParticle()).ParInVtx(pv[0]).LzVec(PI_MASS);
  double beam[] = {b0.X(),b0.Y(),b0.Z(),b0.E(),b0.M()}; 

  MSystem sys(beam, PROT_MASS);
  sys.AddParticle(+1,p1_lv.X(),p1_lv.Y(),p1_lv.Z(),PI_MASS);
  sys.AddParticle(-1,p2_lv.X(),p2_lv.Y(),p2_lv.Z(),PI_MASS);
  sys.AddParticle(-1,p3_lv.X(),p3_lv.Y(),p3_lv.Z(),PI_MASS);
  sys.AddParticle( 0,gg_lv.X(),gg_lv.Y(),gg_lv.Z(),gg_lv.M());

  MSystem osys(sys); 
  sys.ReCulcBeamVector(PROT_MASS,PROT_MASS);

  h.Fill1D ("Esum_before_RPD", sys.freso()[3], 1, 500, 0, 210, "Sum of E_{i};" );
  if(sum_lv.P() < EXCLUS_L ) return;

  /*-----------------------------------------End of cuts---------------------------------------*/  
  h.FillVertex(v);
  h.FillKinematics(sys);
  h.FillRecoilAdvanced(osys);

  e.TagToSave();
}
