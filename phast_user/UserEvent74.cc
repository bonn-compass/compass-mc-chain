#include <bitset>
#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#include "mass.h"
#include "heplib.h"
#include "MHister.h"

#include "MHistogramer.h"
#include "MSystem.h"

#include "RPD_Helper.h"

#include "worklib.h"

#define _USE_MATH_DEFINES
#include <cmath>

/*
  The program makes selection for three-charge, two-gamma final state.
  Loop over all gammas combinations
*/

//parameters of cuts, which are applied:
#define N_PV 1
#define NOUT_PART 3
#define ECAL1_TH 1.5
#define ECAL2_TH 1.5
#define NGAMMA 2
#define QSUM -1
#define PI0_CUT 0.02
#define ETA_CUT 0.02
#define TRACK_MOMENTUM_CUT -180

using namespace std;

void UserEvent74(PaEvent& e) {

  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHistogramer h( ( sh_file_name+".mine" ).c_str() );
  //static MHister h( ( sh_file_name+".mine" ).c_str() );

  static bool first(true);
  if(first){ 
    h.BookVertex();
    h.BookKinematics();
    h.BookRecoilAdvanced();
    first=false;
  } 

  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/
  int trig = e.TrigMask();
  std::bitset<12> mask_from_event(trig);
  for(int i=0;i<12;i++) 
    if(mask_from_event[i]) 
      h.Fill1D("trigger",i,1,
	       12,-0.5,11.5,"Trigger;bit");

  /*------------------------------------primary verteces---------------------------------------*/
  vector<int> pv;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { pv.push_back(iv); }
  }
  h.Fill1D( "Npv", pv.size(), 1, 20, -0.5, 19.5, "Amount of PV;Npv" );
  if( pv.size() != N_PV ) return;
  h.Fill1D( "vz", e.vVertex(pv[0]).Z(), 1, 1500,-500,1000 );
  if( e.vVertex(pv[0]).Z()>-35 || e.vVertex(pv[0]).Z() < -75 ) return;

  /*--------------------------------------Outgoing particles-----------------------------------*/
  vector<int> op;
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& vi = e.vVertex(pv[i]);
    for(int j=0;j<vi.NOutParticles();j++) {
      double pMom = e.vParticle( vi.iOutParticle(j) ).ParInVtx(pv[i]).qP();
      h.Fill1D("OUTp",pMom,1,
	       250, -250, 250, "Momentum of track;p[GeV]" );
      op.push_back( vi.iOutParticle(j) );
    }
  }

  h.Fill1D ("OUTpv", op.size(), 1, 10, -0.5, 9.5, "Amount of OUT-particles from PVs;Np" );
  if( op.size() != NOUT_PART ) return;

  int sum_of_charges=0;
  for(uint i=0;i<op.size();i++) sum_of_charges += e.vParticle(op[i]).Q();
  h.Fill1D ("qSum", sum_of_charges, 1, 7, -3.5, 3.5, "sum of charges;sum Q" );
  if( sum_of_charges != QSUM ) return; 

  if(e.vParticle(op[1]).Q()==+1) swap(op[1],op[0]);
  if(e.vParticle(op[2]).Q()==+1) swap(op[2],op[0]);
  const TLorentzVector p1_lv = e.vParticle(op[0]).ParInVtx(pv[0]).LzVec(PI_MASS);
  const TLorentzVector p2_lv = e.vParticle(op[1]).ParInVtx(pv[0]).LzVec(PI_MASS);
  const TLorentzVector p3_lv = e.vParticle(op[2]).ParInVtx(pv[0]).LzVec(PI_MASS);
  
  /*-----------------------------------------Calorimetry---------------------------------------*/
  std::vector<int> gammas1,gammas2;
  
  const int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    if(part.Q()==0) {
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	const PaCaloClus &cl = e.vCaloClus(iClast);
	if( cl.iCalorim() == 0 ) gammas1.push_back(iClast);
	if( cl.iCalorim() == 1 ) gammas2.push_back(iClast);
	if(j!=0) cerr << "Error<>: You don't understand something." << endl;
      }
    }
  }
  
  h.Fill1D("ECAL1m",gammas1.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL1;E[GeV];Nsh");
  h.Fill1D("ECAL2m",gammas2.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL2;E[GeV];Nsh");
  h.Fill1D("ECAL1p2m",gammas1.size()+gammas2.size(), 1, 
	   20,-0.5, 19.5, "Amount of shower ECAL1+ECAL2;E[GeV];Nsh");

  for(uint i=0;i<gammas1.size();i++) h.Fill1D("ECAL1e",e.vCaloClus(gammas1[i]).E(),1,300,0,90.);
  for(uint i=0;i<gammas2.size();i++) h.Fill1D("ECAL2e",e.vCaloClus(gammas2[i]).E(),1,300,0,90.);

  if(gammas1.size()+gammas2.size()<NGAMMA) return;

//  std::vector<int> position_of_good;
//  for(uint i=0;i<gammas1.size();i++) 
//    if(e.vCaloClus(gammas1[i]).E()>ECAL1_TH) position_of_good.push_back(gammas1[i]);
//  for(uint i=0;i<gammas2.size();i++) 
//    if(e.vCaloClus(gammas2[i]).E()>ECAL2_TH) position_of_good.push_back(gammas2[i]);
//
//  h.Fill1D("Ngamma_more_th",gammas1.size()+gammas2.size(),1,
//	   20,-0.5,19.5);

  //threshold
//  for(int i=0;i<int(gammas1.size())&&gammas1.size()!=0;i++) {
//    const PaCaloClus & cl = e.vCaloClus(gammas1[i]);
//    if(cl.E() < ECAL1_TH) {gammas1.erase(gammas1.begin()+i);i--;}
//  }
//  for(uint i=0;i<gammas2.size();i++) {
//    const PaCaloClus & cl = e.vCaloClus(gammas2[i]);
//    if(cl.E() < ECAL2_TH) {gammas2.erase(gammas2.begin()+i);i--;}
//  }

  //loop over all combinations
  double v[] = {e.vVertex(pv[0]).X(),
		e.vVertex(pv[0]).Y(),
		e.vVertex(pv[0]).Z()};
  std::vector<std::pair<int,TLorentzVector*> > vG;
  for(uint i=0;i<gammas1.size()+gammas2.size();i++) {
    const PaCaloClus & cl = e.vCaloClus(i<gammas1.size() ? gammas1[i] : gammas2[i-gammas1.size()]);
    vG.push_back(std::make_pair<int,TLorentzVector*> 
		 ( (i<gammas1.size()) ? 1 : 2, 
		   new TLorentzVector(GetPgammaLV(cl.X(),cl.Y(),cl.Z(),cl.E(),v)) ) );
  }
  
  //fill all combinations information
  int Npi0candidate = 0;
  for(int i=0;i<int(vG.size())-1;i++) {
    for(uint j=i+1;j<vG.size();j++) {
      TLorentzVector *li = vG[i].second;
      TLorentzVector *lj = vG[j].second;
      TLorentzVector sum(*li+*lj);
      h.Fill1D("gg_mass",sum.M(),1,100,0.,0.7,"inv gg-mass;M_{#gamma#gamma} (GeV)");
      h.Fill2D("gg_mass_vs_minE",sum.M(),(li->E()<lj->E()) ? li->E() : lj->E(),1.0,100,0.,0.7,100,0.,10.,
	       "inv gg-mass vs E_{#gamma} min;M_{#gamma#gamma} (GeV);min E_{#gamma}");
      h.Fill2D("gg_mass_vs_avgE",sum.M(),(li->E()+lj->E())/2.,1.0,100,0.,0.7,100,0.,50.,
	       "inv gg-mass vs E_{#gamma} avg;M_{#gamma#gamma} (GeV);avg E_{#gamma}");
      if(sum.M()>0.120&&sum.M()<0.145) Npi0candidate++;
    }  
  }
  h.Fill1D("Npi0candidate",Npi0candidate,1,10,-0.5,9.5);
//  const PaCaloClus & cl1 = e.vCaloClus(position_of_good[0]);
//  TLorentzVector g1_lv = GetPgammaLV(cl1.X(),cl1.Y(),cl1.Z(),cl1.E(),v);
//  const PaCaloClus & cl2 = e.vCaloClus(position_of_good[1]);
//  TLorentzVector g2_lv = GetPgammaLV(cl2.X(),cl2.Y(),cl2.Z(),cl2.E(),v);
//  
//  TLorentzVector gg_lv=g1_lv+g2_lv;
//  h.Fill1D("gg_mass",gg_lv.M(),1,
//	   200,0,0.7);
//  if( fabs(gg_lv.M()-PI0_MASS) > PI0_CUT ) return;
  
  /*---------------------------------------Invariant mass--------------------------------------*/
  //      LOOP HERE

//  //build MSystem
//  MSystem sys(beam, PROT_MASS);
//  sys.AddParticle(+1,p1_lv.X(),p1_lv.Y(),p1_lv.Z(),p1.M());
//  sys.AddParticle(-1,p2_lv.X(),p2_lv.Y(),p2_lv.Z(),p2.M());
//  sys.AddParticle(-1,p3_lv.X(),p3_lv.Y(),p3_lv.Z(),p3.M());
//  sys.AddParticle( 0,gg_lv.X(),gg_lv.Y(),gg_lv.Z(),gg_lv.M());
//  MSystem osys(sys); 
//  sys.ReCulcBeamVector(PROT_MASS,PROT_MASS);
//
//  //Check invariant masses
//  TLorentzVector three_pi_lv1 = p1_lv+p2_lv+gg_lv;
//  TLorentzVector three_pi_lv2 = p1_lv+p3_lv+gg_lv;
//  h.Fill1D("pipipi_mass",three_pi_lv1.M(),1, 200,0,1);
//  h.Fill1D("pipipi_mass",three_pi_lv2.M()           );
//  if( fabs(three_pi_lv1.M()-ETA_MASS)>ETA_CUT && 
//      fabs(three_pi_lv2.M()-ETA_MASS)>ETA_CUT ) return;
//
//  TLorentzVector sum_lv = p1_lv+p2_lv+p3_lv+gg_lv;
//  h.Fill1D("m4pi",sum_lv.M(),1, 200,0,3);
//  
//  TLorentzVector b0 = e.vParticle(e.vVertex(pv[0]).InParticle()).ParInVtx(pv[0]).LzVec(PI_MASS);
//  double beam[] = {b0.X(),b0.Y(),b0.Z(),b0.E(),b0.M()}; 
//
//  h.Fill1D ("Esum_before_RPD", sys.freso()[3], 1, 500, 0, 210, "Sum of E_{i};" );
//  /*--------------------------------------Information from RPD---------------------------------*/
//
//  static RPD *myproton;
//  myproton = &RPD::Instance();
//  myproton->Search(e.vVertex(pv[0]));
//  //multiplicity
//  vector <TLorentzVector> protons = myproton->vTracks();
//  h.Fill1D("RPDm",protons.size(),1, 10,-0.5,9.5,"multiplicity in RPD");
//  if(protons.size() != 1) return;
//
//  //is there best  
//  int iBestProtonTrack = myproton->iBestProtonTrack();
//  h.Fill1D("IsBest",iBestProtonTrack+1, 1,
//	   2,-0.5,1.5,"Is There 'Best Proton Track'");
//
//  if(myproton->iBestProtonTrack() == -1) return;
//
//  double phiRPD = protons[0].Phi();
//  double phi0 = atan2(sys.frecl()[1],sys.frecl()[0]);
//  h.Fill1D("phiRPD",phiRPD,1, 100, -M_PI, M_PI);
//  h.Fill1D("phi0",  phi0,   1, 100, -M_PI, M_PI);
//  h.Fill1D("dPhi",  phi0-phiRPD, 1, 100, -2.0*M_PI, 2.0*M_PI);
//
//  if( fabs(phiRPD-phi0) > 0.17) return;
//
//  h.Fill1D ("Esum_after_RPD", sys.freso()[3], 1, 500, 0, 210, "Sum of E_{i};" );
//
//  /*-----------------------------------------End of cuts---------------------------------------*/  
//  h.FillVertex(v);
//  h.FillKinematics(sys);
//  h.FillRecoilAdvanced(osys);

  e.TagToSave();
}
