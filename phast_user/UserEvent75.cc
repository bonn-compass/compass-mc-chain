//Mikhail Mikhasenko
//05.2015

#include <bitset>
#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#define  PI_MASS    0.139570
#define  PI0_MASS   0.134976
#define  ETA_MASS   0.547300

#define _USE_MATH_DEFINES
#include <cmath>

/*
  The program makes preselection for three-charge, two-gamma final state.
  Preselection of pi-pi-pi+(gamma gamma)
  
  histograms are removed
*/

//parameters of cuts, which are applied:
#define N_PV 1
#define NOUT_PART 3
#define QSUM -1
#define ECAL1_TH 5
#define ECAL2_TH 3
#define ZVERTEX_L -80
#define ZVERTEX_R -20
#define NGAMMA 2
//gg inv mass
#define GG_L1 0.10
#define GG_R1 0.18
#define GG_L2 0.48
#define GG_R2 0.62
//3pi inv mass
#define PI3_L1 0.48
#define PI3_R1 0.62
#define PI3_L2 0.65
#define PI3_R2 0.91
//exclusivity
#define EXCLUS_L 160

using namespace std;

TLorentzVector prepare_gamma_LV(double x,double y,double z,double E, double v[]) {
  
  double dist = sqrt((x-v[0])*(x-v[0])+(y-v[1])*(y-v[1])+(z-v[2])*(z-v[2]));
  return TLorentzVector((x-v[0])/dist*E,(y-v[1])/dist*E,(z-v[2])/dist*E, E);
}

void UserEvent75(PaEvent& e) {

  //collect ctatictics
  UserEvent9999(e);

  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/

  /*------------------------------------primary verteces---------------------------------------*/
  vector<int> pv;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { pv.push_back(iv); }
  }
  if( pv.size() != N_PV ) return;
  if( e.vVertex(pv[0]).Z()>ZVERTEX_R || e.vVertex(pv[0]).Z()<ZVERTEX_L ) return;

  /*--------------------------------------Outgoing particles-----------------------------------*/
  vector<int> op;
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& vi = e.vVertex(pv[i]);
    for(int j=0;j<vi.NOutParticles();j++) {
      //double pMom = e.vParticle( vi.iOutParticle(j) ).ParInVtx(pv[i]).qP();
      op.push_back( vi.iOutParticle(j) );
    }
  }

  if( op.size() != NOUT_PART ) return;

  int sum_of_charges=0;
  for(uint i=0;i<op.size();i++) sum_of_charges += e.vParticle(op[i]).Q();
  if( sum_of_charges != QSUM ) return; 
  
  /*-----------------------------------------Calorimetry---------------------------------------*/
  std::vector<int> gammas1,gammas2;
  
  const int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    if(part.Q()==0) {
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	const PaCaloClus &cl = e.vCaloClus(iClast);
	if( cl.iCalorim() == 0 ) gammas1.push_back(iClast);
	if( cl.iCalorim() == 1 ) gammas2.push_back(iClast);
      }
    }
  }
  
  if(gammas1.size()+gammas2.size()<NGAMMA) return;

  std::vector<int> position_of_good;
  for(uint i=0;i<gammas1.size();i++) 
    if(e.vCaloClus(gammas1[i]).E()>ECAL1_TH) position_of_good.push_back(gammas1[i]);
  for(uint i=0;i<gammas2.size();i++) 
    if(e.vCaloClus(gammas2[i]).E()>ECAL2_TH) position_of_good.push_back(gammas2[i]);

  if(position_of_good.size() != NGAMMA) return;

  /*---------------------------------------Invariant mass--------------------------------------*/   

  double v[] = {e.vVertex(pv[0]).X(),
		e.vVertex(pv[0]).Y(),
		e.vVertex(pv[0]).Z()};

  TLorentzVector g1_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[0]).X(),
					  e.vCaloClus(position_of_good[0]).Y(),
					  e.vCaloClus(position_of_good[0]).Z(),
					  e.vCaloClus(position_of_good[0]).E(),v);
  TLorentzVector g2_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[1]).X(),
					  e.vCaloClus(position_of_good[1]).Y(),
					  e.vCaloClus(position_of_good[1]).Z(),
					  e.vCaloClus(position_of_good[1]).E(),v);
  
  TLorentzVector gg_lv=g1_lv+g2_lv;

  if(e.vParticle(op[1]).Q()==+1) swap(op[1],op[0]);
  if(e.vParticle(op[2]).Q()==+1) swap(op[2],op[0]);  

  const TLorentzVector p1_lv = e.vParticle(op[0]).ParInVtx(pv[0]).LzVec(PI_MASS);
  const TLorentzVector p2_lv = e.vParticle(op[1]).ParInVtx(pv[0]).LzVec(PI_MASS);
  const TLorentzVector p3_lv = e.vParticle(op[2]).ParInVtx(pv[0]).LzVec(PI_MASS);

  bool gg_cut = false;
  if( gg_lv.M()>GG_L1 && gg_lv.M()<GG_R1 ) {
    TLorentzVector three_pi_lv1 = p1_lv+p2_lv+gg_lv;
    TLorentzVector three_pi_lv2 = p1_lv+p3_lv+gg_lv;
    gg_cut = true;
  }
  if( gg_lv.M()>GG_L2 && gg_lv.M()<GG_R2 ) {
    TLorentzVector three_pi_lv1 = p1_lv+p2_lv+gg_lv;
    TLorentzVector three_pi_lv2 = p1_lv+p3_lv+gg_lv;
    gg_cut = true;
  }
  if(!gg_cut) return;

  TLorentzVector sum_lv = p1_lv+p2_lv+p3_lv+gg_lv;
  
  TLorentzVector b0 = e.vParticle(e.vVertex(pv[0]).InParticle()).ParInVtx(pv[0]).LzVec(PI_MASS);
  //double beam[] = {b0.X(),b0.Y(),b0.Z(),b0.E(),b0.M()}; 

  if(sum_lv.P() < EXCLUS_L ) return;

  /*-----------------------------------------End of cuts---------------------------------------*/  

  e.TagToSave();
}
