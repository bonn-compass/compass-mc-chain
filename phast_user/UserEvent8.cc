#include <iostream>
#include <cmath>
#include <stdio.h>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "PaDigit.h"

// misc examples how to get calorimeter clusters info

void UserEvent8(PaEvent& e)   {
  
  static TH1D* h1[20];
  static TH2D* h2[20];
  static bool first(true);
  if(first){  
    Phast::Ref().HistFileDir("Calorim test");
    h1[1] = new TH1D("h1_1","ECAL1 cluster size (Ncells)",20,0,20);
    h1[5] = new TH1D("h1_5","ECAL1 cluster E",100,0,30);
    h1[6] = new TH1D("h1_6","ECAL1 cluster E",100,0, 5);

    h2[1] = new TH2D("h2_1","ECAL1 cluster Y VS X",               250,-250, 250, 150, -150, 150);
    h2[2] = new TH2D("h2_2","ECAL1 cluster Y VS X (cl. size > 1)",250,-250, 250, 150, -150, 150);

    h1[11] = new TH1D("h1_11","ECAL2 cluster size (Ncells)",20,0,20);
    h1[15] = new TH1D("h1_15","ECAL2 cluster E",100,0,30);
    h1[16] = new TH1D("h1_16","ECAL2 cluster E",100,0, 5);

    h2[11] = new TH2D("h2_11","ECAL2 cluster Y VS X",               100,-250, 250, 100, -150, 150);
    h2[12] = new TH2D("h2_12","ECAL2 cluster Y VS X (cl. size > 1)",100,-250, 250, 100, -150, 150);
    first=false;
  } // end of histogram booking
  
  for(int i=0; i < e.NCaloClus(); i++){ // loop over calo clusters
    const PaCaloClus& cl = e.vCaloClus(i);
    const string& nam = cl.CalorimName();
    if(nam.find("EC12P1") != 0){ // ECAL1 clusters
      int ncells = cl.vCellNumber().size();
      h1[1]->Fill(ncells+0.5);
      h1[5]->Fill(cl.E());
      h1[6]->Fill(cl.E());
      h2[1]->Fill(cl.X(), cl.Y());
      if(ncells > 1) h2[2]->Fill(cl.X(), cl.Y());
    }
  }

  for(int i=0; i < e.NCaloClus(); i++){ // loop over calo clusters
    const PaCaloClus& cl = e.vCaloClus(i);
    const string& nam = cl.CalorimName();
    if(nam.find("EC02P1") != 0){ // ECAL2 clusters
      int ncells = cl.vCellNumber().size();
      h1[11]->Fill(ncells+0.5);
      h1[15]->Fill(cl.E());
      h1[16]->Fill(cl.E());
      h2[11]->Fill(cl.X(), cl.Y());
      if(ncells > 1) h2[12]->Fill(cl.X(), cl.Y());
    }
  }// end of loop over calo clusters

}
