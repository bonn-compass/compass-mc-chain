#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#include "mass.h"
#include "heplib.h"
#include "MHister.h"

#include "MHistogramer.h"
#include "MSystem.h"

#include "RPD_Helper.h"

#define _USE_MATH_DEFINES
#include <cmath>

/*
  The program makes selection for pi+pi-pi0;
*/


//bool myfunction (int i,int j) { return (i<j); }

//parameters of cuts, which are applied:
#define N_PV 1
#define NOUT_PART 2
#define ECAL1_TH 1.0
#define ECAL2_TH 4.0
#define NGAMMA 2
#define QSUM 0
#define PI0_CUT 0.02
#define ETA_CUT 0.02
#define TRACK_MOMENTUM_CUT -180

#define VZ -72
#define VZ_CUT 10 

using namespace std;
TLorentzVector prepare_gamma_LV(double x,double y,double z,double E, double v[]);

void UserEvent83(PaEvent& e) {

  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHistogramer h( ( sh_file_name+".mine" ).c_str() );
  //static MHister h( ( sh_file_name+".mine" ).c_str() );

  static bool first(true);
  if(first){ 

    h.BookVertex();
    h.BookKinematics();
    h.BookRecoilAdvanced();

    first=false;
  } 

  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/

  /*------------------------------------primary verteces---------------------------------------*/
  vector<int> pv;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { pv.push_back(iv); }
  }
  h.Fill1D( "Npv", pv.size(), 1, 20, -0.5, 19.5, "Amount of PV;Npv" );
  if( pv.size() != N_PV ) return;
  if( fabs(e.vVertex(pv[0]).Z()-VZ)>VZ_CUT ) return;
    
  /*--------------------------------------Outgoing particles-----------------------------------*/
  vector<int> op;
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& vi = e.vVertex(pv[i]);
    for(int j=0;j<vi.NOutParticles();j++) {
      double pMom = e.vParticle( vi.iOutParticle(j) ).ParInVtx(pv[i]).qP();
      h.Fill1D("OUTp",pMom,1,
	       250, -250, 250, "Momentum of track;p[GeV]" );
      if(pMom < TRACK_MOMENTUM_CUT) continue;
      op.push_back( vi.iOutParticle(j) );
    }
  }

  h.Fill1D ("OUTpv", op.size(), 1, 10, -0.5, 9.5, "Amount of OUT-particles from PVs;Np" );
  if( op.size() != NOUT_PART ) return;

  int sum_of_charges=0;
  for(uint i=0;i<op.size();i++) sum_of_charges += e.vParticle(op[i]).Q();
  h.Fill1D ("qSum", sum_of_charges, 1, 7, -3.5, 3.5, "sum of charges;sum Q" );
  if( sum_of_charges != QSUM ) return; 
  
  /*-----------------------------------------Calorimetry---------------------------------------*/
  std::vector<int> gammas1,gammas2;
  
  const int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    if(part.Q()==0) {
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	const PaCaloClus &cl = e.vCaloClus(iClast);
	if( cl.CalorimName().find("EC01P1") != std::string::npos ) gammas1.push_back(iClast);
	if( cl.CalorimName().find("EC02P1") != std::string::npos ) gammas2.push_back(iClast);
      }
    }
  }
  
  h.Fill1D("ECAL1m",gammas1.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL1;E[GeV];Nsh");
  h.Fill1D("ECAL2m",gammas2.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL2;E[GeV];Nsh");

  h.Fill1D("ECAL1p2m",gammas1.size()+gammas2.size(), 1, 
	   20,-0.5, 19.5, "Amount of shower ECAL1+ECAL2;E[GeV];Nsh");
  if(gammas1.size()+gammas2.size()<NGAMMA) return;

  std::vector<int> position_of_good;
  for(uint i=0;i<gammas1.size();i++) 
    if(e.vCaloClus(gammas1[i]).E()>ECAL1_TH) position_of_good.push_back(gammas1[i]);
  for(uint i=0;i<gammas2.size();i++) 
    if(e.vCaloClus(gammas2[i]).E()>ECAL2_TH) position_of_good.push_back(gammas2[i]);

  h.Fill1D("Ngamma_more_th",gammas1.size()+gammas2.size(),1,
	   20,-0.5,19.5);
  if(position_of_good.size() != NGAMMA) return;

  /*---------------------------------------Invariant mass--------------------------------------*/   

  double v[] = {e.vVertex(pv[0]).X(),
		e.vVertex(pv[0]).Y(),
		e.vVertex(pv[0]).Z()};

  TLorentzVector g1_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[0]).X(),
					  e.vCaloClus(position_of_good[0]).Y(),
					  e.vCaloClus(position_of_good[0]).Z(),
					  e.vCaloClus(position_of_good[0]).E(),v);
  TLorentzVector g2_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[1]).X(),
					  e.vCaloClus(position_of_good[1]).Y(),
					  e.vCaloClus(position_of_good[1]).Z(),
					  e.vCaloClus(position_of_good[1]).E(),v);
  
  TLorentzVector gg_lv=g1_lv+g2_lv;
  h.Fill1D("gg_mass",gg_lv.M(),1,
	   200,0,0.7);
  if( fabs(gg_lv.M()-PI0_MASS) > PI0_CUT ) return;

  if(e.vParticle(op[1]).Q()==+1) swap(op[1],op[0]);
  
  const TLorentzVector p1_lv = e.vParticle(op[0]).ParInVtx(pv[0]).LzVec(PI_MASS);
  const TLorentzVector p2_lv = e.vParticle(op[1]).ParInVtx(pv[0]).LzVec(PI_MASS);

  TLorentzVector sum_lv = p1_lv+p2_lv+gg_lv;
  h.Fill1D("m3pi",sum_lv.M(),1, 200,0,3);
  
  TLorentzVector b0 = e.vParticle(e.vVertex(pv[0]).InParticle()).ParInVtx(pv[0]).LzVec(PI_MASS);
  double beam[] = {b0.X(),b0.Y(),b0.Z(),b0.E(),b0.M()}; 

  MSystem sys(beam, PROT_MASS);
  sys.AddParticle(+1,p1_lv.X(),p1_lv.Y(),p1_lv.Z(),PI_MASS);
  sys.AddParticle(-1,p2_lv.X(),p2_lv.Y(),p2_lv.Z(),PI_MASS);
  sys.AddParticle( 0,gg_lv.X(),gg_lv.Y(),gg_lv.Z(),gg_lv.M());

  MSystem osys(sys); 
  sys.ReCulcBeamVector(PROT_MASS,PROT_MASS);

  /*--------------------------------------Information from RPD---------------------------------*/
//  h.Fill1D ("Esum_before_RPD", sys.freso()[3], 1, 500, 0, 210, "Sum of E_{i};" );
//
//  static RPD *myproton;
//  myproton = &RPD::Instance();
//  myproton->Search(e.vVertex(pv[0]));
//  //multiplicity
//  vector <TLorentzVector> protons = myproton->vTracks();
//  h.Fill1D("RPDm",protons.size(),1, 10,-0.5,9.5,"multiplicity in RPD");
//  if(protons.size() != 1) return;
//
//  //is there best  
//  int iBestProtonTrack = myproton->iBestProtonTrack();
//  h.Fill1D("IsBest",iBestProtonTrack+1, 1,
//	   2,-0.5,1.5,"Is There 'Best Proton Track'");
//
//  if(myproton->iBestProtonTrack() == -1) return;
//
//  double phiRPD = protons[0].Phi();
//  double phi0 = atan2(sys.frecl()[1],sys.frecl()[0]);
//  h.Fill1D("phiRPD",phiRPD,1, 100, -M_PI, M_PI);
//  h.Fill1D("phi0",  phi0,   1, 100, -M_PI, M_PI);
//  h.Fill1D("dPhi",  phi0-phiRPD, 1, 100, -2.0*M_PI, 2.0*M_PI);
//
//  if( fabs(phiRPD-phi0) > 0.17) return;
//
//  h.Fill1D ("Esum_after_RPD", sys.freso()[3], 1, 500, 0, 210, "Sum of E_{i};" );
//
  /*-----------------------------------------End of cuts---------------------------------------*/  
  h.FillVertex(v);
  h.FillKinematics(sys);
  h.FillRecoilAdvanced(osys);

  e.TagToSave();
}
