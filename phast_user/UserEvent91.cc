#include <bitset>
#include <iostream>
#include <cmath>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TLorentzVector.h"

#include "mass.h"
#include "heplib.h"
#include "MHister.h"

#define _USE_MATH_DEFINES
#include <cmath>


#include "worklib.h"
#include "heplib.h"
#include "MHister.h"

#define N_PV 1
#define NOUT_PART 1
#define QSUM -1
#define ECAL1_TH 1.5
#define ECAL2_TH 1.5
#define ZVERTEX_L -80
#define ZVERTEX_R -20
#define NGAMMA 2
//gg inv mass
#define GG_L2 0.48
#define GG_R2 0.62
//exclusivity
#define EXCLUS_L 160
//vertex 
#define VZ 0.
#define VZ_CUT 1000.


void UserEvent91(PaEvent& e) {

  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHister h( ( sh_file_name+".mine" ).c_str() );

  static bool first(true);
  static double pi[5], g1[5], g2[5];
  static TTree *otree;
  if(first){ 
    first=false;  
    
    //h.cd_file();
    otree = new TTree("outtree","outtree",2);
    //pi-
    otree->Branch("pim",&pi[4]);
    otree->Branch("pix",&pi[0]);
    otree->Branch("piy",&pi[1]);
    otree->Branch("piz",&pi[2]);
    //g1
    otree->Branch("g1m",&g1[4]);
    otree->Branch("g1x",&g1[0]);
    otree->Branch("g1y",&g1[1]);
    otree->Branch("g1z",&g1[2]);
    //g2
    otree->Branch("g2m",&g2[4]);
    otree->Branch("g2x",&g2[0]);
    otree->Branch("g2y",&g2[1]);
    otree->Branch("g2z",&g2[2]);

    h.AssociateObject(*otree);
  } 

  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/
  int trig = e.TrigMask();
  //if(!trig) return;
  //std::bitset<12> mask_from_event(trig);
  //std::bitset<12> mask; mask[2]=1;
  //int ui_mask = mask.to_ulong();
  //bool is_kept = trig & ui_mask;
  std::bitset<12> mask_from_event(trig);
  for(int i=0;i<12;i++) 
    if(mask_from_event[i]) 
      h.Fill1D("trigger",i,1,
	       12,-0.5,11.5,"Trigger;bit");

  /*------------------------------------primary verteces---------------------------------------*/
  vector<int> pv;
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    if(v.IsPrimary()) { pv.push_back(iv); }
  }
  h.Fill1D( "Npv", pv.size(), 1, 20, -0.5, 19.5, "Amount of PV;Npv" );
  if( pv.size() != N_PV ) return;
  h.Fill1D( "vz", e.vVertex(pv[0]).Z(), 1, 500, -250, 750, "Vertex Z; Vz (cm)" );
  if( fabs(e.vVertex(pv[0]).Z()-VZ) > VZ_CUT ) return;  
  
  /*--------------------------------------Outgoing particles-----------------------------------*/
  vector<int> op;
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& vi = e.vVertex(pv[i]);
    for(int j=0;j<vi.NOutParticles();j++) {
      double pMom = e.vParticle( vi.iOutParticle(j) ).ParInVtx(pv[i]).qP();
      h.Fill1D("OUTp",pMom,1,
	       250, -250, 250, "Momentum of track;p (GeV)" );
      //if(pMom < TRACK_MOMENTUM_CUT) continue;
      op.push_back( vi.iOutParticle(j) );
    }
  }

  h.Fill1D ("OUTpv", op.size(), 1, 10, -0.5, 9.5, "Amount of OUT-particles from PVs;Np" );
  if( op.size() != NOUT_PART ) return;

  int sum_of_charges=0;
  for(uint i=0;i<op.size();i++) sum_of_charges += e.vParticle(op[i]).Q();
  h.Fill1D ("qSum", sum_of_charges, 1, 7, -3.5, 3.5, "sum of charges;sum Q" );
  if( sum_of_charges != QSUM ) return;
  
  /*-----------------------------------------Calorimetry---------------------------------------*/
  std::vector<int> gammas1,gammas2;
  
  const int Nparticle = e.NParticle();
  for(int i=0;i<Nparticle;i++) {
    const PaParticle& part = e.vParticle(i);
    if(part.Q()==0) {
      for(int j=0;j<part.NCalorim();j++) {
	int iClast = part.iCalorim(j);
	const PaCaloClus &cl = e.vCaloClus(iClast);
	if( cl.CalorimName().find("EC01P1") != std::string::npos ) gammas1.push_back(iClast);
	if( cl.CalorimName().find("EC02P1") != std::string::npos ) gammas2.push_back(iClast);
      }
    }
  }
  
  h.Fill1D("ECAL1m",gammas1.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL1;E[GeV];Nsh");
  h.Fill1D("ECAL2m",gammas2.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL2;E[GeV];Nsh");

  for(uint i=0;i<gammas1.size();i++) h.Fill1D("ECAL1e",e.vCaloClus(gammas1[i]).E(), 1, 100,0.0,100., "Energy of shower in ECAL1;E(GeV)");
  for(uint i=0;i<gammas2.size();i++) h.Fill1D("ECAL2e",e.vCaloClus(gammas2[i]).E(), 1, 100,0.0,100., "Energy of shower in ECAL2;E(GeV)");

  h.Fill1D("ECAL1p2m",gammas1.size()+gammas2.size(), 1, 
	   20,-0.5, 19.5, "Amount of shower ECAL1+ECAL2;E[GeV];Nsh");
  if(gammas1.size()+gammas2.size()<NGAMMA) return;

  std::vector<int> position_of_good;
  for(uint i=0;i<gammas1.size();i++) 
    if(e.vCaloClus(gammas1[i]).E()>ECAL1_TH) position_of_good.push_back(gammas1[i]);
  for(uint i=0;i<gammas2.size();i++) 
    if(e.vCaloClus(gammas2[i]).E()>ECAL2_TH) position_of_good.push_back(gammas2[i]);

  h.Fill1D("Ngamma_more_th",position_of_good.size(),1, 20,-0.5,19.5);
  if(position_of_good.size() < 2) return;

  //sort gammas
  for(uint i=0;i<position_of_good.size()-1;i++) 
    for(uint j=i+1;j<position_of_good.size();j++) 
      if(e.vCaloClus(position_of_good[i]).E() < e.vCaloClus(position_of_good[j]).E()) {
	int ci = position_of_good[i];
	position_of_good[i] = position_of_good[j];
	position_of_good[j] = ci;
      }

  TVector3 pi_v = e.vParticle( op[0] ).ParInVtx(pv[0]).Mom3();
  pi[0] = pi_v.X(); pi[1] = pi_v.Y(); pi[2] = pi_v.Z(); pi[4] = PI_MASS;
  //g1
  const double v[] = {e.vVertex(pv[0]).X(),
		      e.vVertex(pv[0]).Y(),
		      e.vVertex(pv[0]).Z()};
  const PaCaloClus & cl1 = e.vCaloClus(position_of_good[0]); GetPgamma(cl1.X(),cl1.Y(),cl1.Z(),cl1.E(), v, g1);
  const PaCaloClus & cl2 = e.vCaloClus(position_of_good[1]); GetPgamma(cl2.X(),cl2.Y(),cl2.Z(),cl2.E(), v, g2);
  otree->Fill();

  //if(position_of_good.size() != NGAMMA) return;
    //h.Fill1D("mgamma")

//  /*---------------------------------------Invariant mass--------------------------------------*/   
//
//  double v[] = {e.vVertex(pv[0]).X(),
//		e.vVertex(pv[0]).Y(),
//		e.vVertex(pv[0]).Z()};
//
//  TLorentzVector g1_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[0]).X(),
//					  e.vCaloClus(position_of_good[0]).Y(),
//					  e.vCaloClus(position_of_good[0]).Z(),
//					  e.vCaloClus(position_of_good[0]).E(),v);
//  TLorentzVector g2_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[1]).X(),
//					  e.vCaloClus(position_of_good[1]).Y(),
//					  e.vCaloClus(position_of_good[1]).Z(),
//					  e.vCaloClus(position_of_good[1]).E(),v);
//  
//  TLorentzVector gg_lv=g1_lv+g2_lv;
//  h.Fill1D("gg_mass",gg_lv.M(),1,
//	   200,0,0.7);
//  if( fabs(gg_lv.M()-PI0_MASS) > PI0_CUT ) return;
//
//  if(e.vParticle(op[1]).Q()==+1) swap(op[1],op[0]);
//  if(e.vParticle(op[2]).Q()==+1) swap(op[2],op[0]);
//  
//  const TLorentzVector p1_lv = e.vParticle(op[0]).ParInVtx(pv[0]).LzVec(PI_MASS);
//  const TLorentzVector p2_lv = e.vParticle(op[1]).ParInVtx(pv[0]).LzVec(PI_MASS);
//  const TLorentzVector p3_lv = e.vParticle(op[2]).ParInVtx(pv[0]).LzVec(PI_MASS);
//  TLorentzVector three_pi_lv1 = p1_lv+p2_lv+gg_lv;
//  TLorentzVector three_pi_lv2 = p1_lv+p3_lv+gg_lv;
//  h.Fill1D("pipipi_mass",three_pi_lv1.M(),1, 200,0,1);
//  h.Fill1D("pipipi_mass",three_pi_lv2.M()           );
//  if( fabs(three_pi_lv1.M()-ETA_MASS)>ETA_CUT && 
//      fabs(three_pi_lv2.M()-ETA_MASS)>ETA_CUT ) return;
//
//  TLorentzVector sum_lv = p1_lv+p2_lv+p3_lv+gg_lv;
//  h.Fill1D("m4pi",three_pi_lv1.M(),1, 200,0,3);
//  
//  //TLorentzVector gg_lv=g1_lv+g2_lv;
//
//  /*-----------------------------------------End of cuts---------------------------------------*/  

  //e.TagToSave();
}



/*
  //variables for the tree
  //ggmass
  sqrt(2*sqrt(g1x*g1x+g1y*g1y+g1z*g1z)*sqrt(g2x*g2x+g2y*g2y+g2z*g2z)-2*(g1x*g2x+g1y*g2y+g1z*g2z))

  //exclusivity
  g1z+g2z+pi
  sqrt(g1x*g1x+g1y*g1y+g1z*g1z)+sqrt(g2x*g2x+g2y*g2y+g2z*g2z)+sqrt(pix*pix+piy*piy+piz*piz+0.14*0.14)
  sqrt(pow(pix+g1x+g2x,2)+pow(piy+g1y+g2y,2)+pow(piz+g1z+g2z,2))
  
  //sin(angle:pi-beam)
  sqrt(1-pow((pix*(pix+g1x+g2x)+piy*(piy+g1y+g2y)+piz*(piz+g1z+g2z))/(sqrt(pix*pix+piy*piy+piz*piz)*sqrt(pow(pix+g1x+g2x,2)+pow(piy+g1y+g2y,2)+pow(piz+g1z+g2z,2))),2))
  sqrt(pix*pix+piy*piy)/piz

  //p_perp
  sqrt(pow(pix+g1x+g2x,2)+pow(piy+g1y+g2y,2))/(piz+g1z+g2z)


  //cut
  (abs(sqrt(2*sqrt(g1x*g1x+g1y*g1y+g1z*g1z)*sqrt(g2x*g2x+g2y*g2y+g2z*g2z)-2*(g1x*g2x+g1y*g2y+g1z*g2z))-0.525)<0.05)
  sqrt(pix*pix+piy*piy+piz*piz)<165
  sqrt(pix*pix+piy*piy)/piz>0.5e-3


  //nice pictures
  *angle-beam correlation
  outtree->Draw("piz:sqrt(pix*pix+piy*piy)/piz>>h1(100,0,0.01,100,0,200)","(piz<200)*(sqrt(pix*pix+piy*piy)/piz>0)","colz");
  *exclusivity-ggmass correlation
  outtree->Draw("sqrt(pow(pix+g1x+g2x,2)+pow(piy+g1y+g2y,2)+pow(piz+g1z+g2z,2)):sqrt(2*sqrt(g1x*g1x+g1y*g1y+g1z*g1z)*sqrt(g2x*g2x+g2y*g2y+g2z*g2z)-2*(g1x*g2x+g1y*g2y+g1z*g2z))>>h1(100,0,1.0,100,0,200)","(piz<170)*(sqrt(pix*pix+piy*piy)/piz>0.5e-3)","colz");
  //pi0 region
  outtree->Draw("sqrt(pow(pix+g1x+g2x,2)+pow(piy+g1y+g2y,2)+pow(piz+g1z+g2z,2)):sqrt(2*sqrt(g1x*g1x+g1y*g1y+g1z*g1z)*sqrt(g2x*g2x+g2y*g2y+g2z*g2z)-2*(g1x*g2x+g1y*g2y+g1z*g2z))>>h1(40,0.125,0.145,40,150,200)","(piz<165)*(sqrt(pix*pix+piy*piy)/piz>0.5e-3)","colz");
  //eta region
  outtree->Draw("sqrt(pow(pix+g1x+g2x,2)+pow(piy+g1y+g2y,2)+pow(piz+g1z+g2z,2)):sqrt(2*sqrt(g1x*g1x+g1y*g1y+g1z*g1z)*sqrt(g2x*g2x+g2y*g2y+g2z*g2z)-2*(g1x*g2x+g1y*g2y+g1z*g2z))>>h1(40,0.48,0.6,40,150,200)","(piz<165)*(sqrt(pix*pix+piy*piy)/piz>0.5e-3)","colz");

  //exclusivity with eta cut  
  outtree->Draw("sqrt(pow(pix+g1x+g2x,2)+pow(piy+g1y+g2y,2)+pow(piz+g1z+g2z,2))>>h1(100,150,220)","(piz<165)*(sqrt(pix*pix+piy*piy)/piz>0.5e-3)*(abs(sqrt(2*sqrt(g1x*g1x+g1y*g1y+g1z*g1z)*sqrt(g2x*g2x+g2y*g2y+g2z*g2z)-2*(g1x*g2x+g1y*g2y+g1z*g2z))-0.538)<0.01)","");

  //pperp vs exlusivity
  outtree->Draw("sqrt(pow(pix+g1x+g2x,2)+pow(piy+g1y+g2y,2)+pow(piz+g1z+g2z,2)):sqrt(pow(pix+g1x+g2x,2)+pow(piy+g1y+g2y,2))/(piz+g1z+g2z)>>h2(100,0,0.005,50,150,200)","(piz<165)*(sqrt(pix*pix+piy*piy)/piz>0.5e-3)*(abs(sqrt(2*sqrt(g1x*g1x+g1y*g1y+g1z*g1z)*sqrt(g2x*g2x+g2y*g2y+g2z*g2z)-2*(g1x*g2x+g1y*g2y+g1z*g2z))-0.538)<0.01)","colz");
  //pperp vs gg_mass; One needs to normalise
  outtree->Draw("sqrt(2*sqrt(g1x*g1x+g1y*g1y+g1z*g1z)*sqrt(g2x*g2x+g2y*g2y+g2z*g2z)-2*(g1x*g2x+g1y*g2y+g1z*g2z)):sqrt(pow(pix+g1x+g2x,2)+pow(piy+g1y+g2y,2))/(piz+g1z+g2z)>>h2(100,0,0.005,40,0.48,0.6)","(piz<165)*(sqrt(pix*pix+piy*piy)/piz>0.5e-3)*(sqrt(pow(pix+g1x+g2x,2)+pow(piy+g1y+g2y,2)+pow(piz+g1z+g2z,2))>185)","colz");


*/
