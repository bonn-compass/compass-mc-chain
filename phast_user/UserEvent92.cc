#include <iostream>
#include <cmath>
#include <vector>
#include <bitset>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"
#include "PaParticle.h"
#include "PaCaloClus.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TLorentzVector.h"

#include "mass.h"
#include "heplib.h"
#include "MHister.h"

/*
  Studies of vertexes and tracks
*/


//bool myfunction (int i,int j) { return (i<j); }

//parameters of cuts, which are applied:
#define N_PV 1
#define NOUT_PART 2
#define NGAMMA 2
#define QSUM 0
#define TRACK_MOMENTUM_CUT -180
#define ECAL1_TH 1
#define ECAL2_TH 4

#define VZ -72
#define VZ_CUT 10 

using namespace std;
TLorentzVector prepare_gamma_LV(double x,double y,double z,double E, double v[]);

void UserEvent92(PaEvent& e) {

  static string sh_file_name = string( Phast::Ref().h_file->GetEndpointUrl()->GetFile() );
  static MHister h( ( sh_file_name+".mine" ).c_str() );
  
  static bool first(true);
  if(first){ 
    first=false;
  } 

  /*-------------------------------------------------------------------------------------------*/
  /*----------------------------------------Analysis-------------------------------------------*/
  int trig = e.TrigMask();
  //if(!trig) return;
  std::bitset<12> mask_from_event(trig);
  for(int i=0;i<12;i++) 
    if(mask_from_event[i]) 
      h.Fill1D("trigger",i,1,
	       12,-0.5,11.5,"Trigger;bit");

  /*------------------------------------primary verteces---------------------------------------*/
  vector<int> pv, sv;
  h.Fill1D( "Nv", e.NVertex(), 1, 20, -0.5, 19.5, "Amount of vertexes; N_{vertexes}" );
  for(int iv = 0; iv < e.NVertex(); iv++) {
    const PaVertex& v = e.vVertex(iv);
    h.Fill1D( "vz_all", v.Z(), 1, 1500, -500, 1000, "Vertex Z; Vz (cm)" );
    if(v.IsPrimary()) pv.push_back(iv); 
    else sv.push_back(iv); 
  }
  h.Fill1D( "Npv", pv.size(), 1, 20, -0.5, 19.5, "Amount of PV;Npv" );
  h.Fill1D( "Nsv", e.NVertex()-pv.size(), 1, 20, -0.5, 19.5, "Amount of SV;Nsv" );

  h.Fill2D( "Nv_vs_Npv", e.NVertex(), pv.size(), 1, 
	    20, -0.5, 19.5,
	    20, -0.5, 19.5, "Amount of V vs Amount of PV;Nv;Npv" );
  
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& v = e.vVertex(pv[i]);
    h.Fill1D( "vz_pv", v.Z(), 1, 1500, -500, 1000, "Vertex Z; Vz (cm)" );
    int InPN = v.InParticle();
    if(InPN<0 || !(e.vParticle(InPN).IsBeam())) 
      std::cout << "Error: InPN<0 || !(e.vParticle(InPN).IsBeam())" << std::endl;
  }
  for(uint i=0;i<sv.size();i++) {
    const PaVertex& v = e.vVertex(sv[i]);
    h.Fill1D ("nOUTp_from_SV", v.NOutParticles(), 1, 10, -0.5, 9.5, 
	      "Amount of OUT-particles from SV;N_{out}" );
    int scharge=0;
    for(int j=0;j<v.NOutParticles();j++) {
      double pMom = e.vParticle( v.iOutParticle(j) ).ParInVtx(sv[i]).qP();
      h.Fill1D("qpOUTp_xfrom_SV",pMom,1,
	       250, -250, 250, "Momentum of track;aP (GeV)" );
      scharge += (pMom>0) ? +1 : -1;
    }
    h.Fill1D ("chargeInSV", scharge, 1, 11, -5.5, 5.5, 
	      "Sum of charges in SV;sum of charge" );    
    h.Fill1D("vz_sv",v.Z(),1, 1500, -500, 1000, "Vertex Z secondary vertex; Vz (cm)");

  }

  /*--------------------------------------Outgoing particles-----------------------------------*/
  vector<int> op;
  for(uint i=0;i<pv.size();i++) {
    const PaVertex& vi = e.vVertex(pv[i]);
    for(int j=0;j<vi.NOutParticles();j++) {
      double pMom = e.vParticle( vi.iOutParticle(j) ).ParInVtx(pv[i]).qP();
      h.Fill1D("qpOUTp",pMom,1,
	       250, -250, 250, "Momentum of track;aP (GeV)" );
      op.push_back( vi.iOutParticle(j) );
    }
    if(vi.NOutParticles()==1) {      
      double pMom = e.vParticle( vi.iOutParticle(0) ).ParInVtx(pv[i]).qP();
      h.Fill1D("qPifOne",pMom,1,
	       250, -250, 250, "Momentum of track fron PV is one;qP (GeV)");
      if(pMom<180)
	h.Fill1D("zVifOneLess180", vi.Z(), 1, 1500, -500, 1000, "Vertex Z if one OUT; Vz (cm)");
    }
  
  }
  h.Fill1D ("nOUTp", op.size(), 1, 10, -0.5, 9.5, "Amount of OUT-particles from all PVs;N_{out}" );
  h.Fill2D ("nOUTp_vs_nPV", op.size(), pv.size(), 1, 
	    10, -0.5, 9.5,
	    10, -0.5, 9.5,
	    "Amount of OUT-particles VS Amount of PVs;N_{out};N_{vertexes}");
  h.Fill2D ("NPart_vs_NPVplusNout", e.NParticle(), pv.size()+op.size(), 1, 
	    10, -0.5, 9.5,
	    10, -0.5, 9.5,
	    "Amount particles in e.NPart.. VS Amount of OUT+PV;N_{out};N_{vertexes}");

  int sum_of_charges=0;
  for(uint i=0;i<op.size();i++) sum_of_charges += e.vParticle(op[i]).Q();
  h.Fill1D ("qSum", sum_of_charges, 1, 7, -3.5, 3.5, "sum of charges;sum Q" );
  
//  /*-----------------------------------------Calorimetry---------------------------------------*/
//  std::vector<int> gammas1,gammas2;
//  
//  const int Nparticle = e.NParticle();
//  for(int i=0;i<Nparticle;i++) {
//    const PaParticle& part = e.vParticle(i);
//    if(part.Q()==0) {
//      for(int j=0;j<part.NCalorim();j++) {
//	int iClast = part.iCalorim(j);
//	const PaCaloClus &cl = e.vCaloClus(iClast);
//	if( cl.CalorimName().find("EC01P1") != std::string::npos ) gammas1.push_back(iClast);
//	if( cl.CalorimName().find("EC02P1") != std::string::npos ) gammas2.push_back(iClast);
//      }
//    }
//  }
//  
//  h.Fill1D("ECAL1m",gammas1.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL1;E[GeV];Nsh");
//  h.Fill1D("ECAL2m",gammas2.size(), 1, 20,-0.5, 19.5, "Amount of shower in ECAL2;E[GeV];Nsh");
//
//  h.Fill1D("ECAL1p2m",gammas1.size()+gammas2.size(), 1, 
//	   20,-0.5, 19.5, "Amount of shower ECAL1+ECAL2;E[GeV];Nsh");
//  if(gammas1.size()+gammas2.size()<NGAMMA) return;
//
//  std::vector<int> position_of_good;
//  for(uint i=0;i<gammas1.size();i++) 
//    if(e.vCaloClus(gammas1[i]).E()>ECAL1_TH) position_of_good.push_back(gammas1[i]);
//  for(uint i=0;i<gammas2.size();i++) 
//    if(e.vCaloClus(gammas2[i]).E()>ECAL2_TH) position_of_good.push_back(gammas2[i]);
//
//  h.Fill1D("Ngamma_more_th",position_of_good.size(),1,
//	   20,-0.5,19.5);
//  //if(position_of_good.size() != NGAMMA) return;
//    //h.Fill1D("mgamma")
//
//  /*---------------------------------------Invariant mass--------------------------------------*/   
//
//  double v[] = {e.vVertex(pv[0]).X(),
//		e.vVertex(pv[0]).Y(),
//		e.vVertex(pv[0]).Z()};
//
//  TLorentzVector g1_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[0]).X(),
//					  e.vCaloClus(position_of_good[0]).Y(),
//					  e.vCaloClus(position_of_good[0]).Z(),
//					  e.vCaloClus(position_of_good[0]).E(),v);
//  TLorentzVector g2_lv = prepare_gamma_LV(e.vCaloClus(position_of_good[1]).X(),
//					  e.vCaloClus(position_of_good[1]).Y(),
//					  e.vCaloClus(position_of_good[1]).Z(),
//					  e.vCaloClus(position_of_good[1]).E(),v);
//  
//  TLorentzVector gg_lv=g1_lv+g2_lv;
//  h.Fill1D("gg_mass",gg_lv.M(),1,
//	   200,0,0.7);
//  if( fabs(gg_lv.M()-PI0_MASS) > PI0_CUT ) return;
//
//  if(e.vParticle(op[1]).Q()==+1) swap(op[1],op[0]);
//  if(e.vParticle(op[2]).Q()==+1) swap(op[2],op[0]);
//  
//  const TLorentzVector p1_lv = e.vParticle(op[0]).ParInVtx(pv[0]).LzVec(PI_MASS);
//  const TLorentzVector p2_lv = e.vParticle(op[1]).ParInVtx(pv[0]).LzVec(PI_MASS);
//  const TLorentzVector p3_lv = e.vParticle(op[2]).ParInVtx(pv[0]).LzVec(PI_MASS);
//  TLorentzVector three_pi_lv1 = p1_lv+p2_lv+gg_lv;
//  TLorentzVector three_pi_lv2 = p1_lv+p3_lv+gg_lv;
//  h.Fill1D("pipipi_mass",three_pi_lv1.M(),1, 200,0,1);
//  h.Fill1D("pipipi_mass",three_pi_lv2.M()           );
//  if( fabs(three_pi_lv1.M()-ETA_MASS)>ETA_CUT && 
//      fabs(three_pi_lv2.M()-ETA_MASS)>ETA_CUT ) return;
//
//  TLorentzVector sum_lv = p1_lv+p2_lv+p3_lv+gg_lv;
//  h.Fill1D("m4pi",three_pi_lv1.M(),1, 200,0,3);
//  
//  //TLorentzVector gg_lv=g1_lv+g2_lv;
//
//  /*-----------------------------------------End of cuts---------------------------------------*/  

  e.TagToSave();
}
