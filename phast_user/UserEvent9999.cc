//Quality check for compass data
//date: 24/08/2015
//authors: Misha Mkhasenko, Erik Altenbach

#include "TTree.h"
#include "Phast.h"
#include "PaEvent.h"
#include "PaVertex.h"

void UserEvent9999(PaEvent& e) {

	static bool first(true);
	static TTree *quality(0);
	//spill number and number event at spill
	static int spill(0);
	static int run(0);
	static int NevInSpill(0);
	//--------> quality values
	/*----------------------------- GENERAL -----------------------------*/
	//Intensity
	static double Intensity(0);
	static double aIntensity(0);
	//triggers
	static double Ntriggers(0);
	static double aNtriggers(0);
	/*----------------------------- TRACKING ----------------------------*/
	//Number of 'particles'
	static double Npart(0);
	static double aNpart(0);
	//Number of 'particles' from PV
	static double NpartPV(0);
	static double aNpartPV(0);
	//Number of tracks
	static double Ntr(0);
	static double aNtr(0);
	//Number of Vertex
	static double Nv(0);
	static double aNv(0);
	//Number of Primary Vertex
	static double Npv(0);
	static double aNpv(0);
	//Number of Tracks per PV
	static double NtrPV(0);
	static double aNtrPV(0);
	//Charge
	static double Charge(0);
	static double aCharge(0);
	//Charge
	static double ChargePV(0);
	static double aChargePV(0);
	/*---------------------------- CALORIMERY ---------------------------*/
	//Number of Clusters ECAL1
	static double Ncl1(0);
	static double aNcl1(0);
	//Number of Clusters ECAL2
	static double Ncl2(0);
	static double aNcl2(0);
	//Total energy ECAL1
	static double Eg1(0);
	static double aEg1(0);
	//Total energy ECAL2
	static double Eg2(0);
	static double aEg2(0);
	//Total energy not assosiated with tracks ECAL1
	static double Egz1(0);
	static double aEgz1(0);
	//Total energy not assosiated with tracks ECAL2
	static double Egz2(0);
	static double aEgz2(0);

	// Number of Gammas in ECAL1
	static int Ng1(0);
	static int aNg1(0);

	// Number of Gammas in ECAL2
	static int Ng2(0);
	static int aNg2(0);

	if (first) {
		first = false;
		//init
		spill = e.SpillNum();
		run = e.RunNum();

		Phast::Ref().HistFileDir("QuallityCheck");

		quality = new TTree("TQ", "quality");
		quality->Branch("run", &run);
		quality->Branch("spill", &spill);
		quality->Branch("Nev", &NevInSpill);
		//-----------> quality values
		//general
		quality->Branch("aIntensity", &aIntensity);
		quality->Branch("aNtriggers", &aNtriggers);
		//tracking
		quality->Branch("aNpart", &aNpart);
		quality->Branch("aNpartPV", &aNpartPV);
		quality->Branch("aNv", &aNv);
		quality->Branch("aNpv", &aNpv);
		quality->Branch("aNtr", &aNtr);
		quality->Branch("aNtrPV", &aNtrPV);
		quality->Branch("aCharge", &aCharge);
		quality->Branch("aChargePV", &aChargePV);
		//calorimetry
		quality->Branch("aNcl1", &aNcl1);
		quality->Branch("aNcl2", &aNcl2);
		quality->Branch("aEg1", &aEg1);
		quality->Branch("aEg2", &aEg2);
		quality->Branch("aEgz1", &aEgz1);
		quality->Branch("aEgz2", &aEgz2);
		quality->Branch("aNg1", &aNg1);
		quality->Branch("aNg2", &aNg2);

	}
	if (spill != e.SpillNum() || run != e.RunNum()) {
		//quality values
		aIntensity = 1. * Intensity / NevInSpill;
		aNtriggers = 1. * Ntriggers / NevInSpill;
		//tracking
		aNpart = 1. * Npart / NevInSpill;
		aNpartPV = 1. * NpartPV / NevInSpill;
		aNtr = 1. * Ntr / NevInSpill;
		aNv = 1. * Nv / NevInSpill;
		aNpv = 1. * Npv / NevInSpill;
		aNtrPV = 1. * NtrPV / NevInSpill;
		aCharge = 1. * Charge / NevInSpill;
		aChargePV = 1. * ChargePV / NevInSpill;
		//calorimetry
		aNcl1 = 1. * Ncl1 / NevInSpill;
		aNcl2 = 1. * Ncl2 / NevInSpill;
		aEg1 = 1. * Eg1 / NevInSpill;
		aEg2 = 1. * Eg2 / NevInSpill;
		aEgz1 = 1. * Egz1 / NevInSpill;
		aEgz2 = 1. * Egz2 / NevInSpill;
		aNg1 = 1. * Ng1 / NevInSpill;
		aNg2 = 1. * Ng2 / NevInSpill;
		//fill
		quality->Fill();
		//---------reset----------//
		NevInSpill = 0;
		spill = e.SpillNum();
		run = e.RunNum();
		//---------> quality values
		//general
		Intensity = 0;
		Ntriggers = 0;
		//tracking
		Npart = 0;
		NpartPV = 0;
		Ntr = 0;
		Nv = 0;
		Npv = 0;
		NtrPV = 0;
		Charge = 0;
		ChargePV = 0;
		//calorimetry
		Ncl1 = 0;
		Ncl2 = 0;
		Eg1 = 0;
		Eg2 = 0;
		Egz1 = 0;
		Egz2 = 0;
		Ng1 = 0;
		Ng2 = 0;
	}
	NevInSpill++;

	////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////// C A L C U L A T I O N  O F  A L L ///////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////

	/********************* general *********************/
	Intensity += e.EvInSpill();
	for (int i = 0; i < 16; i++)
		if (((1 << i) & e.TrigMask()) != 0)
			Ntriggers++;
	/******************** tracking *********************/
	Npart += e.NParticle();
	for (int i = 0; i < e.NParticle(); i++) {
		const PaParticle &pa = e.vParticle(i);
		if (pa.IsBeam())
			continue;
		for (int j = 0; j < pa.NVertex(); j++)
			if (e.vVertex(pa.iVertex(j)).IsPrimary())
				NpartPV++;
	}
	Ntr += e.NTrack();
	Nv += e.NVertex();
	for (int i = 0; i < e.NVertex(); i++)
		if (e.vVertex(i).IsPrimary())
			Npv++;
	for (int i = 0; i < e.NVertex(); i++)
		if (e.vVertex(i).IsPrimary())
			NtrPV += e.vVertex(i).NOutParticles();
	for (int i = 0; i < e.NTrack(); i++)
		if (e.vTrack(i).HasMom())
			Charge += e.vTrack(i).Q();
	for (int i = 0; i < e.NVertex(); i++)
		if (e.vVertex(i).IsPrimary()) {
			const PaVertex &vi = e.vVertex(i);
			for (int j = 0; j < vi.NOutParticles(); j++) {
				int itr = e.vParticle(vi.iOutParticle(j)).iTrack();
				if (itr < 0) {
					std::cerr << "Warning: I don't understand it." << std::endl;
					continue;
				}
				const PaTrack &tr = e.vTrack(itr);
				if (tr.HasMom())
					ChargePV += tr.Q();
			}
		}
	/******************** calorimetry ******************/
	for (int i = 0; i < e.NCaloClus(); i++)
	  if (e.vCaloClus(i).iCalorim() == 0)
		  Ncl1++;
	  else if (e.vCaloClus(i).iCalorim() == 1)
	    Ncl2++;
	
	///////////////////////////////////////////////// ECAL CHECK NO CHARGED TRACK ///////////////////////////////////////////////////////////
	int NassosClusters;
	const int Nparticle = e.NParticle();
	for (int i = 0; i < Nparticle; i++) {
	  const PaParticle& part = e.vParticle(i);
	  if (part.Q() == 0) {
	    NassosClusters = 0;
	    for (int j = 0; j < part.NCalorim(); j++) {
	      int iClao = part.iCalorim(j);
	      
	      if (e.vCaloClus(j).iCalorim() == 0) {
		NassosClusters++;
		Egz1 += e.vCaloClus(iClao).E();
		Ng1++;
		
	      }
	      if (e.vCaloClus(j).iCalorim() == 1) {
		NassosClusters++;
		Egz2 += e.vCaloClus(iClao).E();
		Ng2++;
	      }
	    }
	    if (NassosClusters > 1)
	      std::cerr << "Strange: 0-Charge Particle with 2 different Clusters!" << NassosClusters
			<< std::endl;
	  }
	}

	///////////////////////////////////////////////// ECAL CHECK DONT CARE FOR CHARGE ///////////////////////////////////////////////////////////

	map<int, int> IsInEcal1;
	map<int, int> IsInEcal2;

	for (int ic = 0; ic < int(e.vCaloClus().size()); ic++) {
		PaCaloClus& cl = e.vCaloClus()[ic];
		if (e.vCaloClus(ic).iCalorim() == 0) {

			const std::vector<Int_t> &Cell = cl.vCellNumber();
			const std::vector<Float_t> &Ce = cl.vCellEnergy();

			for (int j = 0; j < (int) Cell.size(); j++) {
				if (IsInEcal1[Cell[j]] == 0) {
					Eg1 += (double) Ce[j];
					IsInEcal1[Cell[j]] = 1;
				}

			}
		} else if (e.vCaloClus(ic).iCalorim() == 1) {

			const std::vector<Int_t> &Cell = cl.vCellNumber();
			const std::vector<Float_t> &Ce = cl.vCellEnergy();

			for (int j = 0; j < (int) Cell.size(); j++) {
				if (IsInEcal2[Cell[j]] == 0) {
					Eg2 += (double) Ce[j];
					IsInEcal2[Cell[j]] = 1;
				}

			}
		}

	}

}

