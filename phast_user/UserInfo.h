#ifndef UserInfo_h
#define UserInfo_h

#include <iostream>
#include "TObject.h"

using namespace std;

// It is an example of ROOT-persistent user-defined class.
// Objects of this class could be stored in output mDST together with "standard" 
// PaEvent objects.
// User can put in this class any extra information he wants to add to PaEvent.
//  
// see
//   UserEvent5.cc - example of writing
//   UserEvent6.cc - example of reading

class UserInfo: public TObject
{
  
 public:
  
  static int NobjLeft; static int NobjCreated; // object counters
  
  //! Print
  void Print(int level = 0) const;
  
  // Inlined constructor. destructor, copy constructor etc.
  UserInfo():
    run(-1), ev(-1),
    x(0),y(0),
    sigmx(-1),
    sigmy(-1)
    {
      NobjCreated++; NobjLeft++;
      // cout<<"      UserInfo constructor"<<endl;
    }
  
  virtual ~UserInfo() 
    {
      NobjLeft--;
      // cout<<"   UserInfo destructor "<<endl;
    }
  
  UserInfo(const UserInfo& c): TObject(c) 
    {
      if(this != &c){
	(*this)=c;
	NobjCreated++; NobjLeft++;
	// cout<<"      UserInfo copy constructor"<<endl;
      }
    }
  
  //! "=" operator
  UserInfo& operator = (const UserInfo& c)
    {
      TObject::operator = (c);
      ev     = c.ev;
      run    = c.run;
      x      = c.x;
      y      = c.y;
      sigmx  = c.sigmx;
      sigmy  = c.sigmy;
      // cout<<"      UserInfo = operator "<<endl;
      return(*this);
    };

 
  const int& Run()      { return run; }
  const int& Ev()       { return ev; }
  const float& X()      { return x; }
  const float& Y()      { return y; }
  const float& SigmaX() { return sigmx; }
  const float& SigmaY() { return sigmy; }

  void SetRun(const int& i)     { run = i ;}
  void SetEv (const int& i)     { ev  = i ;}
  void SetX(const float& f)     { x = f ;}
  void SetY(const float& f)     { y = f ;}
  void SetSigmX(const float& f) { sigmx = f ;}
  void SetSigmY(const float& f) { sigmy = f ;}

 private:
  
  int  run;
  int  ev;
  float x;                   // X coordinate of XXX
  float y;                   // Y coordinate of XXX
  float sigmx;               // error X
  float sigmy;               // error Y
  
  ClassDef(UserInfo,1)
    
};

#endif















