#include<iostream>
#include "TROOT.h"
#include "TH1.h"
#include "Phast.h"


//
// It's a place for some "end-of-job
// actions (like histogram fits, histogram
// normalizations etc)
//
// For every UserEventN, complementary 
// UserJobEndN is called (if exists)
//

void UserJobEnd0()
{
  const Phast& ph = Phast::Ref();

  if(ph.print) cout<<"[ UserJobEnd0 has been called ]"<<endl;


  // here is an example of histogram normalization (per processed event)
  ph.h_file->cd();
  TDirectory* dir = (TDirectory*)gROOT->FindObject("UserEvent0");
  if(dir != NULL) dir->cd(); 
  TH1D* h11 = (TH1D*)gROOT->FindObject("h11");
  if(h11 != NULL) {
    h11->Scale(1./ph.NevProc);
  }

  float Flux = Phast::Ref().IntegratedBeamFlux();
  cout<< "UserJobEnd0 == Integrated Beam Flux()=" << Flux << endl;

}

