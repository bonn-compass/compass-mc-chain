#include<iostream>
#include "Phast.h"

//
// It's a place for some "end-of-run"
// actions (like run-dependent histogram filling)
//
// For every UserEventN, complementary 
// UserRunEndN and UserJobEndN is called 
// (if they are exist)
//

void UserRunEnd0(int irun)
{
  const Phast& ph = Phast::Ref();
  if(ph.print) cout<<"[ UserRunEnd0 has been called for run # "<<irun<<" ]"<<endl;
}

