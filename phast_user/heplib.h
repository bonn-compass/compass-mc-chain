/* heplib.h */
/* Created by Anton Ivashin 2008
*/
/* Modification 2012 by Mikh.Mikhail
   function hltrn3 (const double* in,
                          double* out, 
		    const double *ref);
*/

#ifndef  __HEPLIB_H__
#define  __HEPLIB_H__

#include <math.h>

#define  hlmaxvl(a,b) ( ((a) > (b))? (a) : (b) )

#define  hlminvl(a,b) ( ((a) > (b))? (b) : (a) )


#define  hlsclp3(x,y)  ((x)[0]*(y)[0]+(x)[1]*(y)[1]+(x)[2]*(y)[2])
#define  hlsclp5(x,y)  ((x)[3]*(y)[3] - hlsclp3(x,y))
#define  hlangl3(x,y)  (hlsclp3(x,y)/sqrt(hlsclp3(y,y)*hlsclp3(x,x)))
#define  hlsignm(x)    (((x)<0)? -1:1)

#ifdef __cplusplus
extern "C" {
#endif

  /* Vector functions (hepvec.c)*/

  void   hltospa(double* xy,double* sp);
  void   hltospc(double* xy,double* sp);
  void   hlatoxy(double* sp,double* xy);
  void   hlctoxy(double* sp,double* xy);
  
  void   hlvecp3(const double* x,const double* y,double* out);
  void   hlnorm3(const double* in,double* out);
  void   hladdt3(const double* x,const double* y,double* out);
  void   hlsubt3(const double* x,const double* y,double* out);
  
  void   hladdt5(const double* x,const double* y,double* out);
  void   hlsubt5(const double* x,const double* y,double* out);
  void   hlmass5(const double* in,double* out,double mass);
  
  void   hlrotx3(const double* in,double* out,double f);
  void   hlroty3(const double* in,double* out,double f);
  void   hlrotz3(const double* in,double* out,double f);
  
  void   hlbstx5(const double* in,double* out,double g);
  void   hlbsty5(const double* in,double* out,double g);
  void   hlbstz5(const double* in,double* out,double g);

  double hlmod2p(double f);

  void   hltrn3(const double* in,double* out, const double *ref);

#ifdef __cplusplus
}
#endif

#endif

