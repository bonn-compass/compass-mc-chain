/* hepvec.c */
/* Created by Anton Ivashin 2008
*/
/* Modification 2012 by Mikh.Mikhail
   function hltrn3 (const double* in,
                          double* out, 
		    const double *ref);
*/

#include"heplib.h"
#include<string.h>
/*================================*/

void   hltospa(double* xy,double* sp)
{
  double  lsp[3];
  double  nrm2;
  
  lsp[0] = sqrt(hlsclp3(xy,xy));
  
  if(lsp[0] == 0.)
  {
    bzero(sp,3*sizeof(double));
    return;
  }     

  lsp[1] = acos(xy[2]/lsp[0]);
  
  nrm2 = sqrt(xy[0]*xy[0]+xy[1]*xy[1]);
  
  if(nrm2 == 0.) nrm2 = 1.; 
    
  lsp[2] = acos(xy[0]/nrm2);
  
  if(xy[1] < 0.) lsp[2] = 2.*M_PI - lsp[2];

  memcpy(sp,lsp,3*sizeof(double));   
}
/*================================*/

void   hltospc(double* xy,double* sp)
{
  double  lsp[3];
  double  nrm2;
  
  lsp[0] = sqrt(hlsclp3(xy,xy));
  
  if(lsp[0] == 0.)
  {
    bzero(sp,3*sizeof(double));
    return;
  }     

  lsp[1] = xy[2]/lsp[0];

  nrm2 = sqrt(xy[0]*xy[0]+xy[1]*xy[1]);
  
  if(nrm2 == 0.) nrm2 = 1.; 

  lsp[2] = acos(xy[0]/nrm2);
  
  if(xy[1] < 0.) lsp[2] = 2.*M_PI - lsp[2];

  memcpy(sp,lsp,3*sizeof(double));   


}
/*================================*/

void   hlatoxy(double* sp,double* xy)
{
  double rsnt,rcst;
  
  rsnt = sp[0]*sin(sp[1]);
  rcst = sp[0]*cos(sp[1]);
  
  xy[0] = rsnt*cos(sp[2]);
  xy[1] = rsnt*sin(sp[2]);
  
  xy[2] = rcst;  

}
/*================================*/

void   hlctoxy(double* sp,double* xy)
{
  double rsnt,rcst;
  
  rsnt = sp[0]*sqrt(1.-sp[1]*sp[1]);
  rcst = sp[0]*sp[1];
  
  xy[0] = rsnt*cos(sp[2]);
  xy[1] = rsnt*sin(sp[2]);
  
  xy[2] = rcst;  

}
/*================================*/

void   hlvecp3(const double* x,const double* y,double* out)
{
  double  lout[3];
  
  lout[0] = x[1]*y[2] - x[2]*y[1];
  lout[1] = x[2]*y[0] - x[0]*y[2];
  lout[2] = x[0]*y[1] - x[1]*y[0];
  
  memcpy(out,lout,3*sizeof(double));
  
}
/*================================*/

void   hlnorm3(const double* in,double* out)
{
  double  nrm;
  
  nrm = sqrt(hlsclp3(in,in));
  
  out[0] = in[0]/nrm;
  out[1] = in[1]/nrm;
  out[2] = in[2]/nrm;

}
/*================================*/

void   hladdt3(const double* x,const double* y,double* out)
{
  double lout[3];
  
  lout[0] = x[0] + y[0];
  lout[1] = x[1] + y[1];
  lout[2] = x[2] + y[2];
  
  memcpy(out,lout,3*sizeof(double));
    
}
/*================================*/

void   hlsubt3(const double* x,const double* y,double* out)
{
  double lout[3];
  
  lout[0] = x[0] - y[0];
  lout[1] = x[1] - y[1];
  lout[2] = x[2] - y[2];
  
  memcpy(out,lout,3*sizeof(double));
    
}
/*================================*/

void   hladdt5(const double* x,const double* y,double* out)
{
  double lout[5];
  double m2;
  
  lout[0] = x[0] + y[0];
  lout[1] = x[1] + y[1];
  lout[2] = x[2] + y[2];
  lout[3] = x[3] + y[3];
  
  m2 = hlsclp5(lout,lout);
  
  lout[4] = hlsignm(m2)*sqrt(fabs(m2));
  
  memcpy(out,lout,5*sizeof(double));
    
}
/*================================*/

void   hlsubt5(const double* x,const double* y,double* out)
{
  double lout[5];
  double m2;
  
  lout[0] = x[0] - y[0];
  lout[1] = x[1] - y[1];
  lout[2] = x[2] - y[2];
  lout[3] = x[3] - y[3];
  
  m2 = hlsclp5(lout,lout);
  
  lout[4] = hlsignm(m2)*sqrt(fabs(m2));
  
  memcpy(out,lout,5*sizeof(double));

}
/*================================*/

void   hlmass5(const double* in,double* out,double mass)
{
  out[0] = in[0];
  out[1] = in[1];
  out[2] = in[2];
  out[3] = sqrt(mass*mass + hlsclp3(in,in));
  out[4] = mass;
}
/*================================*/

void   hlrotx3(const double* in,double* out,double f)
{
  double lout[3],cf,sf;
  
  cf = cos(f);
  sf = sin(f);
  
  lout[0] = in[0];
  lout[1] = in[1]*cf + in[2]*sf; 
  lout[2] = in[2]*cf - in[1]*sf;

  memcpy(out,lout,3*sizeof(double));
}
/*================================*/

void   hlroty3(const double* in,double* out,double f)
{
  double lout[3],cf,sf;
  
  cf = cos(f);
  sf = sin(f);
  
  lout[0] = in[0]*cf - in[2]*sf;
  lout[1] = in[1]; 
  lout[2] = in[2]*cf + in[0]*sf;

  memcpy(out,lout,3*sizeof(double));

}
/*================================*/

void   hlrotz3(const double* in,double* out,double f)
{
  double lout[3],cf,sf;
  
  cf = cos(f);
  sf = sin(f);
  
  lout[0] = in[0]*cf + in[1]*sf;
  lout[1] = in[1]*cf - in[0]*sf; 
  lout[2] = in[2];

  memcpy(out,lout,3*sizeof(double));

}
/*================================*/

void   hlbstx5(const double* in,double* out,double g)
{
  double lout[4],gm,gmv;
  
  gm = fabs(g);
  gmv= sqrt(g*g-1)*hlsignm(g);
  
  lout[0] = in[0]*gm - in[3]*gmv;
  lout[1] = in[1]; 
  lout[2] = in[2];
  lout[3] = in[3]*gm - in[0]*gmv;
  
  memcpy(out,lout,4*sizeof(double));

}
/*================================*/

void   hlbsty5(const double* in,double* out,double g)
{
  double lout[4],gm,gmv;
  
  gm = fabs(g);
  gmv= sqrt(g*g-1)*hlsignm(g);
  
  lout[0] = in[0];
  lout[1] = in[1]*gm - in[3]*gmv; 
  lout[2] = in[2];
  lout[3] = in[3]*gm - in[1]*gmv;
  
  memcpy(out,lout,4*sizeof(double));

}
/*================================*/

void   hlbstz5(const double* in,double* out,double g)
{
  double lout[4],gm,gmv;
  
  gm = fabs(g);
  gmv= sqrt(g*g-1)*hlsignm(g);
  
  lout[0] = in[0];
  lout[1] = in[1]; 
  lout[2] = in[2]*gm - in[3]*gmv;
  lout[3] = in[3]*gm - in[2]*gmv;
  
  memcpy(out,lout,4*sizeof(double));

}
/*================================*/

double hlmod2p(double f)
{
  while(f > 2.*M_PI) f-=2.*M_PI;
  while(f < 0.)      f+=2.*M_PI;
  
  return f;
}

void hltrn3(const double* in,double* out,const double *ref) {
  //turn space to set ref[2] along Z
  //turn around Y and then around X
  double p1 = in[0],
         p2 = in[1],
         p3 = in[2];

  double b1 = ref[0],
         b2 = ref[1],
         b3 = ref[2];

  double l1,l2,l3;
  double phi1 = atan2(b1,b3);
  double phi2 = atan2(b2,sqrt(b1*b1+b3*b3));

  l1 = p1*cos(phi1)-p3*sin(phi1);
  l2 = -sin(phi1)*sin(phi2)*p1 + 
    cos(phi2)*p2 -
    sin(phi2)*cos(phi1)*p3;
  l3 = sin(phi1)*cos(phi2)*p1 + 
    sin(phi2)*p2 +
    cos(phi2)*cos(phi1)*p3;

  out[0] = l1;
  out[1] = l2;
  out[2] = l3;
}

/*==============END===============*/
