/* mass.h 
          Masses  of stable particles 
*/
#ifndef  __MASS_H__
#define  __MASS_H__

#define  PROT_MASS  0.938272
#define  NEUT_MASS  0.939566
#define  PI_MASS    0.139570
#define  PI0_MASS   0.134976
#define  ETA_MASS   0.547300
#define  K_MASS     0.493677
#define  K0_MASS    0.497672
#define  MU_MASS    0.105658
#define  E_MASS     0.000511 
#define  TAU_MASS   1.777000

#endif
