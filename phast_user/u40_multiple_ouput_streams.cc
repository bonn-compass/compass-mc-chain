#include <iostream>
#include <cmath>
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "TH1D.h"


// Example how to tag events to be saved into different output streams
//
// Streams are defined by text file with file names
// E.g. file "streams.txt" contains few lines:
//    a.root
//    b.root
//    c.root
//    d.root

// If you run phast  with "-O streams.txt" option (do not use "-o" in the same time!)
// you will have few streams: 0,1,2,3.. corresponding to output files a.root b.root, c.root etc. 
//
// In this example events with trigger "i" will be saved to stream "i", 
// 

void UserEvent40(PaEvent& e)
{
  int msk = e.TrigMask();
  if((msk & 1<<0) != 0) e.SaveToStream(0);
  if((msk & 1<<1) != 0) e.SaveToStream(1);
  if((msk & 1<<2) != 0) e.SaveToStream(2);
  if((msk & 1<<3) != 0) e.SaveToStream(3);
}














