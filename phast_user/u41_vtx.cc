#include <iostream>
#include <cmath>
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "PaAlgo.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"

void UserEvent4100(PaEvent& e)
{
  
  bool print = false;
  static bool first = true;
  static TH1D* h1[50];
  static TH2D* h2[50];
  static TH3D* h3[3];

  const double z0 = -100;
  const double z1 =   50;

  //const double z0 = 400;
  //const double z1 = 1200;

  if(first){
    h1[1]  = new TH1D("h1_01","Zmin",               1000, z0, z1);
    h1[2]  = new TH1D("h1_02","Zmin error [cm]",    200,  0,  20);
    h1[3]  = new TH1D("h1_03","Dist at min [cm]",   100,  0,  10);
    h1[4]  = new TH1D("h1_04","Zmin (after cuts)",  1000, z0, z1);
    h1[5]  = new TH1D("h1_05","N iterations",       200, 0, 200);
    h1[6]  = new TH1D("h1_06","Sigma X mean",       100, 0, 1);
    h1[7]  = new TH1D("h1_07","Sigma Y mean",       100, 0, 1);

    h2[1]  = new TH2D("h2_01","Y VX X vertex",              200, -10, 10, 200, -10, 10);
    h2[2]  = new TH2D("h2_02","Y VX X vertex (after cuts)", 200, -10, 10, 200, -10, 10);

    h3[1]  = new TH3D("h3_01","X,Y,Z vertex",               200, -10, 10, 200, -10, 10, 50, z0, z1);
    h3[2]  = new TH3D("h3_02","X,Y,Z vertex (after cuts)",  200, -10, 10, 200, -10, 10, 50, z0, z1);

    first = false;
  }

  // select track candidates to build vertex
  vector<PaTPar> vpar;
  for(int i = 0; i < int(e.vTrack().size()); i++){ // loop over tracks
    const PaTrack& t = e.vTrack()[i];
    if(t.ZFirst() == t.ZLast())  continue; // beam
    if(t.ZFirst() > 350)  continue; // not befor SM1
    //if(t.ZFirst() < 700. && t.ZFirst() > 2000)   continue; // not between RICH and SM2
    // start from the first point
    const PaTPar par = t.vTPar(0);
    vpar.push_back(par);
  }
  

  // take all combinations with M tracks out of found candidates
  int M = 2; // M-prong vtx
  int N = vpar.size();
  if(int(vpar.size()) < M) return; 

  if(print) cout<<"==================== N = "<<N<<endl;

  int ind[M];  
  while(1){ // loop over track combinations
    if(!PaAlgo::CombMofN(N,M,ind)) break;
    vector<PaTPar> vcomb; // for tracks of  current combination (must be size M)
    if(print) cout<<"-----> ";
    for(int j = 0; j < M; j++){ // loop over indexies of current combination
      if(print) cout<<ind[j]<<"  ";
      vcomb.push_back(vpar[ind[j]]); // store current combination 
    }
    if(print) cout<<endl;
    if(print) cout<<"     Z0 = "<<vcomb[0](0)<<" X0 = "<<vcomb[0](1)<<" Y0 = "<<vcomb[0](2)<<endl;
    if(print) cout<<"     Z1 = "<<vcomb[1](0)<<" X1 = "<<vcomb[1](1)<<" Y1 = "<<vcomb[0](2)<<endl;

    double zmin, ezmin, dist;
    int niter;
    bool ret = PaAlgo::FindVtx(vcomb, z0, z1, zmin, ezmin, dist, niter); // try to find vtx
    if(!ret) continue; // minimization had failed

    h1[1]->Fill(zmin);
    h1[2]->Fill(ezmin);
    h1[3]->Fill(dist);
    h1[5]->Fill(niter+0.5);
    
    
    // X Y as weighted mean
    double xsum = 0;
    double ysum = 0;
    double wxsum = 0;
    double wysum = 0;
    for(int i = 0; i <int(vcomb.size()); i++){
      PaTPar HH;
      vcomb[i].Extrap(zmin,HH); // extrap to zmin
      double wx = 1./HH(1,1);
      xsum  += HH(1)*wx;
      wxsum += wx;
      double wy = 1./HH(2,2);
      ysum  += HH(2)*wy;
      wysum += wy;
    }
    double xmean = xsum/wxsum;
    double sigx  = sqrt(1./wxsum);
    double ymean = ysum/wysum;
    double sigy  = sqrt(1./wysum);

    h1[6]->Fill(sigx);
    h1[7]->Fill(sigy);

    h2[1]->Fill(xmean,ymean);
    h3[1]->Fill(xmean,ymean, zmin);
    
    // cuts
    if(dist  > 0.5) continue;
    if(ezmin > 10)  continue;

    h1[4]->Fill(zmin);
    h2[2]->Fill(xmean,ymean);
    h3[2]->Fill(xmean,ymean, zmin);

    if(print) cout<<"===> Zmin "<<zmin<<" +- "<<ezmin<<" Dist = "<<dist<<endl;
  }// end of loop over combinations
  
}













