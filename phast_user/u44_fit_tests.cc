#include <iostream>
#include <cmath>
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "TH1D.h"

//

void UserEvent44(PaEvent& e)
{

  bool print = false;
  static bool first = true;
  static TH1D* h1[100];
  
  if(first){
    h1[1] = new TH1D("h1_01", "#DeltaX at the first point after refit [cm]", 500, -0.25, 0.25);
    h1[2] = new TH1D("h1_02", "#DeltaY at the first point after refit [cm]", 500, -0.25, 0.25);
    h1[3] = new TH1D("h1_03", "#DeltaX' at the first point after refit [urad]", 500, -100, 100);
    h1[4] = new TH1D("h1_04", "#DeltaY' at the first point after refit [urad]", 500, -100, 100);
    h1[5] = new TH1D("h1_05", "#Deltaq/P at the first point after refit [GeV^{-1}]", 500, -0.25, 0.25);
    first = false;
  }
     

  for(int i=0; i < e.NTrack(); i++) { // loop over tracks
    PaTrack t0 = e.vTrack(i);
    PaTrack t1 = t0; // copy of the track

    // do Kalman fit of the track's copy
    t1.FullKF( 1); // fit forward
    t1.FullKF(-1); // fit bacrward

    // compare "original" and "refitted" track parameters 
    if(print){
      cout<<endl<<endl;
      cout<<"------------- Original track"<<endl;
      t0.Print(2);
      cout<<endl;
      cout<<"------------- Refitted track"<<endl;
      t1.Print(2);
    }
    const PaTPar& H0 = t0.vTPar(0); // track parameters in the first point
    const PaTPar& H1 = t1.vTPar(0); // track parameters in the first point

    double dx  = H1(1)-H0(1);
    double dy  = H1(2)-H0(2);
    double dxp = 1.e+6*(H1(3)-H0(3));
    double dyp = 1.e+6*(H1(4)-H0(4));
    double dp  = H1(5)-H0(5);

    h1[1]->Fill(dx);
    h1[2]->Fill(dy);
    h1[3]->Fill(dxp);
    h1[4]->Fill(dyp);
    h1[5]->Fill(dp);

  } // end of loop over tracks
}














