#include <iostream>
#include <cmath>
#include "Phast.h"
#include "PaSetup.h"
#include "PaEvent.h"
#include "TH1D.h"

//

void UserEvent45(PaEvent& e)
{

  static bool first = true;
  static TH1D* h1[100];
  
  string det = "SI02U1__"; // detector under study 
  //string det = "FI01Y1__";

  int ipl = PaSetup::Ref().iDetector(det);             // get detector index
  assert(ipl >= 0);
  const PaDetect& d = PaSetup::Ref().Detector(ipl);
  const vector<Int_t>& vHref = d.vHitRef();               // vector of referencies to hits on the detector
  
  d.ShiftHits(0.01,0);

  if(first){

    h1[10] = new TH1D("h1_10", ("#DeltaU on detector "+det+" [cm] for all hits").      c_str(), 500, -0.05, 0.05);
    h1[11] = new TH1D("h1_11", ("#DeltaU on detector "+det+" [cm] for associated hit").c_str(), 500, -0.05, 0.05);
    
    first = false;
  }
     
  for(int i=0; i < e.NTrack(); i++) { // loop over tracks
    PaTrack t0 = e.vTrack(i);
    PaTrack t1 = t0; // copy of the track

    // form list of detectors which has to be used in the fit.
    // (simply do not call t1.KeepOnly(dets) if whole track has to be refited)
    list<string> dets;
    dets.push_back("FI");
    dets.push_back("SI");
    int nh = t1.KeepOnly(dets);
    if(nh < 6) continue; // too few hits left. Fit could be not reliable. Skip track.
    
    // do Kalman fit of the track's copy
    // "original" (i.e. from CORAL) track parameters at the first plane are used 
    // as starting point of iterations for the fit forward
    bool ret1 = t1.FullKF( 1); // fit forward
    bool ret2 = t1.FullKF(-1); // fit bacrward
    if(!(ret1 && ret2)) continue; // fit failed

    if(!t1.KFdone()) {
      cout<<endl<<"KF forward/backward was not done or not completed"<<endl;
      t1.PrintSmoothMaps();
    }

    // Calculate residuals 

    PaTPar Hsm;
    bool include_hit = false;                            // flag to control if to exclude hit on "det" out of smoothing procedure 

    double chi2 = t1.GetSmoothed(Hsm, det, include_hit); // get smoothed track parameters on "det"

    if(chi2 < 0) continue;                               // requested detector is not crossed by this track. Go to next track

    PaTPar Hrot;
    Hsm.Rotate(d.Ca(), d.Sa(), Hrot);                    // rotate smoothed parameters to detector wire reference system

    for(int ih = 0; ih < int(vHref.size()); ih++){ // loop over hits on the detector
      int ihref = vHref[ih];
      const PaHit& h = e.Hits()[ihref];
      double du = Hrot(1) - h.U();
      h1[10]->Fill(du);
    }                                              // end of loop over hits on the detector
    
    int ihit = t1.iHit(det);
    if(ihit >=0) h1[11]->Fill(Hrot(1) - e.Hits()[ihit].U());
  } // end of loop over tracks
}














