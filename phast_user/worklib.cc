#include "worklib.h"

void GetPgamma(double x,double y,double z,double E, 
	       double vx,double vy,double vz, double *p) {

  double dist = sqrt((x-vx)*(x-vx)+(y-vy)*(y-vy)+(z-vz)*(z-vz));
  p[0] = (x-vx)/dist*E;
  p[1] = (y-vy)/dist*E;
  p[2] = (z-vz)/dist*E;
  p[3] =  E;
  p[4] =  0.0;sqrt(p[3]*p[3]-p[2]*p[2]-p[1]*p[1]-p[0]*p[0]);
}

TLorentzVector GetPgammaLV(double x,double y,double z,double E, 
			   double vx,double vy,double vz) {
  
  double p[5]; GetPgamma(x,y,z,E,vx,vy,vz,p);
  return TLorentzVector(p[0],p[1],p[2],p[3]);
}


TLorentzVector GetPgammaLV(double x,double y,double z, double E, const double v[]) {
  double p[5]; GetPgamma(x,y,z,E,v[0],v[1],v[2],p);
  return TLorentzVector(p[0],p[1],p[2],p[3]);
}
void GetPgamma(double x,double y,double z, double E, const double v[], double *p) {
  GetPgamma(x,y,z,E,v[0],v[1],v[2],p);
}
TLorentzVector GetPgammaLV(const double pos[], double E, double v[]) {
  double p[5]; GetPgamma(pos[0],pos[1],pos[2],E,v[0],v[1],v[2],p);
  return TLorentzVector(p[0],p[1],p[2],p[3]);
}
void GetPgamma(const double pos[], double E, const double v[], double *p) {
  GetPgamma(pos[0],pos[1],pos[2],E,v[0],v[1],v[2],p);
}
