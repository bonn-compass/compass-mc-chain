#ifndef __WORKLIB_H__
#define __WORKLIB_H__

#include "TLorentzVector.h"
//#include "TLorentzVector.h"

void GetPgamma(double x,double y,double z,double E, 
	       double vx,double vy,double vz, double *p);
TLorentzVector GetPgammaLV(double x,double y,double z,double E, 
			   double vx,double vy,double vz);

TLorentzVector GetPgammaLV(const double pos[], double E, double v[]);
TLorentzVector GetPgammaLV(double x,double y,double z, double E, const double v[]);

void GetPgamma(double x,double y,double z, double E, const double v[], double *p);
void GetPgamma(const double pos[], double E, const double v[], double *p);

#endif
